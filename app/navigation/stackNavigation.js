import 'react-native-gesture-handler';
import React,{ Component } from "react";
import { ActivityIndicator,StyleSheet,Image,BackHandler,ToastAndroid,ImageBackground, View } from 'react-native';

import { createStackNavigator,CardStyleInterpolators } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';

import SwiperComponent from '../pages/onBoarding/onBoarding';
import LoginPage from '../pages/login/login';
import RegisterPage from '../pages/registrationPage/register';
import Home from '../pages/Home/Home';
import ForgotPassword from '../pages/forgotPassword/forgotPassword'
import Otp from '../pages/otp/otp'
import ResetPassword from '../pages/resetPassword/resetPassword'
import CourseDetails from '../pages/coursedetails/coursedetails'
import Cart from '../pages/cart/cart'
import Checkout from '../pages/checkout/checkout'
import PaymentPage from '../pages/paymentCheckout/paymentCheckout'
import MyCourse from '../pages/mycourse/mycourse'
import MyCourseDetails from '../pages/mycoursedetails/mycoursedetails'
import ChapterPage from '../pages/chapterpage/chapterpage'
import ChapterDetailsPage from '../pages/chapterDetailsPage/chapterDetailsPage'
import ChapterVideo from '../pages/chaptervideo/chapterVideo'
import ChapterPdf from '../pages/chapterPdf/ChapterPdf'
import Quiz from '../pages/quiz/quiz'
import ProfilePage from '../pages/profile/profile';
import OrderSummary from '../pages/orderSummary/orderSummary'
import MyOrders from '../pages/orders/orders'
import DrawerIcon1 from '../assests/images/png/nav-ico1.png'
import DrawerIcon2 from '../assests/images/png/nav-ico2.png'
import DrawerIcon3 from '../assests/images/png/nav-ico3.png'
import DrawerIcon4 from '../assests/images/png/cart-icon.png'
import DrawerIcon5 from '../assests/images/png/order-ico.png'
import Splash from '../assests/images/png/splash.png'
import DrawerProfile from './drawerNavigatorPage';
import PlayVideo from '../pages/playVideo/playVideo'
import PdfView from '../pages/pdf/pdf'
import QuizView from '../pages/quizView/quizView'
import {getKey} from '../helpers/helpers'

const Stack = createStackNavigator();

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="SwiperComponent"
      component={SwiperComponent}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="LoginPage"
      component={LoginPage}
      options={{
        headerShown: false ,
      }}
    />
    <Stack.Screen
      name="RegisterPage"
      component={RegisterPage}
      options={{
        headerShown: false ,
      }}
    />
    <Stack.Screen
      name="ForgotPassword"
      component={ForgotPassword}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Otp"
      component={Otp}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ResetPassword"
      component={ResetPassword}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Home"
      component={DrawerScreen}
      options={{
        headerShown: false,
      }}
    />
  </AuthStack.Navigator>
);

const PurchasedCourses = createStackNavigator();
const coursesPurchased = () => (
  <PurchasedCourses.Navigator initialRouteName="MyCourse"
  screenOptions={{
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
  }}>
    <PurchasedCourses.Screen
      name="MyCourse"
      component={MyCourse}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="MyCourseDetails"
      component={MyCourseDetails}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ChapterPage"
      component={ChapterPage}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ChapterDetailsPage"
      component={ChapterDetailsPage}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ChapterVideo"
      component={ChapterVideo}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="ChapterPdf"
      component={ChapterPdf}
      options={{
        headerShown: false,
      }}
    />
   <Stack.Screen
      name="Quiz"
      component={Quiz}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="QuizView"
      component={QuizView}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="PlayVideo"
      component={PlayVideo}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="PdfView"
      component={PdfView}
      options={{
        headerShown: false,
      }}
    />
  </PurchasedCourses.Navigator>
);
const Ordered = createStackNavigator();
const orderedClasses = () => (
  <Ordered.Navigator initialRouteName='MyOrder'
  screenOptions={{
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
  }}>
    <Ordered.Screen
      name="MyOrder"
      component={MyOrders}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="OrderSummary"
      component={OrderSummary}
      options={{
        headerShown: false,
      }}
    />
  </Ordered.Navigator>
);
const cart = createStackNavigator();
const cartMenu = () => (
  <cart.Navigator initialRouteName='Cart'
  screenOptions={{
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
  }}>
    <cart.Screen
      name="Cart"
      component={Cart}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Checkout"
      component={Checkout}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="PaymentPage"
      component={PaymentPage}
      options={{
        headerShown: false,
      }}
    />
  </cart.Navigator>
);
/*
 * @name UserLoggedInScreens
 * @description screens after user logged in
 */
const UserLoggedIn = createStackNavigator();
const UserLoggedInScreens = () => (
  <UserLoggedIn.Navigator initialRouteName='Home'
  screenOptions={{
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
  }}>
    <UserLoggedIn.Screen
      name="Home"
      component={Home}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="CourseDetails"
      component={CourseDetails}
      options={{
        headerShown: false,
      }}
    />
    {/* <Stack.Screen
      name="Notification"
      component={Notification}
      options={{
        headerShown: false,
      }}
    /> */}
    <Stack.Screen
      name="MyProfile"
      component={ProfilePage}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Cart"
      component={cartMenu}
      options={{
        headerShown: false,
      }}
    />
    {/* <Stack.Screen
      name="Checkout"
      component={Checkout}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="PaymentPage"
      component={PaymentPage}
      options={{
        headerShown: false,
      }}
    /> */}
    <Stack.Screen
      name="MyCourse"
      component={coursesPurchased}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="MyOrder"
      component={orderedClasses}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="LoginPage"
      component={AuthStackScreen}
      options={{
        headerShown: false,
      }}
    />
  </UserLoggedIn.Navigator>
);

/*
 * @name DrawerScreen
 * @description screens for side drawer navigator
 */
const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator
    initialRouteName="Dashboard"
    drawerContentOptions={{
      activeBackgroundColor: null,
      activeTintColor: '#707070',
      iconContainerStyle: {
        opacity: 2,
      },
    }}
     drawerContent={props => <DrawerProfile {...props}/>}>
    <Drawer.Screen
      name="Dashboard"
      component={UserLoggedInScreens}
      options={{
        headerShown: false,
        drawerIcon: ({ }) => (
          <Image
            style={{
              resizeMode: 'contain',
              width: 30,
              height: 30,
            }}
            source={DrawerIcon1}
          />
        ),
      }}
    />
    <Drawer.Screen
      name="My Courses"
      component={coursesPurchased}
      options={{
        headerShown: false,
        drawerIcon: ({ }) => (
          <Image
            style={{
              resizeMode: 'contain',
              width:30,
              height:30,
            }}
            source={DrawerIcon2}
          />
        ),
      }}
    />
    {/* <Drawer.Screen
      name="Notifications"
      component={Notification}
      options={{
        headerShown: false,
        drawerIcon: ({ }) => (
          <Image
            style={{
              resizeMode: 'contain',
              width: 30,
              height: 30,
            }}
            source={DrawerIcon3}
          />
        ),
      }}
    /> */}
    <Drawer.Screen
      name="My Cart"
      component={cartMenu}
      options={{
        headerShown: false,
        drawerIcon: ({ }) => (
          <Image
            style={{
              resizeMode: 'contain',
              width: 30,
              height: 30,
            }}
            source={DrawerIcon4}
          />
        ),
      }}
    />
    <Drawer.Screen
      name="My Orders"
      component={orderedClasses}
      options={{
        headerShown: false,
        drawerIcon: () => (
          <Image
            style={{
              resizeMode: 'contain',
              width: 30,
              height: 30,
            }}
            source={DrawerIcon5}
          />
        ),
      }}
    />
  </Drawer.Navigator>
);

/*
 * @name RootStackScreen
 * @description parent of all stack navigators
 */
// SCREEN FLOW BASED ON USER LOGIN
const RootStack = createStackNavigator();

const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">
    {!userToken ? (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    ) : (
      <RootStack.Screen
        name="User"
        component={DrawerScreen}
        options={{
          animationEnabled: false,
        }}
      />
    )}
  </RootStack.Navigator>
);
let backPressed = 0;
class MainStackNavigator extends Component{
  constructor(){
    super();
    this.state = {
      splash: false,
      isLoading: true,
      userToken: '',
      userType: '',
      userOnBoarding: '',
      backPressed: 1,
      localKey: null,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
   
  // componentDidMount(){
  //   setTimeout(() => {
  //     this.setTimePassed();
  //   },10);
      
  // }
  // Splash Screen Time Setting
  async componentDidMount() {
    setTimeout(() => {
      this.setTimePassed();
    }, 10);
    BackHandler.addEventListener('backPress', this.handleBackButtonClick);
    let getLocalKey = await getKey('loginData');
    this.setState({ localKey: getLocalKey });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('backPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    if (backPressed > 0) {
      BackHandler.exitApp();
      backPressed = 0;
    } else {
      backPressed++;
      ToastAndroid.show('Press Again To Exit', ToastAndroid.SHORT);
      setTimeout(() => {
        backPressed = 0;
      }, 1000);
      return true;
    }
  };
  setTimePassed(){
    this.setState({splash:true});
  }

  render(){
    const { localKey } = this.state; 
    if(!this.state.splash){
     return( 
        <ActivityIndicator  style={[styles.container, styles.horizontal]}
      color = '#fe4155'
      size = 'large'/>
      // <View>
      //           <Image
      //             source={require('../assests/images/png/splash.png')}
      //             styles={{width:'100%',height:'100%'}}
      //             resizeMode='cover'
      //           />
      // </View>
      )
    }
    else{
    return (
      <RootStackScreen userToken={localKey}/>
    );
    }           
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});


export default MainStackNavigator;
