import React, { Component } from 'react';
import { SafeAreaView,StyleSheet,View,Text,TouchableOpacity,ImageBackground, Platform } from 'react-native';
import ImageField from '../components/images/images';
import Title from '../components/title/title'
import DrawIcon6 from '../assests/images/png/nav-ico6.png'
import Background from '../assests/images/png/bg_share.png'
import { DrawerContentScrollView,DrawerItemList } from '@react-navigation/drawer';
import { studentProfileAPI } from '../store/action/authAction';
import { connect } from "react-redux";
import { getKey,clearLocalStorage } from '../helpers/helpers';
import { StackActions } from '@react-navigation/native';

class DrawerProfile extends Component {
    constructor(){
        super();
    }
    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(studentProfileAPI({userId}))
    }
    logout = () => {
        clearLocalStorage();
        this.props.navigation.dispatch(
            StackActions.replace('LoginPage', { test: 'LoginPage Params' })
          )
        this.props.navigation.navigate('LoginPage')
    }
    render() { 
        const { profileData } = this.props.authStateData
        return ( 
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <ImageBackground source={Background} style={{width:'100%',height:'90%'}}>
                <View style={{marginTop:40,flexDirection :'row'}}>
                    {/* <View style={styles.border_img}> */}
                        {/* <ImageField aariv_img={DrawIcon6} imageview={{width:50,height:50,alignSelf:'center'}}/> */}
                    {/* </View> */}
                    <View style={{flexDirection:'column',marginLeft:80,marginBottom:Platform.OS === 'ios'?40:40}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('MyProfile')}>
                        <Text style={styles.nameText}>{profileData !== ''?profileData.full_name:''}</Text>
                        {/* <Text style={styles.linkStyles}>Class {profileData !== ''?profileData.class:''}</Text> */}
                    </TouchableOpacity>
                    </View>
                </View>
                {/* <DrawerContentScrollView {...this.props} style={{ backgroundColor: '#fff',marginTop:-12 }}> */}
                    <DrawerItemList {...this.props} labelStyle={styles.drawer_label}/>
                    <TouchableOpacity onPress={() => this.logout()} style={{flexDirection:'row'}}>
                        <ImageField aariv_img={DrawIcon6} imageview={{width:30,height:30,marginLeft:19,marginTop:18}}/>
                        <Title titletxt="Logout" titlestyle={[styles.drawer_label,{marginTop:18,marginLeft:30,color:"#707070"}]}/>
                    </TouchableOpacity>
                {/* </DrawerContentScrollView> */}
                </ImageBackground>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    border_img: {
        borderRadius:50,
        resizeMode: 'cover',
        width: 60,
        height: 60,
        borderColor: '#f0eff5',
        borderWidth:0,
        marginLeft:10
      },
      nameText:{
        fontSize:25,
        color:'#1e266d',
        lineHeight: 30,
        fontFamily:'AvenirLTStd-Roman',
        // width: 100,
        letterSpacing: 0.66,
      },
      linkStyles:{
        color: '#14131e',
        fontSize: 18,
        letterSpacing: 0.03,
        fontWeight: '500',
        lineHeight: 22,
        fontFamily:'AvenirLTStd-Roman' ,
        marginTop:5
      },
      drawer_label:{
        fontSize: 18,
        fontFamily:'AvenirLTStd-Roman',padding:Platform.OS === 'ios'?5:2
      }
})
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(DrawerProfile);