import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen'
import MainStackNavigator from './stackNavigation';

class Router extends Component{

  componentDidMount() {
    SplashScreen.hide()
  };
  
  render(){
    return(

          <NavigationContainer>
             <MainStackNavigator/>
          </NavigationContainer>
    
    );
  } 
}
export default Router;