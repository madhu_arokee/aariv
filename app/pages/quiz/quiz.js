import React, { Component } from 'react';
import { SafeAreaView,View,Text,BackHandler,StatusBar,StyleSheet,width,TouchableWithoutFeedback, Platform } from 'react-native';
import BackButton from '../../components/backbutton/backbutton';
import PageComponent from '../../components/pageComponent/pageComponent';
import ViewDocument from '../../components/view_documents/view_documents';
import TopNotch from '../../components/statusBar/statusBar'
import { studentClassesAPI,studentSingleClassAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { WebView } from 'react-native-webview'

class Quiz extends Component {
    constructor() {
        super(); 
        this.state = {
            }       
    }
    render() { 
        const { studentchapterDetailsData } = this.props.authStateData
        // console.log("studentchapterDetailsData",studentchapterDetailsData)
        // const source = {uri:"http://dev.arokee.com/aslam/Aariv/quiz/class-1-social-chapter-2",cache:false};
        const source = this.props.route.params.quiz;
        console.log('text',source)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            <BackButton OnClick={() => this.props.navigation.goBack()} button_style={{paddingBottom:40}}/>
                    <WebView
                        source={{uri : source}}
                        startInLoadingState={true}
                        renderLoading={
                            ()=> {
                            return (<ActivityLoader style={{marginTop:-400}} /> )
                        }
                    }
                    />
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(Quiz);