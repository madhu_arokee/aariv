import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,height,ScrollView,BackHandler,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../../components/images/images';
import Alarm from '../../assests/images/png/alarm.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import ViewIcon from '../../assests/images/png/view-ico.png';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import CardImage from '../../assests/images/png/cardimage.png' 
import TopNotch from '../../components/statusBar/statusBar'
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { getStudentCartAPI,getOrdersAPI,singleOrderAPI, authAction } from '../../store/action/authAction';
import { getKey } from '../../helpers/helpers'
import { connect } from "react-redux";
import BurgerMenu from '../../assests/images/png/burgurmenu.png';

class MyOrders extends Component {
    constructor() {
        super(); 
        this.state = {   
        }      
    } 
    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(getOrdersAPI({userId}))
    }
    orderDetails = async(order_id) => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(singleOrderAPI({
            userId,
            order_id,
            navigation:this.props.navigation
        }))
    }
    render() { 
        const { studentOrderData,studentLoader } = this.props.authStateData
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {studentLoader === false ?
                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:30}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>My Orders</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                        <ScrollView style={styles.subContainer} contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?100:100}}>
                        {
                                studentOrderData && studentOrderData.length > 0 ? 
                                studentOrderData && studentOrderData.map((item,id) => {
                                    return(
                                    <View style={styles.semicontainer} key={id}>
                                        <View style={{marginLeft:20,marginBottom:10}} >
                                            <View style={{flexDirection:'row',marginTop:10}}>
                                                <Title titlestyle={styles.txt} linenoTxt={2} titletxt='Every Class always will be the Step for next Class'/>
                                                <Title titlestyle={{color:'grey',right:Platform.OS === 'ios'?30:30}} titletxt={item.order_id}/>
                                                <Title titletxt={item.order_date} titlestyle={{color:'grey',top:20,right:Platform.OS === 'ios'?90:85}}  />
                                            </View>
                                            <View style={{flexDirection:'row',marginTop:15,marginBottom:10}}>
                                                <Title titlestyle={styles.amount_txt} titletxt={`₹${item.order_total}`}/>
                                                {item && item.order_status && item.order_status === 'completed'?
                                                <View style={[styles.button,{backgroundColor:'#2e4453'}]}>
                                                    <Title titletxt={item.order_status} titlestyle={styles.button_txt}/>
                                                    {/* Need to change into buttom when the cancel is enabled */}
                                                </View>
                                                :
                                                <View style={styles.button}>
                                                    <Title titletxt={item.order_status} titlestyle={styles.button_txt}/>
                                                    {/* Need to change into buttom when the cancel is enabled */}
                                                </View>
                                                }
                                            </View>   
                                            <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:-50,marginBottom:10,marginRight:-15}}>
                                                    <TouchableOpacity onPress={() => this.orderDetails(item.order_id)}>
                                                        <ImageField imageview={{width:35,height:35}} aariv_img={ViewIcon} />
                                                    </TouchableOpacity>
        
                                            </View>          
                                        </View>

                                 
                            </View>
                               )
                            })
                            :
                            <Text style={{color:'red',textAlign:'center',marginTop:50}}>Please place the Order</Text> 
                            }
                        </ScrollView>
                    </ImageBackground>
                    :
                    <ActivityLoader/>
                        }
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'100%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:50
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:-30,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    subContainer:{
        flex:3,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    semicontainer:{
        width:'88%',
        backgroundColor:'#fff',
        marginLeft:25,
        marginTop:30,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        borderRadius:5,
    },
    minicontainer:{
        width:'90%',
        height:140,
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:30,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        marginBottom:30
    },
    amount:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        color:'#262626'
    },
    amount_txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        color:'#fe4155',
        lineHeight:35,
        paddingLeft:3,
        fontWeight:'bold'
    },
    txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        color:'#1e266d',
        lineHeight:25,
        marginRight:90
    },
    cross_txt:{
        color:'#fff',
        fontSize:25,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?7:2,
        paddingLeft:Platform.OS === 'ios'?2:1,
        letterSpacing:0.6
    },
    button_txt:{
        color:'#fff',
        paddingTop:Platform.OS === 'ios'?6:5,
        letterSpacing:0.6,
        textAlign:'center',
    },
    cross_style:{
        width:'80%',
        height:35,
        borderRadius:5,
        marginLeft:30,
        backgroundColor:'#da2c2c'
    },
    button:{
        width:'30%',
        height:30,
        backgroundColor:'#fe4155',
        borderRadius:5,
        marginLeft:20,
    },
    imageView:{
        width:90,
        height:90,
        marginLeft:20,
        marginTop:30,
        marginBottom:-20,
        // elevation:10
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })

 export default connect(mapStateToProps)(MyOrders);