import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, View, ScrollView, Text, Dimensions, ImageBackground, TouchableOpacity, Platform, Alert } from 'react-native';
import CustomButton from '../../components/customButton/customButton';
import TopNotch from '../../components/statusBar/statusBar'
import Title from '../../components/title/title';
import logo from '../../assests/images/png/logo.png';
import login_image from '../../assests/images/png/login_image.png'
import ImageField from '../../components/images/images';
import Textfield from '../../components/textfield/textfield';
import BackButton from '../../components/backbutton/backbutton';
import mailIcon from '../../assests/images/png/mail.png';
import pwdIcon from '../../assests/images/png/pwdicon.png';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { loginAPI, authAction } from '../../store/action/authAction';
import { connect } from "react-redux";

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            email: '',
            password: '',
        }
    }
    setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword })
    }
    componentDidMount = () => {
        this.props.dispatch(authAction({loginErr:''}))
    }
    handleSubmit = () => {
        const { dispatch } = this.props;
        const { email, password } = this.state;
        dispatch(authAction({ loginLoader: false }))
        if (password === '' && email === '') {
            dispatch(authAction({ loginLoader: false, loginErr: 'Please Check Email and Password' }));
        }
        else if (password === '' && email !== '') {
            dispatch(authAction({ loginLoader: false, loginErr: 'Please enter Password' }));
        }
        else if (email === '' && password !== '') {
            dispatch(authAction({ loginLoader: false, loginErr: 'Please enter Email' }));
        }
        else if (email !== '' && password !== '') {
            dispatch(authAction({ loginLoader: true }))
            dispatch(loginAPI({ email, password, navigation: this.props.navigation }))
        }
        else {
            dispatch(authAction({ loginLoader: false, loginErr: 'Please enter the Fields' }));
        }
    }

    render() {
        const { loginErr, loginLoader } = this.props.authStateData
        return (
            <>
                <SafeAreaView style={styles.topSafeArea} />
                <SafeAreaView style={styles.container}>
                    <TopNotch topNotchColor={"#fff"} topNotchStyle={"dark-content"} />
                    {Platform.OS === 'android' ?
                        <ScrollView>
                            <ImageBackground source={login_image} style={styles.image}>
                                <BackButton OnClick={() => this.props.navigation.navigate('SwiperComponent')} />
                                <ScrollView>
                                    <ImageField imageview={styles.avatar} aariv_img={logo} />
                                    <Title titlestyle={styles.login} titletxt='Login User' />
                                    <Title titlestyle={styles.description} titletxt='Enter your Email address and account password to get access your account' />
                                    <View style={{ marginVertical: 10 }}>
                                        <Textfield
                                            holder="Enter Email ID"
                                            text_Input='#3b3c4e'
                                            box_style={[{ marginTop: -5 }, styles.box_style]}
                                            icon={<ImageField imageview={[styles.login_smarticon, { marginVertical: 12 }]} aariv_img={mailIcon} />}
                                            onChangeText={value => this.setState({ email: value })}
                                        // value={this.state.email}
                                        />
                                    </View>
                                    <View style={{ marginVertical: 0 }}>
                                        <Textfield
                                            holder="Password"
                                            text_Input='#3b3c4e'
                                            box_style={styles.box_style}
                                            icon={<ImageField imageview={[styles.login_smarticon, { marginVertical: 10 }]} aariv_img={pwdIcon} />}
                                            secureTextEntry={this.state.hidePassword}
                                            onChangeText={value => this.setState({ password: value })}
                                        // value={this.state.password}
                                        />
                                        <TouchableOpacity onPress={() => this.setPasswordVisibility()} style={styles.pass_show}>
                                            <Ionicons name={(this.state.hidePassword) ? 'eye-outline' : 'eye-off-outline'} size={20} style={{ color: '#3b3c4e' }} />
                                        </TouchableOpacity>
                                    </View>
                                    {loginErr !== null &&
                                        <Text style={{ color: 'red', textAlign: 'center' }}>{loginErr}</Text>}
                                    <View style={styles.forgot_Pass}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                            <Title titlestyle={styles.forgotPassword} titletxt='Forgot Password ?' />
                                        </TouchableOpacity>
                                    </View>
                                    {
                                        loginLoader === false ?
                                            <CustomButton btntxt='GET STARTED' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.handleSubmit()} />
                                            :
                                            <ActivityLoader style={{ paddingTop: 10 }} size={15} />
                                    }
                                    <View style={styles.footer}>
                                        <Title titlestyle={styles.new} titletxt='New user ?' />
                                        <CustomButton btntxt='SIGN UP' touchview={styles.signupbutton} touchtext={styles.signuptxt} OnClick={() => this.props.navigation.navigate('RegisterPage')} />
                                    </View>
                                </ScrollView>
                            </ImageBackground>
                        </ScrollView>
                        :
                        <ImageBackground source={login_image} style={styles.image}>
                            <BackButton OnClick={() => this.props.navigation.goBack()} />
                            <ScrollView contentContainerStyle={{ paddingBottom: 300 }}>
                                <ImageField imageview={styles.avatar} aariv_img={logo} />
                                <Title titlestyle={styles.login} titletxt='Login User' />
                                <Title titlestyle={styles.description} titletxt='Enter your Email address and account password to get access your account' />
                                <View style={{ marginVertical: 10 }}>
                                    <Textfield
                                        holder="Enter Email ID"
                                        text_Input='#3b3c4e'
                                        box_style={[{ marginTop: -5 }, styles.box_style]}
                                        icon={<ImageField imageview={[styles.login_smarticon, { marginVertical: 12 }]} aariv_img={mailIcon} />}
                                        onChangeText={value => this.setState({ email: value })}
                                    />
                                </View>
                                <View style={{ marginVertical: 0 }}>
                                    <Textfield
                                        holder="Password"
                                        text_Input='#3b3c4e'
                                        box_style={styles.box_style}
                                        icon={<ImageField imageview={[styles.login_smarticon, { marginVertical: 10 }]} aariv_img={pwdIcon} />}
                                        secureTextEntry={this.state.hidePassword}
                                        onChangeText={value => this.setState({ password: value })}
                                    />
                                    <TouchableOpacity onPress={() => this.setPasswordVisibility()} style={styles.pass_show}>
                                        <Ionicons name={(this.state.hidePassword) ? 'eye-outline' : 'eye-off-outline'} size={20} style={{ color: '#3b3c4e' }} />
                                    </TouchableOpacity>
                                </View>
                                {loginErr !== null &&
                                    <Text style={{ color: 'red', textAlign: 'center' }}>{loginErr}</Text>}
                                <View style={styles.forgot_Pass}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                        <Title titlestyle={styles.forgotPassword} titletxt='Forgot Password ?' />
                                    </TouchableOpacity>
                                </View>
                                {
                                    loginLoader === false ?
                                        <CustomButton btntxt='GET STARTED' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.handleSubmit()} />
                                        :
                                        <ActivityLoader style={{ paddingTop: 10 }} size={15} />
                                }
                                <View style={styles.footer}>
                                    <Title titlestyle={styles.new} titletxt='New user ?' />
                                    <CustomButton btntxt='SIGN UP' touchview={styles.signupbutton} touchtext={styles.signuptxt} OnClick={() => this.props.navigation.navigate('RegisterPage')} />
                                </View>
                            </ScrollView>
                        </ImageBackground>}
                </SafeAreaView>
            </>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor: "#fff"
    },
    image: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height
    },
    avatar: {
        marginTop: Platform.OS === 'ios' ? 55 : 20,
        alignSelf: 'center'
    },
    login: {
        marginLeft: 40,
        fontFamily: 'AVENIRLTSTD-BLACK',
        fontSize: 20,
        fontWeight: '400',
        color: '#262626',
        marginTop: 60
    },
    description: {
        marginLeft: 20,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 13,
        color: '#3b3c4e',
        letterSpacing: 0.45,
        paddingTop: 8,
        textAlign: 'left',
        fontWeight: '400',
        paddingLeft: 20,
        marginRight: 65,
        paddingBottom: 15,
        lineHeight: 20,
    },
    box_style: {
        width: '80%',
        marginLeft: 40,
        borderRadius: 5,
        borderColor: '#e8e8e8',
        backgroundColor: '#f6f7fb'
    },
    loginbutton: {
        height: 40,
        alignSelf: 'center',
        width: Dimensions.get("window").width / 2.4
    },
    logintxt: {
        fontSize: 18,
        paddingTop: Platform.OS === 'ios' ? 9 : 8,
        textAlign: 'center',
        fontFamily: 'Montserrat-SemiBold'
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20
    },
    pass_show: {
        position: 'absolute',
        right: Platform.OS === 'ios' ? 65 : 65,
        bottom: Platform.OS === 'ios' ? 15 : 15,
    },
    forgotPassword: {
        color: '#1e266d',
        fontSize: 15,
        fontWeight: '400',
        fontFamily: 'AVENIRLTSTD-BLACK'
    },
    forgot_Pass: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginBottom: 26,
        marginTop: Platform.OS === 'ios' ? 15 : 5,
        marginRight: Platform.OS === 'ios' ? 50 : 70,

    },
    new: {
        color: '#1e266d',
        fontWeight: '400',
        marginTop: 18,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: Platform.OS === 'ios' ? 14 : 14,
        paddingRight: 5
    },
    signupbutton: {
        backgroundColor: 'white',
    },
    signuptxt: {
        color: '#1e266d',
        fontWeight: '400',
        marginTop: 17,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 14,
        textAlign: 'center'
    },
    footer: {
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "center",
    }
});
const mapStateToProps = state => ({
    authStateData: state.authState,
})

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);