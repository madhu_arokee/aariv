import React, { Component } from 'react';
import { SafeAreaView,View,Text,ScrollView,TouchableOpacity,Linking,ImageBackground,StyleSheet,BackHandler, Platform, Touchable } from 'react-native';
import BackButton from '../../components/backbutton/backbutton';
import ImageField from '../../components/images/images';
import PageComponent from '../../components/pageComponent/pageComponent';
import Professor from '../../assests/images/png/professor.png'
import Play from '../../assests/images/png/playicon.png'
import Share from '../../assests/images/png/share-blu.png'
import Label from '../../assests/images/png/label-blu.png'
import Title from '../../components/title/title';
import TopNotch from '../../components/statusBar/statusBar' 
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import blueLine from '../../assests/images/png/line.png';
import Alarm from '../../assests/images/png/alarm.png';
import Intersection from '../../assests/images/png/intersection.png';
import { studentchapterDetailsAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
import Video from 'react-native-video';
import { WebView } from 'react-native-webview'
import PlayVideo from '../playVideo/playVideo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class QuizView extends Component {
    constructor() {
        super(); 
        this.state = {   
        }    
    }
    handleButtonPress = (quiz) => {
        this.props.navigation.navigate('Quiz',{quiz:quiz})
    }
    render() { 
        const { studentchapterDetailsData} = this.props.authStateData
        const quiz = studentchapterDetailsData.quiz;
        console.log('quiz',quiz)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                    <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:30}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                    <Text style={styles.name} numberOfLines={1}>Chapter Quiz</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                    <View style={{marginBottom:20}}>
                        <Text style={styles.chap_video}>Quiz</Text>
                            <TouchableOpacity style={{bottom:48,left:Platform.OS === 'ios'?275:290}}>
                                <BackButton back_txt={{color:'#fff',fontSize:18,marginTop:Platform.OS === 'ios'?-25:-27}} icon={{color:'#fff'}} OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                            </TouchableOpacity> 
                    </View>
                <View style={styles.subContainer}>
                        <ScrollView contentContainerStyle={{marginLeft:25,paddingBottom:1000}}>
                            {quiz && quiz.map((item,id) => {
                                console.log('passing quiz',quiz)
                                return(
                                    <View key={id}>
                                    <View style={{flexDirection:'row'}}>
                                    <Text style={styles.txt} numberOfLines={3}>
                                    Always there is a problem, and I have to solve the problem. It's almost like research is sort of in a quiz          
                                    </Text>
                                    <TouchableOpacity  style={{width:55,height:55,marginTop:35,marginLeft:Platform.OS === 'ios'?-70:-70,borderWidth:1,backgroundColor:'red',borderColor:'red',borderRadius:30}} onPress={() => this.handleButtonPress(quiz[id])}>

                                        <MaterialCommunityIcons size={40} name={'chevron-right'} style={{color:'#fff',marginLeft:7,marginTop:5}}/>

                                    </TouchableOpacity>
                                    </View> 
                                    <View style={{borderColor:'#f2f2f2',borderWidth:1,marginTop:20,marginRight:30}}/> 
                                    
                                    </View>
                                )
                            })}
                        </ScrollView>
                </View>
                </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?1:1,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        marginTop:-50
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    txt:{
        color:'#0f1010',
        fontWeight:'400',
        fontSize:18,
        fontFamily:'AvenirLTStd-Roman',
        paddingRight:100,
        lineHeight:25,
        marginTop:25
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginRight:25,
        marginTop:-20,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20
    },
    image:{
        width:'100%',
        height:'115%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    video: {
        alignSelf: 'center',
        width: 320,
        height: 200,
      },
    chap_video:{
        color:'#fff',
        marginTop:15,
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        marginLeft:Platform.OS === 'ios'?20:22
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(QuizView);