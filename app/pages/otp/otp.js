import React, { Component } from 'react';
import {SafeAreaView,View,ImageBackground, StyleSheet,BackHandler,Dimensions,Text, Platform, TouchableOpacity} from 'react-native';
import Forgot_bg from '../../assests/images/png/forgotPassword_bg.png'
import Forgot_img from '../../assests/images/png/forgotPassword_img.png'
import BackButton from '../../components/backbutton/backbutton';
import ImageField from '../../components/images/images';
import TopNotch from '../../components/statusBar/statusBar';
import Title from '../../components/title/title';
import OTPField from 'react-native-otp-field';
import CustomButton from '../../components/customButton/customButton';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { codeAPI,resetPwdAPI,authAction } from '../../store/action/authAction';
import { connect } from "react-redux";
import { ScrollView } from 'react-native-gesture-handler';

class Otp extends Component {
    constructor() {
        super()
        this.state = {
            emailOtp: '',
            maskedEmailOtp: '',
        };
        this.backAction = this.backAction.bind(this);    
    }

    backAction = () => {
        this.props.navigation.navigate('ForgotPassword')
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
      }
    
    componentDidMount = async () => {
        BackHandler.addEventListener("hardwareBackPress", this.backAction);
    }
    handleSetEmailOtp = (otp) => {
        let newArr = [];
        let newOtp = [...this.state.emailOtp];

        for (let i = 0; i < otp.length; i++) {
            newArr.push('꘎')
        }
        if (otp.slice(-1) === '꘎') {
            newOtp.pop()
        } else if (otp.slice(-1) === '') {
            newOtp = []
        } else {
            newOtp.push(otp.slice(-1))
        }

        this.setState({ emailOtp: newOtp.join(''), maskedEmailOtp: newArr.join('') })
    }
    emailVerified = () => {
        const { dispatch } = this.props;
        const { email } = this.props.authStateData;
        dispatch(authAction({otpLoader:false,otpErr:''}))
        if(this.state.emailOtp === ''){
            dispatch(authAction({otpErr:'OTP is not verified'}))
        }
        else 
        if(this.state.emailOtp !== '' && email){
            dispatch(authAction({otpLoader:true,otpErr:''}))
            dispatch(codeAPI({
                emailOtp: this.state.emailOtp,
                email,
                navigation:this.props.navigation
            }))
        }
        else{
            dispatch(authAction({loginLoader: false,otpErr:'OTP is not verified'}))
        }
    }
    resetPwd = () => {
        const { dispatch } = this.props;
        const { email } = this.props.authStateData
        dispatch(authAction({ loginLoader: false }))
        if (email) { 
            dispatch(resetPwdAPI({ email,navigation:this.props.navigation}))
        }
    }
    render() { 
        const { email,otpErr,otpLoader } = this.props.authStateData
        return ( 
            <SafeAreaView style={styles.container}>
                <TopNotch topNotchColor={"white"} topNotchStyle={"dark-content"}/>
                <ScrollView>
                <ImageBackground style={styles.image} source={Forgot_bg}>
                    <BackButton OnClick={() => this.props.navigation.goBack()}/>
                    
                    <View style={styles.subContainer}>
                    <ImageField imageview={styles.png} aariv_img={Forgot_img}/>
                    <Title titletxt="Verify your Email" titlestyle={styles.forgot}/>
                    <Title titletxt="Please Enter the 4 digit code sent to" titlestyle={styles.forgot_description}/>
                    <Title titletxt={email} titlestyle={[styles.forgot,{marginTop:-10,fontSize:18}]}/>
                    <View style={{marginTop:10}}>
                    <OTPField
                        length={4}
                        value={this.state.maskedEmailOtp}
                        containerStyle={styles.otp_field}
                        textFieldStyle={styles.otp_input}
                        onChange={(text) => this.handleSetEmailOtp(text)}
                    />
                </View>
                <View style={styles.forgot_Pass}>
                <TouchableOpacity onPress={() => this.resetPwd()}>
                        <Title titletxt="Resend code" titlestyle={[styles.forgot,{fontSize:15,color:'#1e266d',marginRight:65}]}/>
                    </TouchableOpacity>
                </View>
                {otpErr !== null &&
                    <Text style={{color:'red',textAlign:'center'}}>{otpErr}</Text>
                }
                    {
                     otpLoader === false ?
                     <CustomButton btntxt='VERIFY' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.emailVerified()}/>
                   :
                   <ActivityLoader size={15}/>  
                  } 
                    </View> 
                </ImageBackground>
                </ScrollView>
            </SafeAreaView>
         );
    }
}
 const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff'
        },
    image:{
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
     },
    png:{
        width:190,
        height:180,
        marginTop:-80,
        alignSelf:'center'
    },
    subContainer:{
        width:'90%',
        height:Platform.OS === 'ios'?Dimensions.get('window').height/2:Dimensions.get('window').height/1.8,
        backgroundColor:'#fff',
        marginLeft:20,
        marginVertical:100,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    forgot_Pass: {
        flexDirection:'row',
        justifyContent:'flex-end',
        // marginTop:-10,
        right:Platform.OS === 'ios'?-30:-20
    },
    otp_field: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor:"#fff",
        alignItems: "center",
        justifyContent: "space-evenly",
    },
    otp_input: {
        color: 'black',
        textAlign: "center",
        fontSize:32,
        fontWeight: '500',
        paddingBottom:0,
        lineHeight:Platform.OS === 'ios'?0:30
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20
    },
    loginbutton:{
        height:40,
        marginTop:5,
        alignSelf:'center',
        width:Dimensions.get("window").width / 2.4
    },
    logintxt:{
        fontSize:18,
        paddingTop:Platform.OS === 'ios'?9:8,
        paddingBottom:10,
        fontFamily:'Montserrat-SemiBold'
    },
    forgot:{
        marginLeft:10,
        fontFamily: 'AVENIRLTSTD-BLACK',
        fontSize: 22,
        color: '#3b3c4e',
        letterSpacing:0.45,
        marginTop:30,
        textAlign:'left',
        paddingLeft:20,
        marginRight:25,
        paddingBottom:15,
        lineHeight:25,
        fontWeight:'bold'
    },
    forgot_description:{
        marginLeft:10,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 15,
        color: '#3b3c4e',
        letterSpacing:0.45,
        textAlign:'left',
        fontWeight:'400',
        paddingLeft:20,
        marginRight:65,
        paddingBottom:15,
        lineHeight:25,
    }
 })
 const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(Otp);