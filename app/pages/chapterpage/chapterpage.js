import React, { Component } from 'react';
import { SafeAreaView,View,ScrollView,Text,TouchableOpacity,FlatList,StyleSheet, Platform,BackHandler } from 'react-native';
import PageComponent from '../../components/pageComponent/pageComponent';
import SubjectBox from '../../components/subjectbox/subjectbox';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import Chapter1 from '../../assests/images/png/chapter1.png';
import Chapter2 from '../../assests/images/png/chapter2.png'
import Icon3 from '../../assests/images/png/icon3.png'
import Icon4 from '../../assests/images/png/icon4.png'
import Icon6 from '../../assests/images/png/icon6.png'
import TopNotch from '../../components/statusBar/statusBar'
import Video_icon from '../../assests/images/png/video_icon.png'
import Pdf_icon from '../../assests/images/png/pdf_icon.png'
import { studentchapterDetailsAPI,studentsubjectAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'

class ChapterPage extends Component {
    constructor() {
        super(); 
        this.state = {   
        }        
    }
    goChapterDetailsPage = async (chapter_id) => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(studentchapterDetailsAPI({
            userId,
            chapter_id,
            navigation:this.props.navigation
        }))
    }
    render() { 
        const { studentchapterData,studentsubjectData,profileData } = this.props.authStateData
        // console.log('studentchapterData',studentsubjectData)
        return ( 
            <>
            <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                <PageComponent name={profileData.full_name}  chapters="Chapters"  chapter_icon_style={styles.chapter_icon_style} chapter_icon={Pdf_icon}
                OnClick={() => this.props.navigation.navigate('Notification')}
                OnNav={() => this.props.navigation.openDrawer()}/>
                <View style={styles.subContainer}>
                        <ScrollView>
                        <View style={{marginLeft:40,marginBottom:30,marginTop:Platform.OS === 'ios'?0:10}}>
                                <Title titlestyle={styles.title_style} titletxt='My Chapters'/>
                                {/* <Title titletxt="LIBRARY" titlestyle={styles.button_txt} /> */}
                            </View>
                            <Title titletxt="Choose Chapter" titlestyle={styles.choose_chap} />
                                <View style={{justifyContent:'space-around'}}>
                                    {studentchapterData && studentchapterData.length > 0 ?
                                <FlatList
                                    data={studentchapterData}
                                    numColumns={3}
                                    keyExtractor={(item,id) => id }
                                    renderItem={(item,id) => 
                                        <SubjectBox key={id} Icon={{uri:item.item.image}} subject_txt={item.item.name} OnClick={() => this.goChapterDetailsPage(item.item.chapter_id)}/> 
                                    }
                                />
                                :
                                <Text>No Subjects</Text>
                                }
                                </View>
                                {/* <SubjectBox Icon={Chapter1} subject_txt="Under Standing Numbers" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                                <SubjectBox Icon={Chapter2} subject_txt="Representing Numbers" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                                <SubjectBox Icon={Icon3} subject_txt="Mathematics" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/> */}
                            
                            {/* <View style={{flexDirection:'row',justifyContent:'space-evenly',marginBottom:20}}>
                                <SubjectBox Icon={Icon4} subject_txt="Exam-Mock" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                                <SubjectBox Icon={Chapter2} subject_txt="Representing Numbers" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                                <SubjectBox Icon={Icon6} subject_txt="Quiz" OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                            </View> */}
                        </ScrollView>
                </View>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?2.8:2.8,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        // marginTop:-20
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:30,
        left:-20
    },
    // button:{
    //     backgroundColor:'#fe4155',
    //     width:100,
    //     height:30,
    //     borderRadius:10,
    //     marginTop:25,
    //     right:-20
    // },
    button_txt:{
        color:'#fff',
        fontSize:15,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?9:6,
        letterSpacing:0.6,
        backgroundColor:'#fe4155',
        width:100,
        height:30,
        borderRadius:10,
        marginTop:25,
        right:-20,
        textAlign:'center'
    },
    choose_chap:{
        color:"#100706",
        fontSize:18,
        marginLeft:20,
        marginBottom:20,
        fontFamily:'AvenirLTStd-Roman',
    },
    chapter_icon_style:{
        width:30,
        height:30,
        marginLeft:20,
        marginTop:10,
        marginBottom:10
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(ChapterPage);