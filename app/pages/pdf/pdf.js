import React, { Component } from 'react';
import { SafeAreaView,ActivityIndicator,StyleSheet} from 'react-native';
import TopNotch from '../../components/statusBar/statusBar'
import { connect } from "react-redux";
import { WebView } from 'react-native-webview';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
class PdfView extends Component {
    constructor(props) {
        super(props);
      }
    render() { 
        const baseUrl = "http://docs.google.com/gview?embedded=true&url="
        const source = {uri : `${baseUrl}${this.props.route.params.pdf}`}
        console.log('text',source)
        return (
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {/* {loginLoader === false? */}
                    <WebView
                        source={ source } 
                        style={{width:'100%',height:'100%'}}
                        startInLoadingState={true}
                        renderLoading={
                                ()=> {
                                return (<ActivityLoader /> )
                            }
                        }
                    />
                   {/* :
                    <ActivityLoader/>
            } */}
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(PdfView);