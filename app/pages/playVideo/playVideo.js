import React, { Component } from 'react';
import { SafeAreaView,View,Text,BackHandler,StatusBar,StyleSheet,width,TouchableWithoutFeedback, Platform } from 'react-native';
import BackButton from '../../components/backbutton/backbutton';
import PageComponent from '../../components/pageComponent/pageComponent';
import ViewDocument from '../../components/view_documents/view_documents';
import TopNotch from '../../components/statusBar/statusBar'
import { studentClassesAPI,studentSingleClassAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { WebView } from 'react-native-webview'
import Video from 'react-native-video'
import Orientation from 'react-native-orientation';
import VideoPlayer from 'react-native-video-controls'
import { DrawerItem } from '@react-navigation/drawer';
import ChapterVideo from '../chaptervideo/chapterVideo';


class PlayVideo extends Component {
    constructor() {
        super(); 
        this.state = {
            // OrientationStatus : '',
            // Height_Layout : '',
            // Width_Layout : '',
            fullScreen:false,
            }
        this.handleVideoPage = this.handleVideoPage.bind(this);       
    }
    componentDidMount = async () => {
        BackHandler.addEventListener('deviceExit', this.handleVideoPage);
    }   
    componentWillUnmount(){
    BackHandler.removeEventListener('deviceExit', this.handleVideoPage);
    }
    handleVideoPage=()=>{
    this.props.navigation.navigate('ChapterVideo')
    return true;
    }
    // _onOrientationDidChange = (orientation) => {
    //     if (orientation == 'PORTRAIT') {
    //       Orientation.lockToLandscape();
    //     }
    //   };
      
    //   componentDidMount() {
    //     // StatusBar.setHidden(true);
    //     Orientation.lockToLandscape();
    //     Orientation.addOrientationListener(this._onOrientationDidChange);
    //   }
      
    //   componentWillUnmount() {
    //     Orientation.unlockAllOrientations()
    //     Orientation.removeOrientationListener(this._onOrientationDidChange);
    //   }
    handleExitFullScreen = () => {
        this.setState({
          fullScreen: false
        });
      };
    render() { 
        const { studentchapterDetailsData } = this.props.authStateData
        const source = this.props.route.params.video;
        console.log('text',source)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                {/* <Text>{text}</Text> */}
                    <WebView 
                        source={{uri : source}}
                        resizeMode='cover'
                        allowsFullscreenVideo={true}
                        domStorageEnabled={false}
                        allowsBackForwardNavigationGestures={true}
                        mediaPlaybackRequiresUserAction={true}
                        injectedJavaScript={`document.getElementsByTagName("video")[0].controlsList="nodownload";`}
                    />
                    {/* <VideoPlayer
                        source={source}
                        toggleResizeModeOnFullscreen={true}
                        seekColor='orange'
                        tapAnywhereToPause={true}
                        disableBack
                        disableVolume
                        onExitFullscreen={this.handleExitFullScreen}
                        fullscreen={true}
                        style={{ width: "100%", height:'100%',backgroundColor:'#000'}}
                    /> */}
                    {/* <Video
                        source={source}
                        style={{ width: "100%", height:'100%',backgroundColor:'#fff'}}
                        controls={true}
                        rate={1.0}
                        volume={1.0}
                        muted={false}
                        resizeMode={'contain'}
                        playWhenInactive={false}
                        playInBackground={false}
                        ignoreSilentSwitch={'ignore'}
                    /> */}
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    error:{
        backgroundColor:'black'
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(PlayVideo);

// import React, {useState, useRef} from 'react';

// // import all the components we are going to use
// import {SafeAreaView, StyleSheet, Text, View} from 'react-native';

// //Import React Native Video to play video
// import Video from 'react-native-video';

// //Media Controls to control Play/Pause/Seek and full screen
// import
//   MediaControls, {PLAYER_STATES}
// from 'react-native-media-controls';
// import { connect } from "react-redux";

// const PlayVideo = () => {
//   const videoPlayer = useRef(null);
//   const [currentTime, setCurrentTime] = useState(0);
//   const [duration, setDuration] = useState(0);
//   const [isFullScreen, setIsFullScreen] = useState(false);
//   const [isLoading, setIsLoading] = useState(true);
//   const [paused, setPaused] = useState(false);
//   const [
//     playerState, setPlayerState
//   ] = useState(PLAYER_STATES.PLAYING);
//   const [screenType, setScreenType] = useState('content');

//   const onSeek = (seek) => {
//     //Handler for change in seekbar
//     videoPlayer.current.seek(seek);
//   };

//   const onPaused = (playerState) => {
//     //Handler for Video Pause
//     setPaused(!paused);
//     setPlayerState(playerState);
//   };

//   const onReplay = () => {
//     //Handler for Replay
//     setPlayerState(PLAYER_STATES.PLAYING);
//     videoPlayer.current.seek(0);
//   };

//   const onProgress = (data) => {
//     // Video Player will progress continue even if it ends
//     if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
//       setCurrentTime(data.currentTime);
//     }
//   };

//   const onLoad = (data) => {
//     setDuration(data.duration);
//     setIsLoading(false);
//   };

//   const onLoadStart = (data) => setIsLoading(true);

//   const onEnd = () => setPlayerState(PLAYER_STATES.ENDED);

//   const onError = () => alert('Oh! ', error);

//   const exitFullScreen = () => {
//     alert('Exit full screen');
//   };

//   const enterFullScreen = () => {};

//   const onFullScreen = () => {
//     setIsFullScreen(isFullScreen);
//     if (screenType == 'content') setScreenType('cover');
//     else setScreenType('content');
//   };

//   const renderToolbar = () => (
//     <View>
//       <Text style={styles.toolbar}> toolbar </Text>
//     </View>
//   );

//   const onSeeking = (currentTime) => setCurrentTime(currentTime);

//   return (
//     <View style={{flex: 1}}>
//       <Video
//         onEnd={onEnd}
//         onLoad={onLoad}
//         onLoadStart={onLoadStart}
//         onProgress={onProgress}
//         paused={paused}
//         ref={videoPlayer}
//         resizeMode={screenType}
//         // onFullScreen={isFullScreen}
//         source={{
//           uri:
//           "http://dev.arokee.com/aslam/Aariv/wp-content/themes/aariv/videos/dummy2.mp4",
//         }}
//         style={styles.mediaPlayer}
//         volume={10}
//       />
//       <MediaControls
//               isFullScreen={isFullScreen}
//         duration={duration}
//         isLoading={isLoading}
//         mainColor="orange"
//         onFullScreen={onFullScreen}
//         onPaused={onPaused}
//         onReplay={onReplay}
//         onSeek={onSeek}
//         onSeeking={onSeeking}
//         playerState={playerState}
//         progress={currentTime}
//         toolbar={renderToolbar()}
//       />
//     </View>
//   );
// };

// const mapStateToProps = state => ({
//     authStateData: state.authState,
//  })
//  export default connect(mapStateToProps)(PlayVideo);

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   toolbar: {
//     marginTop: 30,
//     backgroundColor: 'white',
//     padding: 10,
//     borderRadius: 5,
//   },
//   mediaPlayer: {
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     bottom: 0,
//     right: 0,
//     width:'100%',
//     height:'100%',
//     backgroundColor: '#fff',
//     // justifyContent: 'center',
//   },
// });