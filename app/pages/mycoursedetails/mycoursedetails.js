import React, { Component } from 'react';
import { SafeAreaView,View,ScrollView,Text,FlatList,TouchableOpacity,StyleSheet,BackHandler } from 'react-native';
import PageComponent from '../../components/pageComponent/pageComponent';
import SubjectBox from '../../components/subjectbox/subjectbox';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import Icon1 from '../../assests/images/png/icon1.png';
import Icon2 from '../../assests/images/png/icon2.png'
import Icon3 from '../../assests/images/png/icon3.png'
import Icon4 from '../../assests/images/png/icon4.png'
import Icon5 from '../../assests/images/png/icon5.png'
import Icon6 from '../../assests/images/png/icon6.png'
import BookImage from '../../components/bookImage/bookImage';
import Slider from '../../components/slider/slider';
import TopNotch from '../../components/statusBar/statusBar'
import { studentchapterAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
class MyCourseDetails extends Component {
    constructor() {
        super(); 
        this.state = {   
        }       
    }
    gotoChapterPage = async (name) =>{
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(authAction({studentchapterLoader:true}))
        dispatch(studentchapterAPI({
            userId,
            name,
            navigation:this.props.navigation
        }))
    }
    render() { 
        const { studentsubjectData,studentchapterLoader,profileData } = this.props.authStateData
        // console.log('studentsubjectData',studentsubjectData)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {studentchapterLoader === false?
            <>
                    <PageComponent name={profileData.full_name}
                OnClick={() => this.props.navigation.navigate('Notification')}
                OnNav={() => this.props.navigation.openDrawer()}/>
                <View style={styles.subContainer}>
                        <ScrollView>
                        <View style={{marginLeft:40,marginBottom:30,marginTop:Platform.OS === 'ios'?0:10}}>
                                <Title titlestyle={styles.title_style} titletxt='My Subjects'/>
                                {/* <Title titletxt="CLASSES" titlestyle={styles.button_txt} /> */}
                            </View>
                            <Title titletxt="Choose Subject" titlestyle={styles.choose_chap} />
                            <View style={{justifyContent:'space-around'}}>
                                    {studentsubjectData && studentsubjectData.length > 0 ?
                                <FlatList
                                    data={studentsubjectData}
                                    numColumns={3}
                                    keyExtractor={(item,id) =>id }
                                    renderItem={(item,id) => 
                                        <SubjectBox key={id} Icon={{uri:item.item.image}} subject_txt={item.item.name} OnClick={() => this.gotoChapterPage(item.item.name)}/> 
                                    }
                                />
                                :
                                <Text>No Subjects</Text>
                                }
                                </View>
                            {/* <View style={{flexDirection:'row',justifyContent:'space-evenly',marginBottom:20}}>
                                <SubjectBox Icon={Icon4} subject_txt="Exam-Mock" OnClick={() => this.props.navigation.navigate('ChapterPage')}/>
                                <SubjectBox Icon={Icon5} subject_txt="Chemistry" OnClick={() => this.props.navigation.navigate('ChapterPage')}/>
                                <SubjectBox Icon={Icon6} subject_txt="Quiz" OnClick={() => this.props.navigation.navigate('ChapterPage')}/>
                            </View> */}
                            <BookImage book_image={{marginTop:20}}/>
                            <Slider/>
                        </ScrollView>
                </View>
                </>
                :
                <ActivityLoader/>
                        }
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?3.4:3.2,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:30,
        left:-20
    },
    // button:{
    //     backgroundColor:'#fe4155',
    //     width:100,
    //     height:30,
    //     borderRadius:10,
    //     marginTop:25,
    //     right:-20
    // },
    button_txt:{
        color:'#fff',
        fontSize:15,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?9:6,
        letterSpacing:0.6,
        backgroundColor:'#fe4155',
        width:100,
        height:30,
        borderRadius:10,
        marginTop:25,
        right:-20,
        textAlign:'center'
    },
    choose_chap:{
        color:"#100706",
        fontSize:18,
        marginLeft:20,
        marginBottom:20,
        fontFamily:'AvenirLTStd-Roman',
    },
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(MyCourseDetails);