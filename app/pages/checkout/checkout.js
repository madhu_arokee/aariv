import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,height,ScrollView,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../../components/images/images';
import Alarm from '../../assests/images/png/alarm.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import Textfield from '../../components/textfield/textfield';
import TopNotch from '../../components/statusBar/statusBar';
import { billingDetailsAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'

class Checkout extends Component {
    constructor(props){
        super(props);
        this.state ={
            firstname:'',
            lastname:'',
                mobile:'',
                streetaddress:'',
                postcode:'',
                city:'',
                state:'',
                country:'',
        }
    }
    checkAlpha = (name) =>{
        return !!name.match("^(?=.*[a-z])(?=.*[A-Z])");
    }
    billingDetails = async() => {
        const { dispatch } = this.props;
        const { firstname,lastname,mobile,streetaddress,postcode,city,state,country } = this.state;
        const { email } = this.props.authStateData;
        let checkName = this.checkAlpha(firstname);
        let checkLastName = this.checkAlpha(lastname);
        let checkCountry = this.checkAlpha(country);
        let checkStreet = this.checkAlpha(streetaddress);
        let checkCity = this.checkAlpha(city);
        let checkState = this.checkAlpha(state);
        let userId = await getKey("loginData");
        if(firstname === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter First Name'}))
        }
        else if(checkName === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter First name in Capital & small Letters Only'}))
        }
        else if(lastname === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Last Name'}))
        }
        else if(checkLastName === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Last Name in Capital & small Letters Only'}))
        }
        else if(mobile === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Mobile Number'}))
        }
        else if(email === ''){
            dispatch(authAction({billingLoader:false,edetailsErr:'Please enter Email'}))
        }
        else if(country === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Country'}))
        }
        else if(checkCountry === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Country in Capital & small Letters Only'}))
        }
        else if(streetaddress === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Street Address'}))
        }
        else if(checkStreet === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter Street Name in Capital & small Letters Only'}))
        }
        else if(city === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter City'}))
        }
        else if(checkCity === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter City in Capital & small Letters Only'}))
        }
        else if(state === ''){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter State'}))
        }
        else if(checkState === false){
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter State in Capital & small Letters Only'}))
        }
        else if(postcode === ''){
            dispatch(authAction({billingLoader:false,err:'Please enter PostCode / ZIP Code'}))
        }
        else if(firstname !== '' && lastname !== '' && email !== '' && mobile !== '' && streetaddress !== ''  && city !== '' && state !== '' && country !== '' && postcode !== ''){
            dispatch(authAction({billingLoader:true,detailsErr:''}))
            dispatch(billingDetailsAPI({
                userId,
                firstname,
                lastname,
                email,
                mobile,
                streetaddress,
                postcode,
                city,
                state,
                country,
                navigation:this.props.navigation
            }))
        }
        else {
            dispatch(authAction({billingLoader:false,detailsErr:'Please enter all the Fields'}))
        }
    }
    render() { 
        const { dispatch } = this.props;
        const { detailsErr,billingLoader } = this.props.authStateData;
        return ( 
            <>
            <SafeAreaView style={styles.topSafeArea}/>
        <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
        <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                <ImageBackground source={Intersection} style={styles.image}>
                    {/* <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity> 
                    </View> */}
                        <Text style={styles.name} numberOfLines={1}>Checkout</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                
                <View style={styles.subContainer}>
                        <ScrollView contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?350:0}}>
                            <View>
                                <Title titlestyle={styles.title_style} titletxt='Billing Details'/>
                            </View>
                            <View style={styles.miniContainer}>
                                <Textfield 
                                    holder="First Name" 
                                    text_Input='black' 
                                    box_style={styles.box_style}
                                    onChangeText={value => this.setState({firstname:value})}
                                    />
                                <Textfield 
                                    holder="Last Name" 
                                    text_Input='black' 
                                    box_style={styles.box_style}
                                    onChangeText={value => this.setState({lastname:value})}
                                    />
                                <Textfield 
                                    holder="Phone Number" 
                                    text_Input='black' 
                                    box_style={styles.box_style} 
                                    keyboardType={'phone-pad'}
                                    maxLength={10}
                                    onChangeText={value => this.setState({mobile:value})}
                                    />
                                <Textfield 
                                    holder="Email ID" 
                                    text_Input='black' 
                                    box_style={styles.box_style}
                                    onChangeText={value => dispatch(authAction({email : value}))}
                                    />
                                <Textfield 
                                    holder="Country / Region" 
                                    text_Input='black' 
                                    box_style={styles.box_style}
                                    onChangeText={value => this.setState({country:value})}
                                    />
                                <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
                                    <Textfield 
                                        holder="Street Address" 
                                        text_Input='black' 
                                        box_style={styles.box_style} 
                                        txt={{width:'90%'}}
                                        onChangeText={value => this.setState({streetaddress:value})}
                                        />
                                    <Textfield 
                                        holder="Town / City" 
                                        text_Input='black' 
                                        box_style={styles.box_style} 
                                        txt={{width:'90%'}}
                                        onChangeText={value => this.setState({city:value})}
                                        />
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
                                    <Textfield 
                                        holder="State" 
                                        text_Input='black' 
                                        box_style={styles.box_style} 
                                        txt={{width:'90%'}}
                                        onChangeText={value => this.setState({state:value})}
                                        />
                                    <Textfield 
                                        holder="PostCode / ZIP *" 
                                        text_Input='black' 
                                        box_style={styles.box_style} 
                                        keyboardType={'phone-pad'} 
                                        txt={{width:'90%'}}
                                        maxLength={6}
                                        onChangeText={value => this.setState({postcode:value})}
                                        />
                                </View>
                                {detailsErr !== null &&
                                <Text style={{color:'red',textAlign:'center',marginTop:15}}>{detailsErr}</Text>}
                                {
                     billingLoader === false ?
                     <TouchableOpacity>
                         <CustomButton btntxt="Next" touchtext={styles.button_txt} touchview={styles.button} OnClick={() => this.billingDetails()}/>
                     </TouchableOpacity>
                   :
                   <ActivityLoader size={15} style={{marginTop:50,marginBottom:50}}/>  
                  }
                                
                            </View>
                        </ScrollView>
                    </View>
                    </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'100%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:50
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:50,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    subContainer:{
        flex:3,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    miniContainer:{
        width:'90%',
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:30,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    box_style:{
        borderColor:'#fff',
        borderBottomColor:'#f2f2f2',
        width:'90%',
        marginLeft:10,
        marginTop:10,
        fontFamily:'AvenirLTStd-Roman'
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:50,
        marginLeft:30
    },
    button_txt:{
        color:'#fff',
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?15:12,
        letterSpacing:0.6,
    },
    button:{
        width:'90%',
        height:50,
        borderRadius:5,
        marginTop:40,
        marginLeft:20,
        marginBottom:30
    },
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(Checkout);