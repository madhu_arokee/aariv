import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,BackHandler,height,ScrollView,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../../components/images/images';
import Alarm from '../../assests/images/png/alarm.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import CardImage from '../../assests/images/png/cardimage.png' 
import TopNotch from '../../components/statusBar/statusBar'
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { getStudentCartAPI,removeClassToCartAPI } from '../../store/action/authAction';
import { getKey } from '../../helpers/helpers'
import { connect } from "react-redux";
import BurgerMenu from '../../assests/images/png/burgurmenu.png';

class Cart extends Component {
    constructor() {
        super(); 
        this.state = {   
        }       
    }
    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(getStudentCartAPI({userId}))
    }
    removeFromCart = async (cart_key) => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(removeClassToCartAPI({
            cart_key,
            userId,
            navigation:this.props.navigation
        }))
    }
    render() { 
        const { cartData,loader,removeErr } = this.props.authStateData
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {loader === false?
                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>My Cart</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                        <View style={styles.subContainer}>
                            
                            <ScrollView contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?350:0}}>
                            { cartData.product_details && cartData.product_details.length > 0 ?
                              cartData.product_details && cartData.product_details.map((item,id)=> {
                                        return(
                                            <>
                                            <View key={id}>
                            <ImageField imageview={styles.imageView} aariv_img={{uri:item.product_image}}/>
                            <View style={styles.semicontainer}>
                                <View style={{marginLeft:30,marginBottom:10}}>
                                    <Title titlestyle={styles.txt} linenoTxt={2} titletxt={item.product_name}/>
                                    <Title titlestyle={styles.amount_txt} titletxt={item.price}/>               
                                </View>
                            </View>
                            
                            <TouchableOpacity style={{flexDirection:'row',justifyContent:'flex-end',bottom:90,right:40,elevation:5}}>
                                <CustomButton btntxt="X" touchtext={styles.cross_txt} touchview={styles.cross_style} OnClick={() => this.removeFromCart(item.cart_key)}/>
                            </TouchableOpacity> 
                            
                            </View>
                            </>
                               )
                            })  
                            :
                            <Text style={{textAlign:'center',marginTop:200,fontSize:22,color:'red'}}>Cart is Empty</Text> 
                            } 
                            </ScrollView>
                             {cartData.product_details && cartData.product_details.length > 0 ?
                            <View style={styles.minicontainer}>
                            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                            <Title titlestyle={styles.amount} titletxt="Total:"/>
                            <Title titlestyle={styles.amount} titletxt={cartData.cart_total}/>
                            </View>
                            <CustomButton btntxt="Checkout" touchtext={styles.button_txt} touchview={styles.button} OnClick={() => this.props.navigation.navigate('Checkout')}/>
                            </View>
                            :
                            null }
                                                        
                        </View>
                    </ImageBackground>
                    :
                    <ActivityLoader/>
                    } 
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'100%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:50
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        // marginTop:-30,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    subContainer:{
        flex:3,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    semicontainer:{
        width:'80%',
        backgroundColor:'#fff',
        marginLeft:50,
        marginTop:-50,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        borderRadius:10,
        zIndex:Platform.OS === 'ios'?-1:0
    },
    minicontainer:{
        width:'90%',
        height:140,
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:30,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        marginBottom:30
    },
    amount:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        color:'#262626'
    },
    amount_txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        color:'#fe4155',
        lineHeight:35,
        paddingLeft:50,
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        color:'#1e266d',
        lineHeight:25,
        paddingLeft:50,
        marginTop:10,
        marginRight:40
    },
    cross_txt:{
        color:'#fff',
        fontSize:25,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?7:2,
        paddingLeft:Platform.OS === 'ios'?2:1,
        letterSpacing:0.6
    },
    button_txt:{
        color:'#fff',
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?15:12,
        letterSpacing:0.6
    },
    cross_style:{
        width:'80%',
        height:35,
        borderRadius:5,
        marginLeft:30,
        backgroundColor:'#da2c2c'
    },
    button:{
        width:'85%',
        height:50,
        borderRadius:5,
        marginTop:20,
        marginLeft:30,
        marginBottom:30
    },
    imageView:{
        width:90,
        height:90,
        marginLeft:20,
        marginTop:20,
        marginBottom:-20,
        elevation:10
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })

 export default connect(mapStateToProps)(Cart);