import React, { Component } from 'react';
import { SafeAreaView,Text,StyleSheet,Dimensions,ScrollView,View,BackHandler,ImageBackground,TouchableOpacity,Platform } from 'react-native';
import CustomButton from '../../components/customButton/customButton';
import TopNotch from '../../components/statusBar/statusBar'
import Title from '../../components/title/title';
import Alarm from '../../assests/images/png/alarm.png';
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import logo from '../../assests/images/png/logo.png';
import login_image from '../../assests/images/png/login_image.png'
import ImageField from '../../components/images/images';
import Textfield from '../../components/textfield/textfield';
import BackButton from '../../components/backbutton/backbutton';
import register_image from '../../assests/images/png/registerpage.png';
import mailIcon from '../../assests/images/png/mail.png';
import pwdIcon from '../../assests/images/png/pwdicon.png';
import student from '../../assests/images/png/student.png';
import location from '../../assests/images/png/home.png';
import Class from '../../assests/images/png/class.png';
import parent from '../../assests/images/png/family.png';
import calender from '../../assests/images/png/calendar.png';
import phone from '../../assests/images/png/telephone.png'
import Ionicons from 'react-native-vector-icons/Ionicons'
import DatePicker from 'react-native-datepicker'
import Intersection from '../../assests/images/png/intersection.png';
import moment from 'moment'
import PageComponent from '../../components/pageComponent/pageComponent'
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { studentProfileAPI,authAction, changePasswordAPI } from '../../store/action/authAction';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'

class ProfilePage extends Component {
    constructor(props){
        super(props);
        this.state ={
            currentPassword:true,
            newPassword:true,
            ConfirmPassword:true,
            curr_password:'',
            n_password:'',
            C_password:'',
            // firstNameState:'',
            // email:'',
            // phone:'',
            // class:"",
            // parent:'',
            // address:'',
            // parent_pan_number:''
        } 
    }
componentDidMount = async () => {
    const { dispatch } = this.props
    let userId = await getKey("loginData");
    dispatch(studentProfileAPI({userId}))
}
    currentPasswordVisibility = () =>{
        this.setState({currentPassword:!this.state.currentPassword})
    }
    newPasswordVisibility = () =>{
        this.setState({newPassword:!this.state.newPassword})
    }
    confirmPasswordVisibility = () =>{
        this.setState({ConfirmPassword:!this.state.ConfirmPassword})
    }
    checkPassword = (password) =>{
        return !!password.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{6,10})");
    }

    handleSubmit = async () => {
        const { n_password,C_password,curr_password } = this.state;
        const { dispatch } = this.props;
        let userId = await getKey("loginData");
        dispatch(authAction({passwordLoader:false,password_Err_Msg:''}))
        let checkPwdStrength = this.checkPassword(n_password);
        let checkConfirmPwdStrength = this.checkPassword(C_password);
        if(curr_password === '' && n_password === '' && C_password === ''){
            this.props.navigation.navigate('Home')
        }
        else if(curr_password === ''){
            dispatch(authAction({passwordLoader:false,password_Err_Msg : "Please enter Current Password"}))
        }
        else if(n_password === ''){
            dispatch(authAction({passwordLoader:false,password_Err_Msg : "Please enter New Password"}))
        }
        else if(C_password === ''){
            dispatch(authAction({passwordLoader:false,password_Err_Msg : "Please enter Confirm Password"}))
        }
        else if (checkPwdStrength === false) { 
            dispatch(authAction({ passwordLoader:false,password_Err_Msg: '6-10 characters,At least one upper case and lower case character and special Charater' })) 
        }
        else if (checkConfirmPwdStrength === false) { 
            dispatch(authAction({passwordLoader:false, password_Err_Msg: '6-10 characters,At least one upper case and lower case character and special Charater' })) 
        }
        else if(n_password !== C_password){
            dispatch(authAction({passwordLoader:false,password_Err_Msg : "Password Doesn't Match"}))
        }
        else if(curr_password !== '' && n_password !== '' && C_password !== ''  && (n_password === C_password) ){
                dispatch(authAction({passwordLoader:true,password_Err_Msg:''}))
                dispatch(changePasswordAPI({
                    userId,
                    n_password,
                    C_password,
                    curr_password,
                    navigation:this.props.navigation
                }))
        }
        else{
            dispatch(authAction({password_Err_Msg : 'Please check all the fields'}))
        }
    }
    render() { 
        const { password_Err_Msg,passwordLoader,docSuccessMessage,student,email,phone,standard,PAN_number,dateUnique,address,profileData } = this.props.authStateData
        return ( 
            <>
            <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={styles.container}>
                <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                    {/* <View style={{marginBottom:30}}> */}
                        {/* <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity> */}
                        {/* <BackButton back_txt={{color:'#fff'}} icon={{color:'#fff'}}/> */}
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    {/* </View> */}
                    </View>
                <View style={{marginTop:0,marginLeft:10,flexDirection :'row'}}>
                    {/* <View style={styles.border_img}>
                        <ImageField aariv_img={parent} imageview={{width:50,height:50,alignSelf:'center'}}/>
                    </View> */}
                    <View style={{flexDirection:'column',marginLeft:18,marginBottom:Platform.OS === 'ios'?0:40}}>
                        <Text style={styles.nameText}>{profileData !== ''?profileData.full_name: ''}</Text>
                            <TouchableOpacity>
                                <Text style={styles.linkStyles}>Class  {profileData !== ''?profileData.class:''}</Text>
                            </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.subContainer}>
                    <ScrollView contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?220:0}}>
                        <Title titlestyle={styles.title_style} titletxt='Personal Information'/>
                        <Title titlestyle={styles.student_reg} titletxt='Full Name'/>
                        <View style={styles.box_style}>
                            {/* <Textfield
                                // value={firstNameState}
                                value={student}
                                text_Input='#3b3c4e'
                                showSoftInputOnFocus={false}
                                box_style={[styles.box_style]}
                                // onChangeText={value => this.setState({ firstNameState : value})}
                                // onChangeText={(text) => this.handleFirstName(text)}
                                onChangeText={value => dispatch(authAction({ student : value}))}
                            /> */}
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.full_name:''}/>
                         </View>
                        
                        <Title titlestyle={styles.student_reg} titletxt='Email ID'/>
                        {/* <View>
                            <Textfield
                                value={email}
                                text_Input='#3b3c4e'
                                showSoftInputOnFocus={false}
                                box_style={styles.box_style}
                                onChangeText={value => dispatch(authAction({ email: value}))}
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.email:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='Phone Number'/>
                        {/* <View>
                            <Textfield
                                value={phone}
                                text_Input='#3b3c4e'
                                showSoftInputOnFocus={false}
                                box_style={styles.box_style}
                                maxLength={10}
                                onChangeText={value => dispatch(authAction({ phone: value}))}
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.phone:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='Class'/>
                        {/* <View>
                            <Textfield
                                value={standard}
                                 text_Input='#3b3c4e'
                                 showSoftInputOnFocus={false}
                                 box_style={styles.box_style}
                                 onChangeText={value => dispatch(authAction({ standard: value}))}                           
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.class:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='Date Of Birth'/>
                        {/* <View style={{marginVertical: 10}}>
                        <DatePicker
                                style={{ width: '92%'}}
                                placeholder={`${dateUnique}`}
                                mode="date"
                                maxDate={new Date()}
                                // format="DD-MM-YYYY"
                                showIcon={false}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: [styles.dateInputDiv],                           
                                    placeholderText: [styles.dateTextDiv]
                                }}
                                onDateChange={(date) => dispatch(authAction({dateUnique: date}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={calender}/>}
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.date_of_birth:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='Parent Name'/>
                        {/* <View style={{marginVertical: 5}}>
                            <Textfield
                            value={parent}
                            showSoftInputOnFocus={false}
                                text_Input='#3b3c4e'
                                box_style={styles.box_style}
                                onChangeText={value => dispatch(authAction({ parent : value}))}
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.parent:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='Address'/>
                        {/* <View>
                            <Textfield
                                value={address}
                                 text_Input='#3b3c4e'
                                 showSoftInputOnFocus={false}
                                 box_style={styles.box_style}
                                 multiline={true}
                                 txt={{width:'90%'}}
                                 onChangeText={value => dispatch(authAction({ address : value}))}                          
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.address:''}/>
                         </View>
                        <Title titlestyle={styles.student_reg} titletxt='PAN Number'/>
                        {/* <View style={{marginVertical: 5}}>
                            <Textfield
                            value={PAN_number}
                                text_Input='#3b3c4e'
                                showSoftInputOnFocus={false}
                                box_style={styles.box_style}
                                onChangeText={value => dispatch(authAction({ PAN_number : value}))}
                            />
                        </View> */}
                        <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData !== ''?profileData.parent_pan_number:''}/>
                         </View>
                        <Title titlestyle={[styles.title_style,{marginTop:Platform.OS === 'ios'?30:10}]} titletxt='Change Password'/>
                        <Title titlestyle={styles.student_reg} titletxt='Current Password'/>
                        <View>
                            <Textfield
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                //  icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                 secureTextEntry={this.state.currentPassword}
                                onChangeText={value => this.setState({ curr_password: value })}
                            
                            />
                            <TouchableOpacity  onPress={() => this.currentPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.currentPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                         {/* <View style={styles.box_style}>
                            <Title titlestyle={{marginLeft:15,marginTop:10,color:'#3b3c4e'}} titletxt={profileData.password}/>
                         </View> */}
                        <Title titlestyle={styles.student_reg} titletxt='New Password'/>
                        <View>
                            <Textfield
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                //  icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                 secureTextEntry={this.state.newPassword}
                                onChangeText={value => this.setState({ n_password: value })}
                            
                            />
                            <TouchableOpacity onPress={() => this.newPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.newPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                        <Title titlestyle={styles.student_reg} titletxt='Confirm New Password'/>
                        <View style={{marginVertical:5}}>
                            <Textfield
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                //  icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                 secureTextEntry={this.state.ConfirmPassword}
                                 onChangeText={value => this.setState({ C_password: value })}
                            />
                            <TouchableOpacity onPress={() => this.confirmPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.ConfirmPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                        {password_Err_Msg !== null &&
                        <Text style={{color:'red',textAlign:'center',marginTop:5,paddingLeft:30,paddingRight:30}}>{password_Err_Msg}</Text>
                        }
                        {passwordLoader === false?
                     <CustomButton btntxt='SAVE CHANGES' touchview={styles.finish_button} touchtext={styles.finish_txt} OnClick={() => this.handleSubmit()}/>
                     :
                     <Text style={styles.successText}>{docSuccessMessage}</Text>
                        }
                   
                    </ScrollView>
                    </View>
                    </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    successText: {
        color: 'green',
        fontSize: 18,
        textAlign:'center',
        paddingBottom:40
    },
    subContainer:{
        flex:1,
        marginTop:Platform.OS === 'ios'?30:0,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    image:{
        width:'100%',
        height:'100%'
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        // marginVertical:-30,
        marginVertical:20,
        marginRight:20,
        // alignSelf:'flex-end',
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AVENIRLTSTD-BLACK',
        fontSize:22,
        marginTop:30,
        marginLeft:20,
        marginBottom:10
    },
    border_img: {
        borderRadius:50,
        resizeMode: 'cover',
        width: 70,
        height: 70,
        borderColor: '#f0eff5',
        borderWidth:1,marginLeft:10
      },
      nameText:{
        fontWeight:'bold',
        fontSize:28,
        color:'#fff',
        lineHeight: 30,
        marginTop:50,
        fontFamily:'AvenirLTStd-Roman',
        // width: 100,
        letterSpacing: 0.66,
      },
      linkStyles:{
        color: '#fff',
        fontSize: 18,
        letterSpacing: 0.03,
        fontWeight: '500',
        lineHeight: 22,
        fontFamily:'AvenirLTStd-Roman' ,
        marginTop:5
      },
    student_reg:{
        marginLeft:20,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 15,
        fontWeight: '400',
        color: '#262626',
        marginTop:Platform.OS === 'ios'?18:10,
    },
    box_style:{
        width:'90%',
        marginLeft:20,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb',
        height:45,
        borderWidth:1,
        marginTop:10
    },
    finish_button:{
        height:40,
        marginTop:15,
        marginBottom:50,
        alignSelf:'center',
        width:Dimensions.get("window").width/2.2,
    },
    dateInputDiv: {
        marginLeft:30,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb',
        borderWidth: 1,
        height: 48,
        alignItems: 'flex-start',
        paddingLeft:15,

    },
    dateTextDiv: {
        color:'#3b3c4e',
    },
    finish_txt:{
        fontSize:Platform.OS === 'ios'?16:18,
        paddingTop:Platform.OS === 'ios'?10:8,
        textAlign:'center',
        paddingBottom:10,
        fontFamily:'Montserrat-SemiBold'
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20,
        marginVertical:12
    },
    card_sectfooter: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        top: 11.5,
        left: 8
    },
    pass_show: {
        position: 'absolute',
        right: 40,
        bottom: 15,
    },
    
});
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(ProfilePage);