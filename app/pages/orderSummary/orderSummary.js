import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,height,ScrollView,BackHandler,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../../components/images/images';
import Alarm from '../../assests/images/png/alarm.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import CardImage from '../../assests/images/png/cardimage.png' 
import TopNotch from '../../components/statusBar/statusBar'
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { getStudentCartAPI,getBillingDetailsAPI, getOrdersAPI } from '../../store/action/authAction';
import { getKey } from '../../helpers/helpers'
import { connect } from "react-redux";

class OrderSummary extends Component {
    constructor() {
        super(); 
        this.state = {   
        }      
    } 
    render() { 
        const { singleOrderData,singleLoader } = this.props.authStateData
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {singleLoader === false?
                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>Order Summary</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                        <ScrollView style={styles.subContainer} contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?100:100}}>
                        <Title titlestyle={styles.title_style} titletxt='Order Details'/>
                        <View style={styles.miniContainer}>
                            {singleOrderData && singleOrderData.order_detail && singleOrderData.order_detail.length > 0 ?
                            singleOrderData && singleOrderData.order_detail && singleOrderData.order_detail.map((item,id) => {
                                return(
                                    <View key={id} style={{flexDirection:'row',justifyContent:'space-between',marginRight:30}}>
                                        <Title titlestyle={styles.billing} titletxt={item.name}/>
                                        <Title titlestyle={styles.billing} titletxt={`₹${item.total}`}/>
                                    </View>
                                    )
                            })
                        :
                        null
                        }
                                <View style={{borderColor:'#f2f2f2',borderWidth:1,marginTop:15,width:'90%',marginLeft:20}}/>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                                    <Title titletxt="Sub-Total" titlestyle={styles.amount}/>
                                    <Title  titletxt={singleOrderData !== ''?`₹${singleOrderData.order_sub_total}`:''} titlestyle={styles.amount}/>
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                                    <Title titletxt="Payment Method" titlestyle={[styles.amount,{fontSize:18}]}/>
                                    <Title  titletxt={singleOrderData !== ''?singleOrderData.payment_method:''} titlestyle={[styles.amount,{fontSize:15}]}/>
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30,marginBottom:30}}>
                                    <Title titletxt="Total to Pay" titlestyle={[styles.amount,{fontWeight:'bold'}]}/>
                                    <Title  titletxt={singleOrderData !== ''?`₹${singleOrderData.order_sub_total}`:''} titlestyle={[styles.amount,{fontWeight:'bold'}]}/>
                                </View>
                                
                        </View>
                        <Title titlestyle={styles.title_style} titletxt='Billing Address'/>
                                <View style={styles.miniContainer} >
                                    <Title titlestyle={[styles.address1,{marginTop:20}]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.name}/>
                                    <Title titlestyle={[styles.address1]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.email}/>
                                    <Title titlestyle={[styles.address1]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.phone}/>
                                    <Title titlestyle={[styles.address1]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.address_1}/>
                                    <Title titlestyle={[styles.address1]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.city}/>  
                                    <Title titlestyle={[styles.address1]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.state}/>
                                    <Title titlestyle={[styles.address1,{marginBottom:20}]}  titletxt={singleOrderData && singleOrderData.billing_address && singleOrderData.billing_address.country}/>
                                </View>
                        </ScrollView>
                    </ImageBackground>
                    :
                    <ActivityLoader/>
                        }
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'100%'
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AVENIRLTSTD-BLACK',
        fontSize:22,
        marginTop:30,
        marginLeft:30,
        marginBottom:10
    },
    billing:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:30,
        marginLeft:30
    },
    address:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:20,
        marginLeft:50,
        marginRight:100,
        lineHeight:28
    },
    address1:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginLeft:30,
        marginRight:100,
        lineHeight:28,
    },
    miniContainer:{
        width:'90%',
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:10,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:50
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:50,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    subContainer:{
        flex:3,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    semicontainer:{
        width:'80%',
        backgroundColor:'#fff',
        marginLeft:50,
        marginTop:-50,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        borderRadius:10,
        zIndex:0
    },
    amount:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        color:'#262626'
    },
    amount_txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        color:'#fe4155',
        lineHeight:35,
        paddingLeft:50,
    },
    txt:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        color:'#1e266d',
        lineHeight:25,
        paddingLeft:50,
        marginTop:10,
        marginRight:40
    },
    cross_txt:{
        color:'#fff',
        fontSize:25,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?7:2,
        paddingLeft:Platform.OS === 'ios'?2:1,
        letterSpacing:0.6
    },
    button_txt:{
        color:'#fff',
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?15:12,
        letterSpacing:0.6
    },
    cross_style:{
        width:'80%',
        height:35,
        borderRadius:5,
        marginLeft:30,
        backgroundColor:'#da2c2c'
    },
    button:{
        width:'85%',
        height:50,
        borderRadius:5,
        marginTop:20,
        marginLeft:30,
        marginBottom:30
    },
    imageView:{
        width:90,
        height:90,
        marginLeft:20,
        marginTop:30,
        marginBottom:-20,
        // elevation:10
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })

 export default connect(mapStateToProps)(OrderSummary);