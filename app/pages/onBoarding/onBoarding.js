import React, { Component } from 'react'
import { Text,View,StyleSheet,SafeAreaView,height,width,TouchableOpacity, BackHandler} from 'react-native'
import Swiper from 'react-native-swiper';
import TopNotch from '../../components/statusBar/statusBar'
import ImageField from '../../components/images/images'
import board1 from '../../assests/images/png/board1.png'
import board2 from '../../assests/images/png/board2.png'
import board3 from '../../assests/images/png/board3.png'


const onboardDetails = [
  {
    key: 1,
    onboardpic: board1,
    intro_text: "Best teacher in India",
    intro_sub_text: "Enjoy the captivating process of online education"
  },
  {
    key: 2,
    onboardpic: board2,
    intro_text: "Any Time Any Where",
    intro_sub_text: "Enjoy the captivating process of online education"
  },
  {
    key: 3,
    onboardpic: board3,
    intro_text: "Ready to find a course ?",
    intro_sub_text: "Join our online school that will help you learn"
  },
]

class SwiperComponent extends Component {
  constructor() {
    super()
    this.backAction = this.backAction.bind(this);
  }

    backAction = () => {
      BackHandler.exitApp();
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
      }
    componentDidMount = async () => {
      BackHandler.addEventListener("hardwareBackPress", this.backAction);
  }
  render() {
    return (
      <>
      <SafeAreaView style={{flex:1}}>
        <TopNotch topNotchColor={"white"} topNotchStyle={"dark-content"}/>
        <Swiper
          showsButtons={false}
          loop={false}
          dot={
            <View
              style={{
                backgroundColor: 
                '#fe4155',
                width: 10,
                height: 10,
                borderRadius: 5,
                margin:3,
                opacity: 0.2
              }}
            />
          }
          activeDot={
            <View
              style={{
                backgroundColor: 
                '#fe4155',
                width: 10,
                height: 10,
                borderRadius: 5,
                margin:3,
              }}
            />
          }
          paginationStyle={{
            bottom: 65,
          }}
        >
          {
            onboardDetails.map(item => {
              return (
                <View style={styles.container} key={item.key}>
                  <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginPage')}>
                    <Text style={styles.skip}>SKIP</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={styles.slide}>
                    <ImageField imageview={styles.avatar} aariv_img={item.onboardpic} />
                    <Text style={styles.text}>{item.intro_text}</Text>
                    <Text style={styles.sub_text}>{item.intro_sub_text}</Text>
                  </View>
                </View>
              )
            })
          }
        </Swiper>
        </SafeAreaView>
      </>
    )
  }
}
export default SwiperComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        height: height,
        width: width
    },
    skip:{
        opacity: 0.5,
        letterSpacing:0.45,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 16,
        // textAlign:'right',
        padding:15,
        paddingRight:20,
        color: '#3b3c4e'
    },
  slide: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 30,
        marginVertical: 10
  },
  avatar: {
    resizeMode: 'contain',
    alignSelf: "center",
    marginTop:-50,
    marginBottom:30
  },
  text: {
    color: '#1e266d',
    fontSize: 27,
    fontWeight:'500',
    textAlign: "center",
    // letterSpacing:0.45,
    fontFamily:'AVENIRLTSTD-BOOK',
  },
  sub_text: {
    color: '#585858',
    fontSize:16,
    textAlign: "center",
    fontWeight:'500',
    paddingTop:10,
    padding:60,
    lineHeight:25,
    letterSpacing:0.45,
    fontFamily:'Avenir Medium',
  },
});