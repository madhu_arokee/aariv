import React, { Component } from 'react';
import { SafeAreaView,View,StyleSheet,Text,ImageBackground, Platform,BackHandler } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import PageComponent from '../../components/pageComponent/pageComponent';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import ImageField from '../../components/images/images';
import Card from '../../components/card/card';
import Card_image from '../../assests/images/png/cardimage.png';
import Slider from '../../components/slider/slider';
import { studentpurchasedAPI,studentsubjectAPI,authAction,studentProfileAPI } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'

class MyCourse extends Component {
    constructor() {
        super(); 
        this.state = {   
        }      
    } 
    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(studentpurchasedAPI({userId}))
        dispatch(studentProfileAPI({userId}))
    } 
    gotosubjectPage = async (classname) =>{
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(authAction({purchasedLoader:true}))
        dispatch(studentsubjectAPI({
            userId,
            classname,
            navigation:this.props.navigation
        }))
    }
    render() { 
        const { purchasedData,purchasedLoader,profileData } = this.props.authStateData
        // console.log('purchasedData',purchasedData)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>  
            {/* {purchasedLoader === false? */}
                <>
                <PageComponent name={profileData.full_name} 
                OnClick={() => this.props.navigation.navigate('Notification')}
                OnNav={() => this.props.navigation.openDrawer()}
                />
                 <View style={styles.subContainer}>
                        <ScrollView>
                            <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:Platform.OS === 'ios'?0:10}}>
                                <Title titlestyle={styles.title_style} titletxt='My Courses'/>
                                {/* <Title titletxt="CLASSES" titlestyle={styles.button_txt} /> */}
                                <View style={styles.button}>
                    <Text style={styles.text}>CLASSES</Text>
                </View>
                            </View>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <ScrollView horizontal={true}>
                                    { purchasedData && purchasedData.length > 0 ?
                                    purchasedData && purchasedData.map((item,id) => {
                                        // console.log('bnsdjvnnsvjnsjnvk',item.classname)
                                        return(
                                            <Card key={id}
                                            card={{marginRight:10}}
                                            image_card={<ImageField imageview={styles.image_card} aariv_img={Card_image}/>}
                                            cardtxt="Personalised online tutoring learning program"
                                            classes={item.classname}
                                            OnClick={() => this.gotosubjectPage(item.classname)}
                                    />
                                         )
                                    })
                                :
                                null                                
                                }  
                                </ScrollView>
                            </View>
                            <Slider/>
                        </ScrollView>
                    </View>
                    </>
                    {/* :
                    <ActivityLoader/>
                    } */}
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?3.4:3.2,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    image_card:{
        width:'100%',
        borderTopLeftRadius:25,
        height:'65%',
        borderTopRightRadius:25,
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:30,
        left:-30
    },
    button:{
        backgroundColor:'#fe4155',
        width:100,
        height:30,
        borderRadius:10,
        marginTop:25,
        right:-20
    },
    text:{
        color:'#fff',
        fontSize:15,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?9:6,
        letterSpacing:0.6,
        textAlign:'center'
    },
    image_card1:{
        width:350,
        height:190,
        marginTop:20,
        marginLeft:20,
        marginRight:10,
        marginBottom:Platform.OS === 'ios'?0:20,
        borderRadius: 20,
        overflow: 'hidden',
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(MyCourse);