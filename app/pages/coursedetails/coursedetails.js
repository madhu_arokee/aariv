import React, { Component } from 'react';
import { SafeAreaView,View,StyleSheet,Text,BackHandler,ImageBackground,width,height } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import PageComponent from '../../components/pageComponent/pageComponent';
import Title from '../../components/title/title';
import ImageField from '../../components/images/images';
import CardImage from '../../assests/images/png/cardimage.png';
import Share from '../../assests/images/png/share.png';
import CustomButton from '../../components/customButton/customButton';
import TopNotch from '../../components/statusBar/statusBar'
import { addClassToCartAPI,authAction,studentProfileAPI } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
import Alarm from '../../assests/images/png/alarm.png';
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';

class CourseDetails extends Component {
    constructor() {
        super(); 
        this.state = {   
        }           
    }
    addToCart = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        const { quantity,singleClassData } = this.props.authStateData
        const product = singleClassData.product_id
        if (quantity && product){
            dispatch(authAction({loader:true}))
            dispatch(addClassToCartAPI({
                userId,
                quantity,
                product,
                navigation:this.props.navigation
            }))
            dispatch(studentProfileAPI({userId}))
        }

    }
    render() { 
        const { addErr,singleClassData,cardLoader,profileData } = this.props.authStateData
        return ( 
            <>
            <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
            {cardLoader === false?
            <>
                {/* <PageComponent name={profileData.full_name} class_name={singleClassData !== '' ? singleClassData.product_name : ''} 
                OnClick={() => this.props.navigation.navigate('Notification')}
                OnNav={() => this.props.navigation.openDrawer()}/> */}

                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>{profileData.full_name}</Text>
                        <Text style={styles.class_name}>{singleClassData !== '' ? singleClassData.product_name : ''}</Text>                    
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                
                 <View style={styles.subContainer}>
                 
                        <ScrollView>
                            <ImageField imageview={styles.card_image} aariv_img={singleClassData !== '' ? {uri:singleClassData.image_url}: ''}/>
                           {/* <TouchableOpacity style={{flexDirection:'row',justifyContent:'flex-end',top:-90}}>
                                <ImageField imageview={styles.share} aariv_img={Share}/>
                           </TouchableOpacity>  */}
                            <View style={styles.subContainer1}>
                            {singleClassData !== ''?
                            singleClassData && singleClassData.purchased === 0?
                            <CustomButton btntxt="BUY THIS COURSE" touchtext={styles.button_txt} touchview={styles.button} OnClick={() => this.addToCart()}/>
                            :
                            <View style={styles.button}>
                            <Title titlestyle={[styles.button_txt,{textAlign:'center'}]} titletxt='PURCHASED' />
                            </View>
                            :
                            null
                            }
                            
                                <Title titlestyle={styles.title_style} titletxt={singleClassData !== '' ? singleClassData.price : ''}/>
                            </View>
                            {addErr !== null &&
                            <Text style={{color:'red',fontSize:15,textAlign:'center',padding:10}}>{addErr}</Text>}
                            <View style={styles.subContainer2}>
                                <Title titlestyle={styles.title_style1} titletxt='Every Class always will be the Step for next Class'/>
                                <Title titlestyle={styles.title_style2} titletxt={singleClassData !== '' ? singleClassData.description : ''}/>
                            </View>
                            
                        </ScrollView>
                       
                    </View>
                    </ImageBackground>
                    </>
                     :
                     <ActivityLoader/>
                     }
                     
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:2,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:40
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:10,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    class_name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:8,
        fontSize:20,
        fontFamily:'AvenirLTStd-Roman'
    },
    subContainer1:{
        width:'95%',
        height:80,
        backgroundColor:"#fff",
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        elevation:20,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:20
    },
    subContainer2:{
        width:'95%',
        // height:400,
        backgroundColor:"#fff",
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        elevation:20,
        marginLeft:10,
        marginRight:10,
        // marginTop:0,
        paddingBottom:20
    },
    card_image:{
        width:'100%',
        height:250,
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    share:{
        width:100,
        height:100,
    },
    button:{
        backgroundColor:'#fe4155',
        width:200,
        height:40,
        borderRadius:10,
        marginTop:20,
        marginLeft:20
    },
    button_txt:{
        color:'#fff',
        fontSize:15,
        fontFamily:'Montserrat-SemiBold',
        paddingTop:10
    },
    title_style:{
        color:'#fe4155',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:25,
        marginRight:40    
    },
    title_style1:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:25,
        marginTop:20,
        marginLeft:20,
        marginRight:30,
        lineHeight:30
    },
    title_style2:{
        color:'#262626',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:20,
        marginLeft:20,
        marginRight:30,
        lineHeight:25
    },
    image:{
        width:'100%',
        height:'100%'
    },
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(CourseDetails);