import React, { Component } from 'react';
import { SafeAreaView,StyleSheet,BackHandler,Alert,FlatList,View,ScrollView,ImageBackground,TouchableOpacity, Platform, Text } from 'react-native';
import TopNotch from '../../components/statusBar/statusBar'
import Title from '../../components/title/title';
import ImageField from '../../components/images/images';
import Textfield from '../../components/textfield/textfield';
import Home_img from '../../assests/images/png/homebg.png';
import Alarm from '../../assests/images/png/alarm.png';
import Search from '../../assests/images/png/search.png';
import Card from '../../components/card/card'
import Card_image from '../../assests/images/png/cardimage.png';
import BookImage from '../../components/bookImage/bookImage'
import Slider from '../../components/slider/slider';
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import { studentClassesAPI,studentSingleClassAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'

class Home extends Component {
    constructor() {
        super();
        // this.backAction = this.backAction.bind(this);  
    }

    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(studentClassesAPI({userId}))
        // BackHandler.addEventListener("hardwareBackPress", this.backAction);
    }
    // componentWillUnmount() {
    //     BackHandler.removeEventListener("hardwareBackPress", this.backAction);
    //   }
    
    // backAction = () => {
    //     BackHandler.exitApp()
    //     return true;
    //   };   
   singleClass = async (product_id) => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(authAction({cardLoader:true,addErr:''}))
        dispatch(studentSingleClassAPI({
            product_id,
            userId,
            navigation:this.props.navigation
        }))  
    }
    render() { 
        const { classesData,cardLoader } = this.props.authStateData
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
                    <SafeAreaView style={styles.container}>
                        <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                        
                            { 
                            // Platform.OS === 'android'?
                              cardLoader === false?
                            <ScrollView>
                                <ImageBackground source={Home_img} style={styles.image}>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                                <Title titletxt="Comprehensive learning programs for all students" titlestyle={styles.text}/> 
                                {/* <Textfield 
                                    box_style={styles.box_style}
                                    holder="Search"
                                    text_Input='black'
                                    txt={styles.txt}
                                    icon={<ImageField imageview={styles.search} aariv_img={Search}/>}
                                /> */}
                                </ImageBackground>
                                <View>
                                    {classesData && classesData.length > 0 ?
                                <FlatList
                                    // style={(id === classesData.length - 1 ? styles.image_card2 : styles.image_card1 )}
                                    data={classesData}
                                    numColumns={2}
                                    keyExtractor={(item,id) => id}
                                    renderItem={(item,id) => 
                                        <Card key={id}
                                            // card={(id === id - 1 ? styles.image_card2 : styles.image_card1 )}
                                            image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.item.image_url}}/>}
                                            // cardtxt="Every Class always will be the Step for next Class"
                                            classes={item.item.product_name}
                                            OnClick={() => this.singleClass(item.item.product_id)}
                                        /> 
                                    }
                                />
                                :
                                null
                                }
                                </View>
                                {/* <View> 
                                 {
                                classesData && classesData.length > 0?
                                classesData && classesData.map((item,id)=> {
                                    return(
                                        <Card key={id}
                                            image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.image_url}}/>}
                                            cardtxt="Personalised online tutoring learning program"
                                            classes={item.product_name}
                                            OnClick={() => this.singleClass(item.product_id)}
                                        />    
                                    )
                                })
                                :
                                <Text>classes are full</Text>
                                }
                                </View> */}
                                <BookImage book_image={{marginTop:20}}/>
                                <Slider/> 
                            </ScrollView>
                            :
                            <ActivityLoader/>
                            }
                            {/* :
                            <>
                                <ImageBackground source={Home_img} style={styles.image}>
                                <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                                        <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                                    </TouchableOpacity> 
                                </View>
                                <Title titletxt="Comprehensive learning programs for all students" titlestyle={styles.text}/> 
                                <Textfield 
                                    box_style={styles.box_style}
                                    holder="Search"
                                    text_Input='black'
                                    txt={styles.txt}
                                    icon={<ImageField imageview={styles.search} aariv_img={Search}/>}
                                />
                                </ImageBackground>
                                <View>
                                    {classesData && classesData.length > 0 ?
                                <FlatList
                                    // style={(id === classesData.length - 1 ? styles.image_card2 : styles.image_card1 )}
                                    data={classesData}
                                    numColumns={2}
                                    keyExtractor={(item) =>item.id}
                                    renderItem={(item,id) => 
                                        <Card key={item.item.product_id}
                                        image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.item.image_url}}/>}
                                            cardtxt="Personalised online tutoring learning program"
                                            classes={item.item.product_name}
                                            OnClick={() => this.singleClass(item.item.product_id)}
                                        /> 
                                    }
                                />
                                :
                                <Text>Classes are full</Text>
                                }
                                </View>
                                <View> 
                                 {
                                classesData && classesData.length > 0?
                                classesData && classesData.map((item,id)=> {
                                    return(
                                        <Card key={id}
                                            image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.image_url}}/>}
                                            cardtxt="Personalised online tutoring learning program"
                                            classes={item.product_name}
                                            OnClick={() => this.singleClass(item.product_id)}
                                        />    
                                    )
                                })
                                :
                                <Text>classes are full</Text>
                                }
                                </View>
                                <BookImage book_image={{marginTop:20}}/>
                                <Slider/>
                        </>  */}
                    </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    image:{
        flex:1,
        height:Platform.OS === 'ios'?'140%':'140%', 
        zIndex:0
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    card:{
        marginTop:Platform.OS === 'ios'?30:40,
        marginLeft:Platform.OS === 'ios'?15:10,
    },
    text:{
        fontSize:30,
        color:'#fff',
        fontWeight:'bold',
        textAlign:'left',
        marginLeft:20,
        marginRight:120,
        lineHeight:40,
        letterSpacing:0.6,
        // marginTop:10
    },
    box_style:{
        borderRadius:30,
        width:'90%',
        borderColor:'#fff',
        backgroundColor: '#f6f7fb',
        marginLeft:20,
        marginTop:40,
        marginBottom:20,
        height:60,
    },
    txt:{
        fontFamily:'Poppins-Medium',
        paddingTop:Platform.OS === 'ios'?0:15
    },
    search:{
        width: 22,
        height: 22,
        resizeMode: "contain",
        marginVertical:18,
        marginLeft:20
    },
    image_card:{
        width:'100%',
        borderTopLeftRadius:25,
        height:'65%',
        borderTopRightRadius:25,
    },
    image_card1:{
        marginTop:90
    },
    image_card2:{
        marginTop:-90
    }
});
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps,mapDispatchToProps)(Home);

//  cardLoader === false?
//                             <ScrollView>
//                                 <ImageBackground source={Home_img} style={styles.image}>
//                                 <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
//                                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
//                                         <ImageField imageview={styles.notification} aariv_img={Alarm}/>
//                                     </TouchableOpacity> 
//                                 </View>
//                                 <Title titletxt="Comprehensive learning programs for all students" titlestyle={styles.text}/> 
//                                 <Textfield 
//                                     box_style={styles.box_style}
//                                     holder="Search"
//                                     text_Input='black'
//                                     txt={styles.txt}
//                                     icon={<ImageField imageview={styles.search} aariv_img={Search}/>}
//                                 />
//                                 </ImageBackground>
//                                 <View>
//                                     {classesData && classesData.length > 0 ?
//                                 <FlatList
//                                     // style={(id === classesData.length - 1 ? styles.image_card2 : styles.image_card1 )}
//                                     data={classesData}
//                                     numColumns={2}
//                                     keyExtractor={(item) =>item.id}
//                                     renderItem={(item,id) => 
//                                         <Card key={item.item.product_id}
//                                         image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.item.image_url}}/>}
//                                             cardtxt="Personalised online tutoring learning program"
//                                             classes={item.item.product_name}
//                                             OnClick={() => this.singleClass(item.item.product_id)}
//                                         /> 
//                                     }
//                                 />
//                                 :
//                                 <Text>Classes are full</Text>
//                                 }
//                                 </View>
//                                 {/* <View> 
//                                  {
//                                 classesData && classesData.length > 0?
//                                 classesData && classesData.map((item,id)=> {
//                                     return(
//                                         <Card key={id}
//                                             image_card={<ImageField imageview={styles.image_card} aariv_img={{uri:item.image_url}}/>}
//                                             cardtxt="Personalised online tutoring learning program"
//                                             classes={item.product_name}
//                                             OnClick={() => this.singleClass(item.product_id)}
//                                         />    
//                                     )
//                                 })
//                                 :
//                                 <Text>classes are full</Text>
//                                 }
//                                 </View> */}
//                                 <BookImage book_image={{marginTop:20}}/>
//                                 <Slider/> 
//                             </ScrollView>
//                             :
//                             <ActivityLoader/>
//                             :