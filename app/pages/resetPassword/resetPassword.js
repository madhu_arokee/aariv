import React, { Component } from 'react';
import {SafeAreaView,View,ImageBackground,Text,BackHandler, StyleSheet,Dimensions,TouchableOpacity, Platform} from 'react-native';
import Forgot_bg from '../../assests/images/png/forgotPassword_bg.png'
import Forgot_img from '../../assests/images/png/forgotPassword_img.png'
import BackButton from '../../components/backbutton/backbutton';
import ImageField from '../../components/images/images';
import TopNotch from '../../components/statusBar/statusBar';
import Textfield from '../../components/textfield/textfield';
import Title from '../../components/title/title';
import pwdIcon from '../../assests/images/png/pwdicon.png';
import CustomButton from '../../components/customButton/customButton';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { ScrollView } from 'react-native-gesture-handler';
import { changePwdAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
class ResetPassword extends Component {
    constructor(props){
        super(props);
        this.state ={
            newPassword:true,
            ConfirmPassword:true,
            password:'',
            C_password:'',
        }
        this.backAction = this.backAction.bind(this);    
    }
    
    backAction = () => {
        this.props.navigation.navigate('Otp')
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
      }
    
    componentDidMount = async () => {
        BackHandler.addEventListener("hardwareBackPress", this.backAction);
    }
    newPasswordVisibility = () =>{
        this.setState({newPassword:!this.state.newPassword})
    }
    confirmPasswordVisibility = () =>{
        this.setState({ConfirmPassword:!this.state.ConfirmPassword})
    }
    checkPassword = (password) =>{
        return !!password.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{6,10})");
    }
    handleSubmit = () => {
        const { password,C_password } = this.state;
        const { dispatch } = this.props;
        const { email } = this.props.authStateData
        dispatch(authAction({resetLoader:false}))
        let checkPwdStrength = this.checkPassword(password);
        let checkConfirmPwdStrength = this.checkPassword(C_password);
        if(password === '' && C_password === ''){
            dispatch(authAction({ resetLoader:false,change_err_msg : 'Please enter Password'}))
        }
        else if(password !== '' && C_password === ''){
            dispatch(authAction({ resetLoader:false,change_err_msg : 'Please enter Confirm Password'}))
        }
        else if(password === '' && C_password !== ''){
            dispatch(authAction({ resetLoader:false,change_err_msg : 'Please enter New Password'}))
        }
        else if(checkPwdStrength === false){
            dispatch(authAction({ resetLoader:false,change_err_msg : '6-10 characters,At least one upper case and lower case character and special Charater'}))
        }
        else if(checkConfirmPwdStrength === false){
            dispatch(authAction({resetLoader:false,change_err_msg : '6-10 characters,At least one upper case and lower case character and special Charater'}))
        }
        else if(password !== C_password){
            dispatch(authAction({ resetLoader:false,change_err_msg : 'Passwords are not same'}))
        }
        else if(password !== '' && checkPwdStrength === true && C_password !== ''  && checkConfirmPwdStrength === true && (password === C_password) && email){
            dispatch(authAction({resetLoader:true,change_err_msg:''}))
            dispatch(changePwdAPI({
                email,
                password,
                C_password,
                navigation:this.props.navigation
            }))
    }
    else{
        dispatch(authAction({resetLoader:true,change_err_msg : 'Please check all the fields'}))
    }
    }
    render() { 
        const { change_err_msg,resetLoader } = this.props.authStateData
        return ( 
            <SafeAreaView style={styles.container}>
                <TopNotch topNotchColor={"white"} topNotchStyle={"dark-content"}/>
            <ScrollView>
                <ImageBackground style={styles.image} source={Forgot_bg}>
                    <BackButton OnClick={() => this.props.navigation.goBack()}/>
                    <View style={styles.subContainer}>
                    <ImageField imageview={styles.png} aariv_img={Forgot_img}/>
                    <Title titletxt="Reset Password" titlestyle={styles.forgot}/>
                    <Title titletxt="Your New Password must be different from Previously used Password" titlestyle={styles.forgot_description}/>
                    <View>
                            <Textfield
                                holder="New Password"
                                text_Input='#3b3c4e'
                                txt={{fontSize:13}}
                                box_style={styles.box_style}
                                icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                secureTextEntry={this.state.newPassword}
                                onChangeText={value => this.setState({ password: value })}
                            />
                            <TouchableOpacity onPress={() => this.newPasswordVisibility()} style={styles.pass_show}>
                            <Ionicons name={(this.state.newPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                        </TouchableOpacity>
                        </View>
                        <View style={{marginVertical:60}}>
                            <Textfield
                                holder="Confirm New Password"
                                text_Input='#3b3c4e'
                                txt={{fontSize:13}}
                                box_style={styles.box_style}
                                icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                secureTextEntry={this.state.ConfirmPassword}
                                onChangeText={value => this.setState({ C_password: value })}
                            />
                            <TouchableOpacity onPress={() => this.confirmPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.ConfirmPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                        {change_err_msg !== null && 
                    <Text style={{color:'red',textAlign:'center'}}>{change_err_msg}</Text>}
                    {resetLoader === false?
                <CustomButton btntxt='SAVE' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.handleSubmit()}/>
                :
                <ActivityLoader size={15}/>
                    }
                    </View>
                </ImageBackground>
                </ScrollView>
            </SafeAreaView>
         );
    }
}
 const styles = StyleSheet.create({
     container:{
         flex:1,
         backgroundColor:'#fff'
        },
     image:{
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
     },
     png:{
        width:190,
        height:180,
        marginTop:-80,
        alignSelf:'center'
    },
    pass_show: {
        position: 'absolute',
        right:40,
        top:16
    },
    subContainer:{
        width:'90%',
        height:Platform.OS === 'ios'?Dimensions.get('window').height/2:Dimensions.get('window').height/1.8,
        backgroundColor:'#fff',
        marginLeft:20,
        marginVertical:100,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    box_style:{
        width:'85%',
        marginLeft:30,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb' 
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20
    },
    loginbutton:{
        height:40,
        alignSelf:'center',
        width:Dimensions.get("window").width / 2.4
    },
    logintxt:{
        fontSize:18,
        paddingTop:Platform.OS === 'ios'?9:8,
        paddingBottom:10,
        fontFamily:'Montserrat-SemiBold'
    },
    forgot:{
        marginLeft:10,
        fontFamily: 'AVENIRLTSTD-BLACK',
        fontSize: 22,
        color: '#3b3c4e',
        letterSpacing:0.45,
        marginTop:30,
        textAlign:'left',
        paddingLeft:20,
        marginRight:65,
        marginBottom:15,
        lineHeight:25,
        fontWeight:'bold'
    },
    forgot_description:{
        marginLeft:10,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 15,
        color: '#3b3c4e',
        letterSpacing:0.45,
        textAlign:'left',
        fontWeight:'400',
        paddingLeft:20,
        marginBottom:15,
        marginRight:65,
        lineHeight:25,
    }
 })
 const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);