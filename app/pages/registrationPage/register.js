import React, { Component } from 'react';
import { SafeAreaView,Text,StyleSheet,Dimensions,ScrollView,View,ImageBackground,TouchableOpacity,Platform, BackHandler } from 'react-native';
import CustomButton from '../../components/customButton/customButton';
import TopNotch from '../../components/statusBar/statusBar'
import Title from '../../components/title/title';
import logo from '../../assests/images/png/logo.png';
import login_image from '../../assests/images/png/login_image.png'
import ImageField from '../../components/images/images';
import Textfield from '../../components/textfield/textfield';
import BackButton from '../../components/backbutton/backbutton';
import register_image from '../../assests/images/png/registerpage.png';
import mailIcon from '../../assests/images/png/mail.png';
import pwdIcon from '../../assests/images/png/pwdicon.png';
import student from '../../assests/images/png/student.png';
import location from '../../assests/images/png/home.png';
import Class from '../../assests/images/png/class.png';
import parent from '../../assests/images/png/family.png';
import calender from '../../assests/images/png/calendar.png';
import phone from '../../assests/images/png/telephone.png'
import Ionicons from 'react-native-vector-icons/Ionicons'
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { registerAPI,authAction } from '../../store/action/authAction';
import { connect } from "react-redux";


class RegisterPage extends Component {
    constructor(props){
        super(props);
        this.state ={
            newPassword:true,
            ConfirmPassword:true,
            n_password:'',
            C_password:'',
        }

        this.backAction = this.backAction.bind(this); 
    }
    backAction = () => {
        this.props.navigation.navigate('LoginPage')
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
      }
    
    componentDidMount = async () => {
        BackHandler.addEventListener("hardwareBackPress", this.backAction);
    }
    newPasswordVisibility = () =>{
        this.setState({newPassword:!this.state.newPassword})
    }
    confirmPasswordVisibility = () =>{
        this.setState({ConfirmPassword:!this.state.ConfirmPassword})
    }
    checkPassword = (password) =>{
        return !!password.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{6,10})");
    }
    // checkAlpha = (name) =>{
    //     return !!name.match("^(?=.*[a-z])(?=.*[A-Z])");
    // }
    // checkBeta = (address) =>{
    //     return !!address.match("^(?=.*[a-z])(?=.*[A-Z])");
    // }
    checkPan = (pan) =>{
        return !!pan.match("^(?=.*[A-Z])(?=.{6,10})");
    }
    handleSubmit = () => {
        const { n_password,C_password } = this.state;
        const { email,student,phone,standard,parent,dateUnique,address,username,PAN_number} = this.props.authStateData
        const { dispatch } = this.props;
        dispatch(authAction({loginLoader:false}))
        let dobN = moment(dateUnique).format('DD-MM-YYYY');
        let checkPwdStrength = this.checkPassword(n_password);
        let checkConfirmPwdStrength = this.checkPassword(C_password);
        // let checkName = this.checkAlpha(student);
        // let checkusername = this.checkAlpha(username);
        // let checkParent = this.checkAlpha(parent);
        // let checkAddress = this.checkBeta(address);
        let checkPan_number = this.checkPan(PAN_number);
        if(username === ''){
            dispatch(authAction({ registerLoader:false,err : 'Please enter User Name'}))
        }
        // else if(checkusername === false){
        //     dispatch(authAction({ registerLoader:false,err : 'Please enter Username only in Alphabets'}))
        // }
        else if(student === ''){
            dispatch(authAction({ registerLoader:false,err : 'Please enter Student Name'}))
        }
        // else if(checkName === false){
        //     dispatch(authAction({ registerLoader:false,err : 'Please enter Name only in Alphabets'}))
        // }
        else if(email === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter Email Address'}))
        }
        else if(phone === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter Mobile number'}))
        }
        else if(standard === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter Class'}))
        }
        else if(dateUnique === "Date of Birth *"){
            dispatch(authAction({registerLoader:false,err : 'Please enter Date Of Birth'}))
        }
        else if(address === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter Address'}))
        }
        // else if(checkAddress === false){
        //     dispatch(authAction({ registerLoader:false,err : 'Please enter Address'}))
        // }
        else if(parent === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter Parent Name'}))
        }
        // else if(checkParent === false){
        //     dispatch(authAction({ registerLoader:false,err : 'Please enter Parent name only in Alphabets'}))
        // }
        else if(PAN_number === ''){
            dispatch(authAction({registerLoader:false,err : 'Please enter PAN Number'}))
        }
        else if(checkPan_number === false){
            dispatch(authAction({ registerLoader:false,err : 'Please enter PAN Number Only in Captial Alphabets and Numbers'}))
        }
        else if(n_password === ''){
            dispatch(authAction({registerLoader:false,err : "Please enter Password"}))
        }
        else if(C_password === ''){
            dispatch(authAction({registerLoader:false,err : "Please enter Confirm Password"}))
        }
        else if (checkPwdStrength === false) { 
            dispatch(authAction({ registerLoader:false,err: '6-10 characters,At least one upper case and lower case character and special Charater' })) 
        }
        else if (checkConfirmPwdStrength === false) { 
            dispatch(authAction({registerLoader:false, err: '6-10 characters,At least one upper case and lower case character and special Charater' })) 
        }
        else if(n_password !== C_password){
            dispatch(authAction({registerLoader:false,err : "Password Doesn't Match"}))
        }
        else if(username !== '' && student !== '' && email !== ''  && n_password !== '' && C_password !== ''  && (n_password === C_password)  && phone !== '' && standard !== ''  && dateUnique !== "Date of Birth *" && address !== '' && PAN_number !== ''){
                dispatch(authAction({registerLoader:true,err:''}))
                dispatch(registerAPI({
                    email,
                    student,
                    standard,
                    parent,
                    PAN_number,
                    dobN,
                    phone,
                    n_password,
                    C_password,
                    address,
                    username,
                    navigation:this.props.navigation
                }))
        }
        else{
            dispatch(authAction({err : 'Please check all the fields'}))
        }
    }
    render() { 
        const { dispatch } = this.props;
        const { dateUnique,err,registerLoader,registeredData } = this.props.authStateData
        return ( 
            <SafeAreaView style={styles.container}>
                <TopNotch topNotchColor={"white"} topNotchStyle={"dark-content"}/>
                    <ImageBackground source={register_image} style={styles.image}>
                    <BackButton OnClick={() => this.props.navigation.goBack()}/>
                    {/* <ScrollView contentContainerStyle={{paddingBottom:200}}> */}
                        <ImageField imageview={styles.avatar} aariv_img={logo}/>
                        <Title titlestyle={styles.student_reg} titletxt='Student Registration'/>
                        <ScrollView contentContainerStyle={{paddingBottom:250}}>
                        <View style={{marginVertical: 5}}>
                            <Textfield
                                holder="User Name *"
                                text_Input='#3b3c4e'
                                box_style={[{marginTop:-5},styles.box_style]}
                                onChangeText={value => dispatch(authAction({ username: value}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={student}/>}
                            />
                        </View>
                        <View style={{marginVertical: 10}}>
                            <Textfield
                                holder="Student Name *"
                                text_Input='#3b3c4e'
                                box_style={[{marginTop:-5},styles.box_style]}
                                onChangeText={value => dispatch(authAction({ student: value}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={student}/>}
                            />
                        </View>
                        <View>
                            <Textfield
                                holder="Email ID *"
                                text_Input='#3b3c4e'
                                box_style={styles.box_style}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={mailIcon}/>}
                                onChangeText={value => dispatch(authAction({ email: value}))}
                            />
                        </View>
                        <View style={{marginVertical: 5}}>
                            <Textfield
                                holder="Mobile Number *"
                                text_Input='#3b3c4e'
                                box_style={styles.box_style}
                                keyboardType={'phone-pad'}
                                maxLength={10}
                                onChangeText={value => dispatch(authAction({ phone: value}))}
                                // icon={
                                // <View style={{ flexDirection: 'row',marginTop:-2 }}>
                                //     <ImageField imageview={[styles.login_smarticon,{marginTop:15}]} aariv_img={phone}/>
                                // </View>}
                            />
                        </View>
                        <View>
                            <Textfield
                                 holder="Class *"
                                 text_Input='#3b3c4e'
                                 maxLength={2}
                                 keyboardType={'phone-pad'}
                                 box_style={styles.box_style}
                                 onChangeText={value => dispatch(authAction({ standard: value}))}
                                //  icon={<ImageField imageview={styles.login_smarticon} aariv_img={Class}/>}                           
                            />
                        </View>
                        <View style={{marginVertical: 10}}>
                        <DatePicker
                                style={{ width: '90%'}}
                                placeholder={`${dateUnique}`}
                                mode="date"
                                maxDate={new Date()}
                                // format="DD-MM-YYYY"
                                showIcon={false}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: [styles.dateInputDiv],                           
                                    placeholderText: [styles.dateTextDiv]
                                }}
                                onDateChange={(date) => dispatch(authAction({dateUnique: date}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={calender}/>}
                            />
                        </View>
                        <View style={{marginVertical: 5}}>
                            <Textfield
                                 holder="Address *"
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                 multiline={true}
                                 txt={{width:'90%'}}
                                 onChangeText={value => dispatch(authAction({ address: value}))}
                                //  icon={<ImageField imageview={styles.login_smarticon} aariv_img={location}/>}                            
                            />
                        </View>
                        <View style={{marginVertical: 5}}>
                            <Textfield
                                holder="Parent Name *"
                                text_Input='#3b3c4e'
                                box_style={styles.box_style}
                                onChangeText={value => dispatch(authAction({ parent : value}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={parent}/>}
                            />
                        </View>
                        <View>
                            <Textfield
                                holder="PAN Number *"
                                text_Input='#3b3c4e'
                                maxLength={10}
                                box_style={styles.box_style}
                                onChangeText={value => dispatch(authAction({ PAN_number : value}))}
                                // icon={<ImageField imageview={styles.login_smarticon} aariv_img={parent}/>}
                            />
                        </View>
                        <View style={{marginVertical:5}}>
                            <Textfield
                                 holder="Password"
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                //  icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                 secureTextEntry={this.state.newPassword}
                                onChangeText={value => this.setState({ n_password: value })}
                            
                            />
                            <TouchableOpacity onPress={() => this.newPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.newPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{marginVertical:5}}>
                            <Textfield
                                 holder="Confirm Password"
                                 text_Input='#3b3c4e'
                                 box_style={styles.box_style}
                                //  icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 10}]} aariv_img={pwdIcon}/>}
                                 secureTextEntry={this.state.ConfirmPassword}
                                 onChangeText={value => this.setState({ C_password: value })}
                            />
                            <TouchableOpacity onPress={() => this.confirmPasswordVisibility()} style={styles.pass_show}>
                                <Ionicons name={(this.state.ConfirmPassword)?'eye-outline':'eye-off-outline'}  size={20} style={{color:'#3b3c4e'}}/>
                            </TouchableOpacity>
                        </View>
                        {err !== null &&
                        <Text style={{color:'red',textAlign:'center',marginTop:5,paddingLeft:30,paddingRight:30}}>{err}</Text>}
                        {
                     registerLoader === false ?
                     <CustomButton btntxt='FINISH' touchview={styles.finish_button} touchtext={styles.finish_txt} OnClick={() => this.handleSubmit()}/>
                   :
                   
                   <ActivityLoader size={15} style={{marginTop:25}}/> 
                //    <Text style={styles.successText}>{registeredData}</Text> 
                  } 
                  </ScrollView>
                  {/* </ScrollView> */}
                </ImageBackground>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    image:{
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
    },
    avatar:{
        alignSelf:'center',
        height:90,
        marginTop:Platform.OS === 'ios'?10:-5
    },
    student_reg:{
        marginLeft:40,
        fontFamily: 'AVENIRLTSTD-BLACK',
        fontSize: 20,
        fontWeight: '400',
        color: '#262626',
        marginTop:Platform.OS === 'ios'?60:40,
        marginBottom:15
    },
    box_style:{
        width:'80%',
        marginLeft:40,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb' 
    },
    finish_button:{
        height:40,
        alignSelf:'center',
        width:Dimensions.get("window").width / 2.4
    },
    // dateIcon:{
    //     width:Platform.OS === 'ios'?25 :20,
    //     height:Platform.OS === 'ios'?25 :20,
    //     marginTop:0,left:-290
    // },
    successText: {
        color: 'green',
        fontSize: 18,
        textAlign:'center',
        paddingBottom:40
    },
    dateInputDiv: {
        width:'120%',
        marginLeft:40,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb',
        borderWidth: 1,
        height: 45,
        alignItems: 'flex-start',
        paddingLeft:15,

    },
    dateTextDiv: {
        color:'#3b3c4e',
    },
    finish_txt:{
        fontSize:18,
        paddingTop:Platform.OS === 'ios'?9:8,
        paddingBottom:10,
        fontFamily:'Montserrat-SemiBold'
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20,
        marginVertical:12
    },
    card_sectfooter: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        top: 11.5,
        left: 8
    },
    pass_show: {
        position: 'absolute',
        right: 60,
        bottom: 15,
    },
    
});
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);