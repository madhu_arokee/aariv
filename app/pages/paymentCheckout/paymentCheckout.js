import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,Alert,height,ScrollView,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../../components/images/images';
import Alarm from '../../assests/images/png/alarm.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import Title from '../../components/title/title';
import CustomButton from '../../components/customButton/customButton';
import TopNotch from '../../components/statusBar/statusBar'
import { getBillingDetailsAPI,createOrderAPI,authAction,removeAllClassToCartAPI,getPaymentDetailAPI } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
import config from '../../config/config'
import RazorpayCheckout from 'react-native-razorpay';

// let paymentArr = [];
// let payment = []
class PaymentPage extends Component {
    constructor(props){
        super(props);
        this.state ={
            line_Items:[],
            paymentArr :[],
            payment : []
        }
    }
    componentDidMount = async () => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        dispatch(getBillingDetailsAPI({userId}))
    }
    removeCartUpdate = async() => {
        const { dispatch } = this.props;
        let userId = await getKey("loginData");
        dispatch(removeAllClassToCartAPI({userId}))
      }
    onClickOrder = async () =>{
        const { dispatch } = this.props;
        const { line_Items } = this.state;
        let userId = await getKey("loginData");
        const { cartData,getBillData } = this.props.authStateData
        let billingObj = {};
            billingObj.first_name = getBillData.first_name,
            billingObj.last_name = getBillData.last_name,
            billingObj.address_1 = getBillData.address,
            billingObj.city = getBillData.city,
            billingObj.state = getBillData.state,
            billingObj.postcode = getBillData.postcode,
            billingObj.country = getBillData.country,
            billingObj.email = getBillData.email,
            billingObj.phone = getBillData.phone
        let paymentMethod = "RazorPay";
        let paymentMethodTitle = "Credit Card/Debit Card/NetBanking";
        let status = "processing";
        {cartData.product_details && cartData.product_details.map((item,id) => {
            let itemObj = {}
            itemObj.name = item.product_name
            itemObj.product_id = item.product_id
            itemObj.quantity = item.quantity
            itemObj.subtotal = item.sub_total
            itemObj.total = item.sub_total
            line_Items.push(itemObj)
        })}
        dispatch(authAction({orderLoader:true}))
        dispatch(removeAllClassToCartAPI({userId}))
        dispatch(createOrderAPI({
            userId,
            billingObj,
            paymentMethod,
            paymentMethodTitle,
            status,
            line_Items,
            navigation:this.props.navigation
        }))
    }

    // AFTER CREATING ORDER NAVIGATING TO CONFIRM PAGE
    componentDidUpdate = async(prevProps) =>{
        const { createOrderData,orderCheck} = this.props.authStateData;    
        const { dispatch } = this.props;
        if (this.props.authStateData.orderCreated !== prevProps.authStateData.orderCreated && 
            this.props.authStateData.orderCreated === true) {
                        let userDetailObj = {}
                        userDetailObj.email = createOrderData.billing.email
                        userDetailObj.contact = createOrderData.billing.phone  
                        userDetailObj.name = `${createOrderData.billing.first_name} ${createOrderData.billing.last_name}` 
                        // const totalInPaise = Number(this.state.checkoutTotal) * Number(100);
                        // console.log('totalInPaise',totalInPaise)
                        const total = createOrderData.total*100;
                        if(userDetailObj && createOrderData.id && total && createOrderData.number){
                            console.log('createOrderData.total',createOrderData.total)
                            this.razorpayPayments(userDetailObj,createOrderData.id,total,createOrderData.number);
                        }
                // SETTING ORDER CREATED STATE TO FALSE
                dispatch(authAction({orderCreated:false}))
            }
            
    }
    razorpayPayments = async (userDetailObj,orderId,total,orderDisplayId) => {
        const { dispatch } = this.props
        let userId = await getKey("loginData");
        var options = {
            description: 'Aariv Order Placed from Mobile App',
            image: config.RAZORPAY_AARIV_LOGO,
            currency: config.RAZORPAY_CURRENCY,
            key: config.RAZORPAY_TEST_KEY,//testing ID
            amount: total,
            name: `Your Order ID : ${orderDisplayId}`,
            prefill:userDetailObj,
            theme: {color: '#1e266d'}
          }
          RazorpayCheckout.open(options).then((data) => {
            // handle success
            // alert(`Success: ${config.RAZORPAY_SUCCESS_STATUS}`);
            if(data.razorpay_payment_id){
                //NAVIGATION TO CONFIRMATION PAGE
                this.props.navigation.navigate('Home')
                // AFTER CREATING ORDER CART UPDATE status
                this.removeCartUpdate();
                this.setState({payment:[]})
                // PAYMENT UPDATE API
                let paymentObj = {};
                
                    let order_Id = orderId;
                    let orderStatus =  'Success';
                    let totalResponse = {};
                    totalResponse.id = data.razorpay_payment_id,
                    totalResponse.amount = options.amount,
                    totalResponse.entity = 'payment',
                    totalResponse.method = 'card',
                    totalResponse.status = 'captured',
                    totalResponse.currency = options.currency,
                    totalResponse.order_id = orderId,
                    totalResponse.description = options.description,
                    totalResponse.email = options.prefill.email,
                    totalResponse.contact = options.prefill.contact

                    let paymentInfo = {};
                    paymentInfo.user_id =userId,
                    paymentInfo.order_id =order_Id,
                    paymentInfo.razorpay_status =orderStatus,
                    paymentInfo.total_response =totalResponse
                    this.state.payment.push(paymentInfo)
                    // console.log('paymentDetailObj success obj',paymentInfo)
                    this.setState({paymentArr:this.state.payment})
                    paymentObj.payment = this.state.paymentArr
                    console.log('paymentDetailObj success Arr',paymentObj.payment)

                    dispatch(getPaymentDetailAPI({paymentObj}))
                    }
                }).catch((data) => {
                    // handle failure
                    // console.log('data',data)

                    // alert(`Error: ${config.RAZORPAY_FAIL_STATUS}`);
                    alert('Payment Failed');
                    this.setState({line_Items:[]})
                    this.setState({paymentArr:[]})
                    // console.log('paymentArr',this.state.paymentArr)
                    let paymentObj = {};
                
                    let order_Id = orderId;
                    let orderStatus =  'Failed';
                    let totalResponse = {};
                    totalResponse.id = data.razorpay_payment_id,
                    totalResponse.amount = options.amount,
                    totalResponse.method = 'card',
                    totalResponse.status = 'captured',
                    totalResponse.entity = 'payment',
                    totalResponse.currency = options.currency,
                    totalResponse.order_id = orderId,
                    totalResponse.description = options.description,
                    totalResponse.email = options.prefill.email,
                    totalResponse.contact = options.prefill.contact

                    let paymentInfo = {};
                    paymentInfo.user_id =userId,
                    paymentInfo.order_id =order_Id,
                    paymentInfo.razorpay_status =orderStatus,
                    paymentInfo.total_response =totalResponse
                    // console.log('paymentDetailObj success obj',paymentInfo)
                    this.state.paymentArr.push(paymentInfo)
                    paymentObj.payment = this.state.paymentArr
                    console.log('paymentDetailObj Failure',paymentObj.payment)

                    dispatch(getPaymentDetailAPI({paymentObj}))
                });
    }
    render() { 
        const { cartData,getBillData,orderLoader } = this.props.authStateData
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                <ImageBackground source={Intersection} style={styles.image}>
                    {/* <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity> 
                    </View> */}
                        <Text style={styles.name} numberOfLines={1}>Checkout</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                
                <View style={styles.subContainer}>
                        <ScrollView>
                                <Title titlestyle={styles.title_style} titletxt='Payment Information'/>
                            <View style={styles.miniContainer}>
                                <Title titlestyle={styles.billing} titletxt='Billing Address'/>
                                <View style={{borderColor:'#f2f2f2',borderWidth:1,marginTop:15,width:'90%',marginLeft:20}}/>
                                        <Title titlestyle={styles.address} titletxt={getBillData.first_name}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.last_name}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.address}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.city}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.state}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.phone}/>
                                        <Title titlestyle={[styles.address1,{marginRight:50}]} titletxt={getBillData.email}/>
                                        <Title titlestyle={styles.address1} titletxt={getBillData.postcode}/>
                                        <Title titlestyle={[styles.address1,{marginBottom:20}]} titletxt={getBillData.country}/>
                                </View>
                            {/* <View style={styles.miniContainer}>
                                <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:20,marginBottom:15}}>
                                <TouchableOpacity style={styles.payment_card}></TouchableOpacity>
                                <TouchableOpacity style={styles.payment_card}></TouchableOpacity>
                                <TouchableOpacity style={styles.payment_card}></TouchableOpacity>
                                </View>
                                <Title titlestyle={styles.payment_txt} titletxt='We Accept'/>
                                <View style={{flexDirection:'row',justifyContent:'space-evenly',marginBottom:25}}>
                                <TouchableOpacity style={styles.card_type}></TouchableOpacity>
                                <TouchableOpacity style={styles.card_type}></TouchableOpacity>
                                <TouchableOpacity style={styles.card_type}></TouchableOpacity>
                                <TouchableOpacity style={styles.card_type}></TouchableOpacity>
                                <TouchableOpacity style={styles.card_type}></TouchableOpacity>
                                </View>
                            </View> */}
                            <View style={styles.miniContainer}>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                                    <Title titletxt="Sub-Total" titlestyle={styles.amount}/>
                                    <Title  titletxt={cartData.cart_total} titlestyle={styles.amount}/>
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                                    <Title titletxt="Shipping" titlestyle={styles.amount}/>
                                    <Title  titletxt="Free" titlestyle={styles.amount}/>
                                </View>
                                {/* <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30}}>
                                    <Title titletxt="Sales Tax" titlestyle={styles.amount}/>
                                    <Title  titletxt="₹50.00" titlestyle={styles.amount}/>
                                </View> */}
                                <View style={{borderColor:'#f2f2f2',borderWidth:1,marginTop:15,width:'90%',marginLeft:20}}/>
                                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20,marginLeft:30,marginRight:30,marginBottom:30}}>
                                    <Title titletxt="Total to Pay" titlestyle={[styles.amount,{fontSize:22}]}/>
                                    <Title  titletxt={cartData.cart_total} titlestyle={[styles.amount,{fontSize:22}]}/>
                                </View>
                            </View>
                            {orderLoader === false?
                            <CustomButton btntxt="Place Order" touchtext={styles.button_txt} touchview={styles.button} OnClick={() => this.onClickOrder()}/>
                            :
                            <CustomButton btntxt="Please Wait..." touchtext={styles.button_txt} touchview={styles.button}/>
                            }
                        </ScrollView>
                    </View>
                    </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'100%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20,
        marginBottom:50
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:50,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    subContainer:{
        flex:3,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25
    },
    miniContainer:{
        width:'90%',
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:30,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    box_style:{
        borderColor:'#fff',
        borderBottomColor:'#f2f2f2',
        width:'90%',
        marginLeft:10,
        marginTop:10,
        fontFamily:'AvenirLTStd-Roman'
    },
    title_style:{
        color:'#1e266d',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:22,
        marginTop:30,
        marginLeft:30
    },
    billing:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:30,
        marginLeft:30
    },
    address:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:20,
        marginLeft:35,
        marginRight:100,
        lineHeight:28
    },
    address1:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginLeft:35,
        marginRight:100,
        lineHeight:28,
    },
    payment_txt:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:15,
        marginLeft:30,
        marginBottom:10
    },
    payment_card:{
        width:90,
        height:80,
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    card_type:{
        width:50,
        height:30,
        backgroundColor:'#fff',
        borderRadius:5,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    amount:{
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        color:'#262626'
    },
    button_txt:{
        color:'#fff',
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?15:12,
        letterSpacing:0.6
    },
    button:{
        width:'90%',
        height:50,
        borderRadius:5,
        marginTop:30,
        marginLeft:20,
        marginBottom:30
    },
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(PaymentPage);