import React, { Component } from 'react';
import { SafeAreaView,View,Text,ScrollView,TouchableOpacity,Linking,ImageBackground,StyleSheet,BackHandler, Platform, Touchable } from 'react-native';
import BackButton from '../../components/backbutton/backbutton';
import ImageField from '../../components/images/images';
import PageComponent from '../../components/pageComponent/pageComponent';
import Professor from '../../assests/images/png/professor.png'
import Play from '../../assests/images/png/playicon.png'
import Share from '../../assests/images/png/share-blu.png'
import Label from '../../assests/images/png/label-blu.png'
import Title from '../../components/title/title';
import TopNotch from '../../components/statusBar/statusBar' 
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import blueLine from '../../assests/images/png/line.png';
import Alarm from '../../assests/images/png/alarm.png';
import Intersection from '../../assests/images/png/intersection.png';
import { studentchapterDetailsAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
import Video from 'react-native-video';
import { WebView } from 'react-native-webview'
import PlayVideo from '../playVideo/playVideo'

class ChapterVideo extends Component {
    constructor() {
        super(); 
        this.state = { 
            
        }       
    }
    handleButtonPress = (video) => {
        this.props.navigation.navigate('PlayVideo',{video:video})
    }
    render() { 
        const { studentchapterDetailsData,classesData,studentchapterDetailsLoader} = this.props.authStateData
        const video = studentchapterDetailsData.video;
        console.log('video',video)
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                {/* <PageComponent name="Under Standing Numbers" class_name="Class 10"  
                    OnClick={() => this.props.navigation.navigate('Notification')}
                    OnNav={() => this.props.navigation.openDrawer()}/> */}
                    {studentchapterDetailsLoader === false?
                    <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:30}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                    <Text style={styles.name} numberOfLines={1}>Chapter Video's</Text>                   
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                    <View style={{marginBottom:20}}>
                        <Text style={styles.chap_video}>Videos</Text>
                            <TouchableOpacity style={{bottom:48,left:Platform.OS === 'ios'?275:290}}>
                                <BackButton back_txt={{color:'#fff',fontSize:18,marginTop:Platform.OS === 'ios'?-25:-27}} icon={{color:'#fff'}} OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                            </TouchableOpacity> 
                    </View>
                    {/* <TouchableOpacity style={{bottom:65,left:Platform.OS === 'ios'?275:290}}>
                        <BackButton back_txt={{color:'#fff',fontSize:18,marginTop:Platform.OS === 'ios'?-25:-27}} icon={{color:'#fff'}} OnClick={() => this.props.navigation.navigate('ChapterDetailsPage')}/>
                    </TouchableOpacity>       */}
                    
                <View style={styles.subContainer}>
                {studentchapterDetailsData !== ''?
                        <ScrollView contentContainerStyle={{marginLeft:30,paddingBottom:1000}}>
                            {video && video.map((item,id) => {
                                // console.log('passing video',video)
                                return(
                                    <View key={id}>
                                    <TouchableOpacity onPress={() => this.handleButtonPress(video[id])}>
                                    <ImageField aariv_img={Professor} imageview={{width:'90%',height:200,borderRadius:10,marginTop:40}}/>
                                    <ImageField aariv_img={Play} imageview={{width:50,height:50,marginTop:-120,alignSelf:'center',marginLeft:-50}}/>
                                    {/* <View style={{flexDirection:'row',backgroundColor:'lightgrey',borderBottomLeftRadius:10,borderBottomRightRadius:10,marginTop:-60,height:60,width:'90%'}}>
                                        <Title titletxt="Introduction" titlestyle={{marginLeft:20,fontSize:20,paddingTop:15}}/>
                                        <ImageField aariv_img={Label} imageview={{width:16,height:22,marginLeft:110,marginTop:18}}/>
                                        <ImageField aariv_img={Share} imageview={{width:20,height:22,marginLeft:30,marginTop:18}}/>
                                    </View> */}
                                    </TouchableOpacity>
                                    </View>
                                )
                            })}
                        </ScrollView>
                        :
                        null
                        }
                </View>               
                </ImageBackground>
                :
                <ActivityLoader/>}
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?1:1,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        marginTop:-50
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:-20,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman',
        marginRight:25
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20
    },
    image:{
        width:'100%',
        height:'115%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    video: {
        alignSelf: 'center',
        width: 320,
        height: 200,
      },
    chap_video:{
        color:'#fff',
        marginTop:15,
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        marginLeft:Platform.OS === 'ios'?20:22
    }
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(ChapterVideo);