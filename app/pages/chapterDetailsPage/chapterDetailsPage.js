import React, { Component } from 'react';
import { SafeAreaView,View,Text,ScrollView,ImageBackground,Dimensions,FlatList,TouchableOpacity,StyleSheet,BackHandler, Platform } from 'react-native';
import BackButton from '../../components/backbutton/backbutton';
import PageComponent from '../../components/pageComponent/pageComponent';
import Title from '../../components/title/title';
import ImageField from '../../components/images/images'
import Yellow from '../../assests/images/png/yellow.png';
import Blue from '../../assests/images/png/blue.png';
import Red from '../../assests/images/png/red.png';
import Quiz from '../../assests/images/png/quiz.png';
import Video from '../../assests/images/png/videos.png';
import Pdf from '../../assests/images/png/pdf.png';
import PageImages from '../../components/detailsPageImages/detailsPageImages'
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import blueLine from '../../assests/images/png/line.png';
import Alarm from '../../assests/images/png/alarm.png';
import Intersection from '../../assests/images/png/intersection.png';
import Video_icon from '../../assests/images/png/video_icon.png'
import Pdf_icon from '../../assests/images/png/pdf_icon.png'
import TopNotch from '../../components/statusBar/statusBar' 
import { studentchapterDetailsAPI,authAction } from '../../store/action/authAction';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { connect } from "react-redux";
import { getKey } from '../../helpers/helpers'
class ChapterDetailsPage extends Component {
    constructor() {
        super(); 
        this.state = {   
        }         
    }
    render() { 
        const { studentchapterDetailsData } = this.props.authStateData
        // console.log('studentchapterDetailsData',studentchapterDetailsData)
        
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                    <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:30}}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Notification')}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>Chapter Details</Text>                 
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                        {/* <PageComponent name="Under Standing Numbers" chapters="PDF" videos="Videos" chapter_icon_style={styles.chapter_icon_style} chapter_icon={Pdf_icon} video_icon={Video_icon}
                    OnClick={() => this.props.navigation.navigate('Notification')}
                    OnNav={() => this.props.navigation.openDrawer()}/> */}
                    {/* <View style={{flexDirection:'row'}}>
                            <ImageField aariv_img={Pdf_icon} imageview={styles.chapter_icon_style}/>
                            <Text style={styles.chap_video}>PDF</Text>
                            <ImageField aariv_img={Video_icon} imageview={styles.chapter_icon_style}/>
                            <Text style={styles.chap_video}>Videos</Text>
                        </View> */}
                            {/* <TouchableOpacity style={{left:Platform.OS === 'ios'?275:290}}>
                                <BackButton back_txt={{color:'#fff',fontSize:18,marginTop:Platform.OS === 'ios'?-25:-27}} icon={{color:'#fff'}} OnClick={() => this.props.navigation.navigate('ChapterPage')}/>
                            </TouchableOpacity>                 */}
                            <View style={styles.subContainer}>
                        <ScrollView contentContainerStyle={{paddingBottom:300}}>
                            <Title titletxt="Maths chapter 1 lorem ipsum dolor sit amet,
                             consectetur adipisicing elit. Tenetur atenderit optio, 
                             laudantium eius quod, eum maxime molestiae porro omnis. 
                             Maths chapter 1 lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                             Tenetur atenderit optio, 
                            laudantium eius quod, eum maxime molestiae porro omnis." titlestyle={styles.title_style}/>
                            <View style={{flexDirection:'row',marginBottom:10}}>
                                <PageImages image_bg={Yellow} icon_style={Video} head_txt="Watch  Videos" OnClick={() => this.props.navigation.navigate('ChapterVideo')}/>
                                <PageImages image_bg={Red} icon_style={Pdf} head_txt="View Document" OnClick={() => this.props.navigation.navigate('ChapterPdf')}/>
                            </View>
                            <View style={{flexDirection:'row',marginTop:25}}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('QuizView')}>
                                    <ImageField imageview={styles.icon} aariv_img={Blue}/>
                                    <ImageField imageview={styles.video_icon} aariv_img={Quiz}/>
                                        <Text style={[styles.class,{marginTop:-65,marginBottom:50}]} numberOfLines={2}>Solve Quiz</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                </View>
                </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    subContainer:{
        flex:Platform.OS === 'ios'?1:1,
        width:'100%',
        backgroundColor:'#fff',
        borderTopLeftRadius:25,
        borderTopRightRadius:25,
        // marginTop:-20,
        marginTop:20
    },
    chap_video:{
        color:'#fff',
        marginTop:Platform.OS === 'ios'?15:10,
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        marginLeft:10,
        width:70,
        height:30,
    },
    icon:{
        width:Dimensions.get("window").width/2.2,
        height:100,
        borderRadius:20,
        marginLeft:8
    },
    video_icon:{
        width:50,
        height:50,
        marginVertical:-75,
        marginLeft:15,
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20,
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:-20,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman',
        marginRight:25
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20
    },
    class:{
        color:'#fff',
        marginLeft:75,
        fontSize:21,
        fontFamily:'AvenirLTStd-Roman',
        lineHeight:30
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    title_style:{
        color:'#14131e',
        fontFamily:'AvenirLTStd-Roman',
        fontSize:18,
        marginTop:30,
        marginLeft:20,
        marginRight:30,
        lineHeight:28
    },
    sub_txtstyle:{
        fontFamily:'AvenirLTStd-Roman',
        color:'#fff',
        fontSize:22,
        textAlign:'right',
        marginVertical:-80,
        // paddingRight:180,
        lineHeight:30
    },
    quiz_txtstyle:{
        fontFamily:'AvenirLTStd-Roman',
        color:'#fff',
        fontSize:22,
        textAlign:'right',
        // marginVertical:-80,
        // paddingRight:180,
        lineHeight:30
    },
    chapter_icon_style:{
        width:30,
        height:30,
        marginLeft:20,
        marginTop:10
    },
    image:{
        width:'100%',
        height:'115%'
    },
}) 
const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 export default connect(mapStateToProps)(ChapterDetailsPage);