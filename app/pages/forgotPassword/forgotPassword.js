import React, { Component } from 'react';
import {SafeAreaView,View,ImageBackground,ScrollView,Text,BackHandler, StyleSheet,Dimensions, Platform} from 'react-native';
import Forgot_bg from '../../assests/images/png/forgotPassword_bg.png'
import Forgot_img from '../../assests/images/png/forgotPassword_img.png'
import BackButton from '../../components/backbutton/backbutton';
import ImageField from '../../components/images/images';
import TopNotch from '../../components/statusBar/statusBar';
import Textfield from '../../components/textfield/textfield';
import Title from '../../components/title/title';
import mailIcon from '../../assests/images/png/mail.png';
import CustomButton from '../../components/customButton/customButton';
import ActivityLoader from '../../components/activityIndicator/activityIndicator';
import { resetPwdAPI,authAction } from '../../store/action/authAction';
import { connect } from "react-redux";
class ForgotPassword extends Component {
    constructor(props){
        super(props);
        this.state ={
        }

        this.backAction = this.backAction.bind(this);
    }
    backAction = () => {
        this.props.navigation.navigate('LoginPage')
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
      }
    
    componentDidMount = async () => {
        BackHandler.addEventListener("hardwareBackPress", this.backAction);
    }
    resetPwd = () => {
        const { dispatch } = this.props;
        const { email } = this.props.authStateData
        dispatch(authAction({ forgotLoader: false }))
        if (email) { 
            dispatch(authAction({ forgotLoader: true }))
            dispatch(resetPwdAPI({ email,navigation:this.props.navigation}))
        }
        else{
            dispatch(authAction({ forgotLoader: false, errMsg: 'Please Check Username/Email' }))
        }
    }
    

    render() { 
        const { forgotLoader,errMsg } = this.props.authStateData
        const { dispatch } = this.props;
        return ( 
            <SafeAreaView style={styles.container}>
                <TopNotch topNotchColor={"white"} topNotchStyle={"dark-content"}/>
                <ImageBackground style={styles.image} source={Forgot_bg}>
                    <BackButton OnClick={() => this.props.navigation.goBack()}/>
                    <ScrollView contentContainerStyle={{paddingBottom:Platform.OS === 'ios'?200:0}}>
                    <View style={styles.subContainer}>
                    <ImageField imageview={styles.png} aariv_img={Forgot_img}/>
                    <Title titletxt="Forgot Password?" titlestyle={styles.forgot}/>
                    <Title titletxt="Please Enter your Email ID to send a Verification Code" titlestyle={styles.forgot_description}/>
                    <View style={{marginVertical: 10}}>
                            <Textfield
                                txt={{width:'80%'}}
                                holder="Enter Email ID"
                                text_Input='#3b3c4e'
                                box_style={[{marginTop:-5},styles.box_style]}
                                icon={<ImageField imageview={[styles.login_smarticon,{marginVertical: 12}]} aariv_img={mailIcon}/>}
                                onChangeText={value => dispatch(authAction({ email:value}))}
                            />
                    </View>
                    {errMsg !== null &&
                    <Text style={{color:'red',textAlign:'center',marginTop:40}}>{errMsg}</Text>}
                    {
                     forgotLoader === false ?
                     <CustomButton btntxt='SEND' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.resetPwd()}/>
                   :
                   <View style={{top:30}}>
                    <ActivityLoader  size={15}/>  
                   </View>
                  } 
                    {/* <CustomButton btntxt='SEND' touchview={styles.loginbutton} touchtext={styles.logintxt} OnClick={() => this.handleSubmit()}/> */}
                    </View>

                    </ScrollView>
                </ImageBackground>
            </SafeAreaView>
         );
    }
}
 const styles = StyleSheet.create({
     container:{
         flex:1,
         backgroundColor:'#fff'
        },
     image:{
        width:Dimensions.get("window").width,
        height:Dimensions.get("window").height
     },
     png:{
        width:190,
        height:180,
        marginTop:-80,
        alignSelf:'center'
    },
    subContainer:{
        width:'90%',height:400,
        backgroundColor:'#fff',
        marginLeft:20,
        marginVertical:100,
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5}
    },
    box_style:{
        width:'85%',
        marginLeft:30,
        borderRadius:5,
        borderColor:'#e8e8e8',
        backgroundColor: '#f6f7fb' 
    },
    login_smarticon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginLeft: 20
    },
    loginbutton:{
        height:40,
        marginTop:5,
        alignSelf:'center',
        width:Dimensions.get("window").width / 2.4
    },
    logintxt:{
        fontSize:18,
        paddingTop:Platform.OS === 'ios'?9:8,
        paddingBottom:10,
        fontFamily:'Montserrat-SemiBold'
    },
    forgot:{
        marginLeft:10,
        fontFamily: 'AVENIRLTSTD-BLACK',
        fontSize: 22,
        color: '#3b3c4e',
        letterSpacing:0.45,
        marginTop:30,
        textAlign:'left',
        paddingLeft:20,
        marginRight:65,
        paddingBottom:15,
        lineHeight:25,
        fontWeight:'bold'
    },
    forgot_description:{
        marginLeft:10,
        fontFamily: 'AvenirLTStd-Roman',
        fontSize: 15,
        color: '#3b3c4e',
        letterSpacing:0.45,
        textAlign:'left',
        fontWeight:'400',
        paddingLeft:20,
        marginRight:65,
        paddingBottom:15,
        lineHeight:25,
    }
 })
 const mapStateToProps = state => ({
    authStateData: state.authState,
 })
 
 function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
 }
 export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);