import React, { Component } from 'react'
import { Text,TouchableOpacity,StyleSheet,SafeAreaView } from 'react-native';

export default class CustomButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }
    render() {
        return (
            <SafeAreaView>   
                <TouchableOpacity style={[styles.button,this.props.touchview]} onPress={() => this.props.OnClick()}>
                    <Text style={[styles.text,this.props.touchtext]}>{this.props.btntxt}</Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    button:{
    alignItems:'center',
    borderRadius:30,
    backgroundColor:'#1e266d',
        
    },
    text:{
        color:'#ffffff'
    }
})