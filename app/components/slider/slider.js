import React, { Component } from 'react';
import { SafeAreaView,ScrollView,Text,View,ImageBackground,StyleSheet } from 'react-native';
import Card_image from '../../assests/images/png/cardimage.png';
import Card_image1 from '../../assests/images/png/cardimage2.png';
import Title from '../../components/title/title';

const studentList = [
    {
        scrollImage: Card_image,
        information:'Personalised online tutoring learning program'
    },
    {
        scrollImage: Card_image1,
        information:'Personalised online tutoring learning program'
    }
]

class Slider extends Component {
    state = {  }
    render() { 
        return ( 
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <Title titletxt="Aariv Corner" titlestyle={{color:'#1e266d',fontFamily:'AvenirLTStd-Roman',marginLeft:20,fontSize:25,marginTop:30}}/>
                    <View style={{flexDirection:'row',paddingBottom:20}}>
                                            <ScrollView horizontal={true}>
                                                {
                                                   studentList.map((item,id) => {
                                                    return (
                                                        <View key={id}>
                                                            <ImageBackground style={(id === studentList.length - 1 ? styles.image_card2 : styles.image_card1 )} source={item.scrollImage}>
                                                                <View>
                                                                    <Text style={styles.info_txt}>{item.information}</Text>
                                                                </View> 
                                                            </ImageBackground>  
                                                        </View>
                                                    )
                                                }
                                                )}
                                            </ScrollView>
                    </View>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    image_card1:{
        width:330,
        height:190,
        marginTop:20,
        marginLeft:10,
        marginRight:10,
        marginBottom:Platform.OS === 'ios'?0:20,
        borderRadius: 20,
        overflow: 'hidden',
    },
    image_card2:{
        width:330,
        height:190,
        marginTop:20,
        // marginLeft:5,
        marginRight:10,
        marginBottom:Platform.OS === 'ios'?0:20,
        borderRadius: 20,
        overflow: 'hidden',
    },
    info_txt:{
        color:'#fff',
        textAlign:'left',
        lineHeight:20,
        fontSize:20,
        fontFamily:'AvenirLTStd-Roman',
        paddingLeft:10,
        paddingTop:130,
        paddingRight:50
    }
}) 
export default Slider;