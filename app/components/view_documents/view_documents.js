import React, { Component } from 'react';
import { SafeAreaView,View,Text,StyleSheet, Platform } from 'react-native';
import ImageField from '../images/images';
import Pdf from '../../assests/images/png/pdf.png'
import { TouchableOpacity } from 'react-native-gesture-handler';
class ViewDocument extends Component {
    state = {  }
    render() { 
        return ( 
            <SafeAreaView style={{flex:1,backgroungColor:'#fff',marginLeft:40,marginRight:40,marginTop:20}}>
                <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                {/* <View style={[styles.number_view,this.props.number_view]}>
                    <Text style={styles.number}>{this.props.number}</Text>
                </View> */}
                <Text style={styles.txt} numberOfLines={3}>
                In every moment, you are the only one who gets to choose your attitude
                </Text>
                <TouchableOpacity onPress={() => this.props.onPress()}>
                <ImageField imageview={styles.pdf_icon} aariv_img={Pdf}/>
                </TouchableOpacity>
                </View> 
                <View style={{borderColor:'#f2f2f2',borderWidth:1,marginTop:20}}/>               
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    number_view:{
        width:80,
        height:80,
        borderRadius:20,
        // backgroundColor:'#f9f1fc',
    },
    number:{
        color:'#0f1010',
        textAlign:'center',
        marginTop:Platform.OS === 'ios'?30:25,
        fontFamily:'AvenirLTStd-Roman',
        fontSize:25
    },
    txt:{
        color:'#0f1010',
        textAlign:"left",
        fontWeight:'400',
        fontSize:20,
        fontFamily:'AvenirLTStd-Roman',
        // paddingLeft:60,
        paddingRight:70,
        lineHeight:25
    },
    pdf_icon:{
        width:50,
        height:50,
        marginTop:15
    },
}) 
export default ViewDocument;