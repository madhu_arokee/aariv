import React, { Component } from 'react';
import { SafeAreaView,TouchableOpacity,Text,StyleSheet,View,height,width, ScrollView } from 'react-native'
import Swiper from 'react-native-swiper';
import TopNotch from '../../components/statusBar/statusBar'
import book_image from '../../assests/images/png/bookimage.png';
import ImageField from '../../components/images/images';
import CustomButton from '../../components/customButton/customButton';
const onboardDetails = [
    {
      key: 1,
      onboardpic: book_image,
    },
    {
      key: 2,
      onboardpic: book_image,
    },
    {
      key: 3,
      onboardpic: book_image,
    },
  ]
  
  class BookImage extends Component {
    constructor() {
      super()
      this.state = {
      };
    }
  
    render() {
      return (
        <>
        <SafeAreaView style={{flex:1}}>
          {/* <Swiper
            showsButtons={false}
            loop={false}
            dot={
              <View
                style={{
                  backgroundColor: 
                  '#fe4155',
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  margin:3,
                  opacity: 0.2
                }}
              />
            }
            activeDot={
              <View
                style={{
                  backgroundColor: 
                  '#fe4155',
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  margin:3,
                }}
              />
            }
            paginationStyle={{
              bottom: -23,
            left: null,
            right: 10
            }}
          > */}
          <ScrollView horizontal={true}>
            {
              onboardDetails.map(item => {
                return (
                  <View style={styles.container} key={item.key}>
                    <View style={styles.slide}>
                    <ImageField imageview={[styles.book_image,this.props.book_image]} aariv_img={item.onboardpic}/>
                    {/* <TouchableOpacity> */}
                        {/* <CustomButton btntxt="Book a Free Trial" touchview={styles.book_button} touchtext={styles.book_txt}/> */}
                    {/* </TouchableOpacity> */}
                    <View style={styles.book_button}>
                    <Text style={styles.book_txt}>Book a Free Trial</Text>
                </View>
                    </View>
                  </View>
                )
              })
            }
            </ScrollView>
          {/* </Swiper> */}
          </SafeAreaView>
        </>
      )
    }
  }
const styles = StyleSheet.create({
    book_button:{
        borderRadius:5,
        width:'40%',
        height:30,
        backgroundColor:'#fff',
        marginBottom:30,//for static
        marginVertical:-45,
        marginLeft:175,
    },
    slide: {
        flex: 1,
        marginVertical: 20
  },
    container: {
        // flex: 1,
        // backgroundColor: 'white',
        height: height,
        width: width
    },
    book_txt:{
        color:'#0f1010',
        textAlign:'center',//for static
        fontSize:15,
        fontFamily:'AvenirLTStd-Roman',
        paddingTop:Platform.OS === 'ios'?9:6
    },
    book_image:{
        alignSelf:'center',
        marginLeft:10,
        marginRight:10
        // marginTop:-10
     },
})
export default BookImage;
