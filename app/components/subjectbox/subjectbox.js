import React, { Component } from 'react';
import { SafeAreaView,StyleSheet,TouchableOpacity,Text, Platform } from 'react-native';
import ImageField from '../images/images'
import Title from '../title/title';

class SubjectBox extends Component {
    state = {  }
    render() { 
        return ( 
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <TouchableOpacity style={styles.subject_box} onPress={() => this.props.OnClick()}>
                    <ImageField imageview={styles.icon} aariv_img={this.props.Icon}/>
                    <Text style={styles.sub_txtstyle} numberOfLines={2}>{this.props.subject_txt}</Text>
                </TouchableOpacity>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    subject_box:{
        width:Platform.OS === 'ios'?110:120,
        height:160,
        marginLeft:Platform.OS === 'ios'?5:10,
        marginBottom:20,
        borderRadius:10,
        backgroundColor:'#fff',
        borderWidth:2,
        borderColor:'#f2f2f2',
        elevation:5,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
    },
    icon:{
        marginTop:30,
        alignSelf:'center',
        width:60,
        height:60
    },
    sub_txtstyle:{
        fontFamily:'AvenirLTStd-Roman',
        color:'#727272',
        textAlign:'center',
        fontSize:15,
        paddingTop:20,
        lineHeight:20
    }
}) 
export default SubjectBox;