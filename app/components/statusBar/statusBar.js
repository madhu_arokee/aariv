import React from 'react';
import { StatusBar} from 'react-native';

function TopNotch(props) {
  return (
    <>
    <StatusBar barStyle={props.topNotchStyle} backgroundColor={props.topNotchColor} />
    </>
  );
}

export default TopNotch;