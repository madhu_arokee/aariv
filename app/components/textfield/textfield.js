import React , { Component } from 'react';
import { View, TextInput, StyleSheet,SafeAreaView, Platform } from 'react-native';

class Textfield extends Component {
    render() { 
        return ( 
            <SafeAreaView style={{flex:1}}>
            <View style={[styles.fieldContainer,this.props.box_style]}>
                {this.props.icon}
                <TextInput
                    style={[styles.txt,this.props.txt]}
                    autoCapitalize='none'
                    showSoftInputOnFocus={this.props.showSoftInputOnFocus}
                    value={this.props.value}
                    keyboardType={this.props.keyboardType}
                    onChangeText={this.props.onChangeText}
                    secureTextEntry={this.props.secureTextEntry}
                    placeholder={this.props.holder}
                    placeholderTextColor={this.props.text_Input}
                    multiline={this.props.multiline}
                    numberOfLines={this.props.numberOfLines}
                    maxLength={this.props.maxLength}
                />
            </View>
            </SafeAreaView>
         );
    }
}

const styles = StyleSheet.create({
    fieldContainer: {
        borderWidth: 1,
        borderColor:'black',
        flexDirection: 'row',
        height: 45,
        marginVertical: 5
    },
    txt:{
        paddingLeft:15,
        fontSize:15,
        width:Platform.OS === 'ios'?'65%':'70%',
        color:'black'
    }
})
export default Textfield;