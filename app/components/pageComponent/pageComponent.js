import React, { Component } from 'react';
import { SafeAreaView,View,Text,ImageBackground,StyleSheet, TouchableOpacity, Platform } from 'react-native';
import ImageField from '../images/images';
import Alarm from '../../assests/images/png/alarm.png';
import BurgerMenu from '../../assests/images/png/burgurmenu.png';
import blueLine from '../../assests/images/png/line.png';
import Intersection from '../../assests/images/png/intersection.png';
import { DrawerActions } from '@react-navigation/native';
import TopNotch from '../../components/statusBar/statusBar'
import BackButton from '../backbutton/backbutton';

class PageComponent extends Component {
    render() { 
        return ( 
            <>
                <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
            <TopNotch topNotchColor={"#2068A5"} topNotchStyle={"light-content"}/>
                <ImageBackground source={Intersection} style={styles.image}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.OnNav()}>
                            <ImageField imageview={styles.burger} aariv_img={BurgerMenu}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.props.OnClick()}>
                            <ImageField imageview={styles.notification} aariv_img={Alarm}/>
                        </TouchableOpacity>  */}
                    </View>
                        <Text style={styles.name} numberOfLines={1}>{this.props.name}</Text>
                        {/* <Text style={styles.class_name}>{this.props.class} {this.props.class_name}</Text>                     */}
                        <ImageField imageview={styles.blueline} aariv_img={blueLine}/>
                        <View style={{flexDirection:'row'}}>
                            <ImageField aariv_img={this.props.chapter_icon} imageview={this.props.chapter_icon_style}/>
                            <Text style={[styles.chap_video,this.props.chapter_style]}>{this.props.chapters}</Text>
                            <ImageField aariv_img={this.props.video_icon} imageview={this.props.chapter_icon_style}/>
                            <Text style={[styles.chap_video,this.props.video_style]}>{this.props.videos}</Text>
                        </View>
                </ImageBackground>
            </SafeAreaView>
            </>
         );
    }
}
const styles = StyleSheet.create({
    image:{
        width:'100%',
        height:'115%'
    },
    topSafeArea: {
        flex: 0,
        backgroundColor:"#2068A5"
    },
    notification:{
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginVertical:20,
        marginRight:20
    },
    burger:{
        width: 28,
        height: 28,
        resizeMode: "contain",
        marginVertical:20,
        marginLeft:20
    },
    blueline:{
        width: 110,
        height: 8,
        borderRadius:20,
        marginVertical:10,
        marginLeft:20
    },
    name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:10,
        fontWeight:'bold',
        fontSize:35,
        fontFamily:'AvenirLTStd-Roman'
    },
    class_name:{
        color:'#fff',
        textAlign:'left',
        marginLeft:20,
        marginTop:8,
        fontSize:20,
        fontFamily:'AvenirLTStd-Roman'
    },
    chap_video:{
        color:'#fff',
        marginTop:Platform.OS === 'ios'?13:10,
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
        marginLeft:10
    }
}) 
export default PageComponent;