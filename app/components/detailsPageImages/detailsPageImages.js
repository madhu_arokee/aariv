import React, { Component } from 'react';
import { View,Text,StyleSheet,SafeAreaView,Dimensions,TouchableOpacity, Platform } from 'react-native';
import ImageField from '../images/images';

class PageImages extends Component {
    state = {  }
    render() { 
        return ( 
            <SafeAreaView style={styles.container}>
                <TouchableOpacity onPress={() => this.props.OnClick()}>
                <ImageField imageview={styles.icon} aariv_img={this.props.image_bg}/>
                    <ImageField imageview={styles.video_icon} aariv_img={this.props.icon_style}/>
                    <View style={{marginTop:-80,marginLeft:30}}>
                        <Text style={[styles.class,this.props.text_style]} numberOfLines={2}>{this.props.head_txt}</Text>
                    </View>
                </TouchableOpacity>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff',
        marginTop:30,
        marginLeft:8
    },
    icon:{
        width:Dimensions.get("window").width/2.2,
        height:100,
        borderRadius:20,
    },
    video_icon:{
        width:50,
        height:50,
        marginVertical:-75,
        marginLeft:10,
    },
    class:{
        color:'#fff',
        marginLeft:40,
        fontSize:21,
        fontFamily:'AvenirLTStd-Roman',
        lineHeight:30,
        marginRight:10
    }
})
export default PageImages;