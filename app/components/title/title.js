import React, { Component } from 'react'
import { Text, View } from 'react-native';

export default class Title extends Component {
    constructor() {
        super()
        this.state = {
        };
    }
    render() {
        return (
            <View>
                <Text style={this.props.titlestyle} numberOfLines={this.props.linenoTxt}>{this.props.titletxt}</Text>
            </View>
        )
    }
}


