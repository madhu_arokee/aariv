import React, { Component } from 'react'
import {
    ActivityIndicator,
    StyleSheet,SafeAreaView,
    View
} from 'react-native'

export default class ActivityLoader extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                        <View style={styles.container}>
                            <ActivityIndicator size={this.props.size ? 15 : 35} color='#fe4155' style={[this.props.style]}/>
                        </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    // horizontal: {
    //     flexDirection: 'row',
    //     justifyContent: 'space-around',
    // }
})