import React, { Component } from 'react';
import { Platform, SafeAreaView,StyleSheet,TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Title from '../title/title'
class BackButton extends Component {
    state = {  }
    render() { 
        return ( 
            <SafeAreaView>
                <TouchableOpacity style={[this.props.button_style,styles.button_style]} onPress={() => this.props.OnClick()}>
                <MaterialCommunityIcons name={'keyboard-backspace'} size={25} style={[styles.icon,this.props.icon]}/>
                <Title titlestyle={[styles.back,this.props.back_txt]} titletxt='Back'/>
                </TouchableOpacity>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    icon:{
        paddingTop:20,
        paddingLeft:20,
        color:'#0253b3'
    },
    back:{
        marginTop:Platform.OS === 'ios'?-24:-25,
        marginLeft: 48,
        fontFamily:'Poppins-Medium',
        fontSize: 15,
        fontWeight: '400',
        letterSpacing: 0.31,
        color: '#0253b3'
    },
    button_style:{
        width:'25%'
    }
})
export default BackButton;