import React, { Component } from 'react';
import { View,Text,StyleSheet,SafeAreaView,TouchableOpacity, Platform,Dimensions } from 'react-native';
class Card extends Component {
    state = {  }
    render() {
        return ( 
            <SafeAreaView style={styles.container}>
                 <TouchableOpacity style={[styles.card,this.props.card]} onPress={() => this.props.OnClick()}>
                     {this.props.image_card}
                     <View style={{marginTop:-40,marginLeft:15}}>
                     <Text style={styles.class}>{this.props.classes}</Text>
                         <View style={styles.line}/>
                         <Text style={styles.txt}>Every Class always will be the Step for next Class</Text>
                     </View>
                 </TouchableOpacity>
            </SafeAreaView>
         );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:Platform.OS === 'ios'?0:1
    },
    card:{
        height:Dimensions.get("window").width/1.5,
        width:Dimensions.get("window").width/2.2,
        backgroundColor:'#fff',
        borderRadius:25,
        shadowOpacity:0.2,
        shadowColor:'grey',
        shadowOpacity:0.2,
        shadowOffset:{width:5,height:5},
        elevation:10,
        marginLeft:12,
        marginTop:20,
        marginBottom:10
    },
    txt:{
        textAlign:'left',
        color:'#262626',
        marginRight:10, 
        fontSize:17,
        fontFamily:'AvenirLTStd-Roman',
        lineHeight:22,
        marginTop:10,
    },
    class:{
        color:'#fe4155',
        textAlign:'left',
        fontSize:22,
        fontFamily:'AvenirLTStd-Roman',
    },
    line:{
        backgroundColor:'#fe4155',
        width:95,
        height:3,
        borderRadius:50,
        marginTop:2
    }
})
 export default Card;