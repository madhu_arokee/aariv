import AsyncStorage from '@react-native-community/async-storage';

async function saveKey(key, value) {
  try {
    const value2 = await AsyncStorage.setItem(key, value);
    return value2;
  } catch (error) {
  }
}

async function getKey(key) {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (error) {
    console.log("Error retrieving data" + error);
  }
}

async function removeKey(key) {
  try {
    const value = await AsyncStorage.removeItem(key);
    return value;
  } catch (error) {
    console.log("Error retrieving data" + error);
  }
}
async function clearLocalStorage() {
  try {
    const value = await AsyncStorage.clear();
    // console.log("clearLocal",value)
    return value;
  } catch (error) {
    console.log("Error clearing data" + error);
  }
}
module.exports = {
    saveKey,
    getKey,
    removeKey,
    clearLocalStorage,
  }