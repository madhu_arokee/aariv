import { Alert } from 'react-native';
import {
    resetPassword,
    setPassword,
    loginApi,
    verifyCode,
    studentProfile,
    register,
    changePassword,
    allClasses,
    singleClass,
    studentCart,
    addCart,
    removeCart,
    removeAllCart,
    addBillingDetails,
    getBillingDetail,
    createOrders,
    getOrders,
    singleOrder,
    purchasedClass,
    mySubject,
    myChapter,
    myChapterDetails,
    paymentResponse
} from '../../api/api';

import apiRequest from '../../api/apiRequest';
import { saveKey } from '../../helpers/helpers'

import {
    AUTH_FORM,
    LOGIN_API,
    LOGIN_API_ERROR,
    RESET_PWD_API,
    RESET_PWD_API_ERROR,
    SET_PWD_API,
    SET_PWD_API_ERROR,
    REGISTER_API_ERROR,
    REGISTER_API,
    CODE_API,
    CODE_API_ERROR,
    STUDENT_PROFILE_API,
    STUDENT_PROFILE_API_ERROR,
    CHANGE_PASSWORD_API,
    CHANGE_PASSWORD_API_ERROR,
    ALL_CLASSES_API,
    ALL_CLASSES_API_ERROR,
    SINGLE_CLASS_API,
    SINGLE_CLASS_API_ERROR,
    GET_STUDENT_CART_API,
    GET_STUDENT_CART_API_ERROR,
    ADD_CLASS_API,
    ADD_CLASS_API_ERROR,
    REMOVE_CLASS_API,
    REMOVE_CLASS_API_ERROR,
    REMOVE_ALL_CLASS_API,
    REMOVE_ALL_CLASS_API_ERROR,
    ADD_BILLING_DETAILS_API,
    ADD_BILLING_DETAILS_API_ERROR,
    GET_BILLING_DETAILS_API,
    GET_BILLING_DETAILS_API_ERROR,
    SINGLE_STUDENT_ORDER_API,
    SINGLE_STUDENT_ORDER_API_ERROR,
    SINGLE_ORDER_BY_ORDER_ID_API,
    SINGLE_ORDER_BY_ORDER_ID_API_ERROR,
    CREATE_NEW_ORDER_API,
    CREATE_NEW_ORDER_API_ERROR,
    STUDENT_PURCHASED_API,
    STUDENT_PURCHASED_API_ERROR,
    STUDENT_SUBJECT_API,
    STUDENT_SUBJECT_API_ERROR,
    STUDENT_CHAPTER_API,
    STUDENT_CHAPTER_API_ERROR,
    STUDENT_CHAPTER_DETAILS_API,
    STUDENT_CHAPTER_DETAILS_API_ERROR,
    PAYMENT_DETAIL_API,
    PAYMENT_DETAIL_API_ERROR
} from '../actionTypes/authType'
import { CommonActions } from '@react-navigation/native';


export const authAction = payload => ({
    type: AUTH_FORM,
    payload,
});


// LOGIN API
export const loginAPI = payload => async dispatch => {
    // Alert.alert("Login page Alert")
    try {
        let loginUserObj = {};
        loginUserObj.username = payload.email;
        // console.log('email',loginUserObj.username)
        loginUserObj.password = payload.password;
        // console.log('password',loginUserObj.password)
        let loginRes = await apiRequest.post(`${loginApi}`, loginUserObj)
        // console.log('Response',loginRes)
        let loginResData = loginRes.data;
       
        // console.log('local login',loginResData)

        
        
        if(loginResData && loginResData.message === "SUCCESS"){ 
            let userToken = loginResData.data.ID;
            await saveKey("loginData", userToken);
            payload.navigation.dispatch(
                CommonActions.reset({
                  index: 1,
                  routes: [
                    { name: 'Home' },
                    {
                      name: 'LoginPage',
                    },
                  ],
                })
              );
            payload.navigation.navigate('Home');
        }
        dispatch({
            type: LOGIN_API,
            payload: loginResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const errMsg = error.data.data
            // console.log('localstorageValues loginerr',errMsg)    
            dispatch({
                type: LOGIN_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
            dispatch({
                type: LOGIN_API_ERROR,
                payload: 'Invalid Email/Password',
            });
        }
    }
}
// RESET PASSWORD API
export const resetPwdAPI = payload => async dispatch => {
    try {
        let resetPwdObj = {};
        resetPwdObj.email = payload.email
        let resetRes = await apiRequest.post(`${resetPassword}`, resetPwdObj)
        
        let resetResData = resetRes.data;
        
        console.log('resetPwdObj resetRes',resetResData)

        if(resetResData && resetResData.message === "SUCCESS"){ 
            payload.navigation.navigate('Otp')
        }

        dispatch({
            type: RESET_PWD_API,
            payload: resetResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const errMsg = error.data.data
            console.log('resetPwdObj err',errMsg)
            dispatch({
                type: RESET_PWD_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
        dispatch({
            type: RESET_PWD_API_ERROR,
            payload: "Invalid Email/UserName"
        });
    }
    }
}
// REGISTER API
export const registerAPI = payload => async dispatch => {
    try {
        let registerObj = {};
        registerObj.username = payload.username
        registerObj.full_name = payload.student
        registerObj.email = payload.email
        registerObj.password = payload.n_password
        registerObj.confirm_password = payload.C_password
        registerObj.phone = payload.phone
        registerObj.class = payload.standard
        registerObj.DOB = payload.dobN
        registerObj.address = payload.address
        registerObj.parent = payload.parent
        registerObj.parent_pan_number = payload.PAN_number
        let registerRes = await apiRequest.post(`${register}`, registerObj)
        
        let registerResData = registerRes.data;
        
        console.log('register resetRes',registerResData)

        if(registerResData && registerResData.message === "SUCCESS"){ 
            payload.navigation.navigate('LoginPage')
        }

        dispatch({
            type: REGISTER_API,
            payload: registerResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('register error',err)
            dispatch({
                type: REGISTER_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: REGISTER_API_ERROR,
            payload: "Invalid Email/Student Name/Password"
        });
    }
    }
}
// CODE API
export const codeAPI = payload => async dispatch => {
    try {
        let codeObj = {};
        codeObj.code = payload.emailOtp
        codeObj.email = payload.email
        let codeObjRes = await apiRequest.post(`${verifyCode}`, codeObj)
        // console.log("OTP RESPONSE",codeObjRes)

        let codeObjResData = codeObjRes.data;
        
        console.log("Coede Password",codeObjResData)
        if(codeObjResData && codeObjResData.message === "SUCCESS"){ 
            payload.navigation.navigate('ResetPassword')
        }

        dispatch({
            type: CODE_API,
            payload: codeObjResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data){
            const errMsg = error.data.data
            console.log('Set password error',errMsg)
            dispatch({
                type: CODE_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
            dispatch({
                type: CODE_API_ERROR,
                payload: "Please check Code"
            });
        }
    }
}
// SET PASSWORD API
export const changePwdAPI = payload => async dispatch => {
    try {

        let changeUserObj = {};
        changeUserObj.confirm_password = payload.C_password
        changeUserObj.password = payload.password
        changeUserObj.email = payload.email
        let changeRes = await apiRequest.post(`${setPassword}`, changeUserObj)

        let changeResData = changeRes.data;
        
        console.log("set Password",changeResData)
        if(changeResData && changeResData.message === "SUCCESS"){ 
            payload.navigation.navigate('LoginPage')
        }

        dispatch({
            type: SET_PWD_API,
            payload: changeResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const errMsg = error.data.data
            console.log('Set password error',errMsg)
            dispatch({
                type: SET_PWD_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
            dispatch({
                type: SET_PWD_API_ERROR,
                payload: "Please check your details"
            });
        }
    }
}
// STUDENT PROFILE API
export const studentProfileAPI = payload => async dispatch => {
    try {

        let studentProfileRes = await apiRequest.get(`${studentProfile}/${payload.userId}`)
        let studentProfileResData = studentProfileRes.data.data;
        // console.log('StudentProfile',studentProfileResData)
        dispatch({
            type: STUDENT_PROFILE_API,
            payload: studentProfileResData || [],
        }); 
    }
    catch (error) {
        dispatch({           
            type: STUDENT_PROFILE_API_ERROR,
            payload: error.message
        });
    }
}
// SET PASSWORD API
export const changePasswordAPI = payload => async dispatch => {
    try {

        let changePasswordObj = {};
        changePasswordObj.current = payload.curr_password
        changePasswordObj.password = payload.n_password
        changePasswordObj.conform_password = payload.C_password
        console.log('changePasswordObj.conform_password', changePasswordObj.current)
        console.log('changePasswordObj.conform_password',changePasswordObj.password)
        console.log('changePasswordObj.conform_password',changePasswordObj.conform_password)
        let changePasswordObjRes = await apiRequest.post(`${changePassword}/${payload.userId}`, changePasswordObj)
        let changePasswordObjResData = changePasswordObjRes.data;
        
        console.log("Password Updated",changePasswordObjResData)
        // if(changePasswordObjResData && changePasswordObjResData.message === "SUCCESS"){ 
        //     payload.navigation.navigate('Home')
        // }

        dispatch({
            type: CHANGE_PASSWORD_API,
            payload: changePasswordObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const errMsg = error.data.data
            console.log('Password Updated Err',errMsg)
            dispatch({
                type: CHANGE_PASSWORD_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
            dispatch({
                type: CHANGE_PASSWORD_API_ERROR,
                payload: "Please check your details"
            });
        }
    }
}
//STUDENT ALL CLASSES API
export const studentClassesAPI = payload => async dispatch => { 
    try {
        let studentClassesRes = await apiRequest.get(`${allClasses}/${payload.userId}`)
        let studentClassesResData = studentClassesRes.data.data;
        
        dispatch({
            type: ALL_CLASSES_API,
            payload: studentClassesResData || [],
        }); 
    }
    catch (error) {
        console.log("student classes  error", error.message)
        dispatch({           
            type: ALL_CLASSES_API_ERROR,
            payload: error.message
        });
    }
}
// STUDENT SINGLE CLASS API
export const studentSingleClassAPI = payload => async dispatch => {
    try {
        let singleObj = {}
        singleObj.user_id = payload.userId
        let singleRes = await apiRequest.post(`${singleClass}/${payload.product_id}`,singleObj)
        // console.log("Single class Res",singleObj)
        let singleResData = singleRes.data;
        
        // console.log('single Res',singleResData)

        if(singleResData && singleResData.message === "SUCCESS"){
            payload.navigation.navigate('CourseDetails')
        }

        dispatch({
            type: SINGLE_CLASS_API,
            payload: singleResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            dispatch({
                type: SINGLE_CLASS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: SINGLE_CLASS_API_ERROR,
            payload: "Invalid Email/Student Name/Password"
        });
    }
    }
}
//GET STUDENT CART API
export const getStudentCartAPI = payload => async dispatch => {
    try {
        let getStudentCartRes = await apiRequest.get(`${studentCart}/${payload.userId}`)
        let getStudentCartResData = getStudentCartRes.data.data;

        dispatch({
            type: GET_STUDENT_CART_API,
            payload: getStudentCartResData || [],
        }); 
    }
    catch (error) {
        dispatch({           
            type: GET_STUDENT_CART_API_ERROR,
            payload: error.message
        });
    }
}
// ADD STUDENT CLASS TO CART API
export const addClassToCartAPI = payload => async dispatch => {
    try {
        let addCartObj = {};
        addCartObj.product_id = payload.product
        addCartObj.quantity = payload.quantity
        let addCartObjRes = await apiRequest.post(`${addCart}/${payload.userId}`, addCartObj)
        let addCartObjResData = addCartObjRes.data;
        
        // console.log('addclass Res',addCartObjResData)

        if(addCartObjResData && addCartObjResData.message === "SUCCESS"){
            payload.navigation.navigate('Cart')
        }

        dispatch({
            type: ADD_CLASS_API,
            payload: addCartObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('addclass error',err)
            dispatch({
                type: ADD_CLASS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: ADD_CLASS_API_ERROR,
            payload: "Invalid Email/Student Name/Password"
        });
    }
    }
}
// REMOVE STUDENT CLASS FROM CART API
export const removeClassToCartAPI = payload => async dispatch => {
    try {
        let removeCartObj = {};
        removeCartObj.cart_key = payload.cart_key
        let removeCartObjRes = await apiRequest.post(`${removeCart}/${payload.userId}`, removeCartObj)
        let removeCartObjResData = removeCartObjRes.data;
        
        // console.log('remove class Res',removeCartObjResData)

        if(removeCartObjResData && removeCartObjResData.message === "SUCCESS"){ 
            let getStudentCartRes = await apiRequest.get(`${studentCart}/${payload.userId}`)
            let getStudentCartResData = getStudentCartRes.data.data;

        dispatch({
            type: GET_STUDENT_CART_API,
            payload: getStudentCartResData || [],
        });
        }

        dispatch({
            type: REMOVE_CLASS_API,
            payload: removeCartObjResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('remove class error',err)
            dispatch({
                type: REMOVE_CLASS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: REMOVE_CLASS_API_ERROR,
            payload: "Invalid Email/Student Name/Password"
        });
    }
    }
}
// REMOVE STUDENT ALL CLASS FROM CART API
export const removeAllClassToCartAPI = payload => async dispatch => {
    try {
        let removeAllCartObjRes = await apiRequest.post(`${removeAllCart}/${payload.userId}`)
        let removeAllCartObjResData = removeAllCartObjRes.data;
        
        console.log('remove class Res',removeCartObjResData)

        if(removeCartObjResData && removeCartObjResData.message === "SUCCESS"){ 
            let getStudentCartRes = await apiRequest.get(`${studentCart}/${payload.userId}`)
            let getStudentCartResData = getStudentCartRes.data.data;

        dispatch({
            type: GET_STUDENT_CART_API,
            payload: getStudentCartResData || [],
        });
        }

        dispatch({
            type: REMOVE_ALL_CLASS_API,
            payload: removeAllCartObjResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('remove class error',err)
            dispatch({
                type: REMOVE_ALL_CLASS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: REMOVE_ALL_CLASS_API_ERROR,
            payload: "Invalid Email/Student Name/Password"
        });
    }
    }
}
// ADD BILLING DETAILS API
export const billingDetailsAPI = payload => async dispatch => {
    try {
        let billingDetailsObj = {};
        billingDetailsObj.first_name = payload.firstname
        billingDetailsObj.last_name = payload.lastname
        billingDetailsObj.phone = payload.mobile
        billingDetailsObj.email = payload.email
        billingDetailsObj.country = payload.country
        billingDetailsObj.street_address = payload.streetaddress
        billingDetailsObj.city = payload.city
        billingDetailsObj.state = payload.state
        billingDetailsObj.postcode = payload.postcode
        let billingDetailsRes = await apiRequest.post(`${addBillingDetails}/${payload.userId}`, billingDetailsObj)
        // console.log('Billing Details',billingDetailsRes)

        let billingDetailsResData = billingDetailsRes.data;
        
        // console.log('Billing Details Res',billingDetailsResData)

        if(billingDetailsResData && billingDetailsResData.message === "SUCCESS"){ 
            payload.navigation.navigate('PaymentPage')
        }

        dispatch({
            type: ADD_BILLING_DETAILS_API,
            payload: billingDetailsResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('Billingdetails  error',err)
            dispatch({
                type: ADD_BILLING_DETAILS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: ADD_BILLING_DETAILS_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
//GET BILLING DETAILS API
export const getBillingDetailsAPI = payload => async dispatch => {
    try {
        let getBillingDetailsRes = await apiRequest.get(`${getBillingDetail}/${payload.userId}`)
        let getBillingDetailsResData = getBillingDetailsRes.data.data;
        // console.log('getBillingDetailsAPI',getBillingDetailsResData)
        dispatch({
            type: GET_BILLING_DETAILS_API,
            payload: getBillingDetailsResData || [],
        }); 
    }
    catch (error) {
        dispatch({           
            type: GET_BILLING_DETAILS_API_ERROR,
            payload: error.message
        });
    }
}
//GET ORDERS FOR SINGLE STUDENT API
export const getOrdersAPI = payload => async dispatch => {
    try {
        let getOrdersRes = await apiRequest.get(`${getOrders}/${payload.userId}`)
        let getOrdersResData = getOrdersRes.data.data;
        console.log('getOrdersResData',getOrdersResData)
        dispatch({
            type: SINGLE_STUDENT_ORDER_API,
            payload: getOrdersResData || [],
        }); 
    }
    catch (error) {
        dispatch({           
            type: SINGLE_STUDENT_ORDER_API_ERROR,
            payload: error.message
        });
    }
}
// SINGLE ORDER API
export const singleOrderAPI = payload => async dispatch => {
    try {
        let singleorderObj = {};
        singleorderObj.order_id = payload.order_id
        let singleorderObjRes = await apiRequest.post(`${singleOrder}/${payload.userId}`, singleorderObj)
        
        let singleorderObjResData = singleorderObjRes.data;
        
        // console.log('Single Order Res',singleorderObjResData)

        if(singleorderObjResData && singleorderObjResData.message === "SUCCESS"){ 
            payload.navigation.navigate('OrderSummary')
        }

        dispatch({
            type: SINGLE_ORDER_BY_ORDER_ID_API,
            payload: singleorderObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('Single Order Res error',err)
            dispatch({
                type: SINGLE_ORDER_BY_ORDER_ID_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: SINGLE_ORDER_BY_ORDER_ID_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// CREATE ORDER API
export const createOrderAPI = payload => async dispatch => {
    try {
        let createOrderObj = {}
        createOrderObj.customer_id = payload.userId
        createOrderObj.billing = payload.billingObj
        createOrderObj.payment_method = payload.paymentMethod
        createOrderObj.payment_method_title = payload.paymentMethodTitle
        createOrderObj.status = payload.status
        createOrderObj.line_items = payload.line_Items
        let createOrderObjRes = await apiRequest.post(`${createOrders}`, createOrderObj)
        
        let createOrderObjResData = createOrderObjRes.data;
        
        console.log('Create Order Res',createOrderObjResData)
        dispatch({
            type: CREATE_NEW_ORDER_API,
            payload: createOrderObjResData || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('Single Order Res error',err)
            dispatch({
                type: CREATE_NEW_ORDER_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: CREATE_NEW_ORDER_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// STUDENT PURCHASED API
export const studentpurchasedAPI = payload => async dispatch => {
    try {
        let purchasedObjRes = await apiRequest.post(`${purchasedClass}/${payload.userId}`)
        let purchasedObjResDatadata = purchasedObjRes.data.data;
        // console.log('PUrchased data Res',purchasedObjResDatadata)

        dispatch({
            type: STUDENT_PURCHASED_API,
            payload: purchasedObjResDatadata || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            // console.log('Purchassed err',err)
            dispatch({
                type: STUDENT_PURCHASED_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: STUDENT_PURCHASED_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// STUDENT SUBJECT API
export const studentsubjectAPI = payload => async dispatch => {
    try {
        let subjectObj = {};
        subjectObj.class_name = payload.classname
        // console.log('class subject',subjectObj.class_name)
        let subjectObjRes = await apiRequest.post(`${mySubject}/${payload.userId}`, subjectObj)
        
        let subjectObjResData = subjectObjRes.data;
        
        // console.log('Subject Res',subjectObjResData)

        if(subjectObjResData && subjectObjResData.message === "SUCCESS"){ 
            payload.navigation.navigate('MyCourseDetails')
        }

        dispatch({
            type: STUDENT_SUBJECT_API,
            payload: subjectObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            // console.log('Student subject err',err)
            dispatch({
                type: STUDENT_SUBJECT_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: STUDENT_SUBJECT_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// STUDENT CHAPTER API
export const studentchapterAPI = payload => async dispatch => {
    try {
        let chapterObj = {};
        chapterObj.subject_name = payload.name
        // console.log('chapterObj',chapterObj.subject_name)
        let chapterObjRes = await apiRequest.post(`${myChapter}/${payload.userId}`, chapterObj)
        
        let chapterObjResData = chapterObjRes.data;
        
        // console.log('chapter Res',chapterObjResData)

        if(chapterObjResData && chapterObjResData.message === "SUCCESS"){ 
            payload.navigation.navigate('ChapterPage')
        }

        dispatch({
            type: STUDENT_CHAPTER_API,
            payload: chapterObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            // console.log('Student chapter err',err)
            dispatch({
                type: STUDENT_CHAPTER_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: STUDENT_CHAPTER_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// STUDENT CHAPTER DETAILS API
export const studentchapterDetailsAPI = payload => async dispatch => {
    try {
        let chapterDetailsObj = {};
        chapterDetailsObj.chapter_id = payload.chapter_id
        let chapterDetailsObjRes = await apiRequest.post(`${myChapterDetails}/${payload.userId}`, chapterDetailsObj)
        
        let chapterDetailsObjResData = chapterDetailsObjRes.data;
        console.log('chapterDetailsObjResData',chapterDetailsObjResData)

        if(chapterDetailsObjResData && chapterDetailsObjResData.message === "SUCCESS"){ 
            payload.navigation.navigate('ChapterDetailsPage')
        }

        dispatch({
            type: STUDENT_CHAPTER_DETAILS_API,
            payload: chapterDetailsObjResData.data || [],
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const err = error.data.data
            console.log('Student chapter details  err',err)
            dispatch({
                type: STUDENT_CHAPTER_DETAILS_API_ERROR,
                payload: err.error,
            });
        }
        else{
        dispatch({
            type: STUDENT_CHAPTER_DETAILS_API_ERROR,
            payload: "Please fill all the fields"
        });
    }
    }
}
// PAYMENT DETAIL API
export const getPaymentDetailAPI = payload => async dispatch => {
    try {

        let paymentDetailObj = payload.paymentObj;
        // console.log('paymentDetailObj paymentDetailObj',paymentDetailObj)
        let paymentDetailRes = await apiRequest.post(`${paymentResponse}`, paymentDetailObj)
        let paymentDetailResData = paymentDetailRes.data;
        console.log('paymentDetailObj res',paymentDetailResData)
        // console.log('paymentDetailObj paymentDetailObj res',paymentDetailResData)
        dispatch({
            type: PAYMENT_DETAIL_API,
            payload: paymentDetailResData,
        }); 
    }
    catch (error) {
        if(error.data && error.data.data){
            const errMsg = error.data.data
            dispatch({
                type: PAYMENT_DETAIL_API_ERROR,
                payload: errMsg.error,
            });
        }
        else{
            dispatch({
                type: PAYMENT_DETAIL_API_ERROR,
                payload: "Something went wrong please try later"
            });
        }
    }
}
