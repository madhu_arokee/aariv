import {
  AUTH_FORM,
  LOGIN_API,
  LOGIN_API_ERROR,
  RESET_PWD_API,
  RESET_PWD_API_ERROR,
  SET_PWD_API,
  SET_PWD_API_ERROR,
  CODE_API,
  CODE_API_ERROR,
  REGISTER_API_ERROR,
  REGISTER_API,
  CHANGE_PASSWORD_API,
  CHANGE_PASSWORD_API_ERROR,
  STUDENT_PROFILE_API,
  STUDENT_PROFILE_API_ERROR,
  ALL_CLASSES_API,
  ALL_CLASSES_API_ERROR,
  SINGLE_CLASS_API,
  SINGLE_CLASS_API_ERROR,
  GET_STUDENT_CART_API,
  GET_STUDENT_CART_API_ERROR,
  ADD_CLASS_API,
  ADD_CLASS_API_ERROR,
  REMOVE_ALL_CLASS_API,
  REMOVE_ALL_CLASS_API_ERROR,
  REMOVE_CLASS_API,
  REMOVE_CLASS_API_ERROR,
  ADD_BILLING_DETAILS_API,
  ADD_BILLING_DETAILS_API_ERROR,
  GET_BILLING_DETAILS_API,
  GET_BILLING_DETAILS_API_ERROR,
  SINGLE_STUDENT_ORDER_API,
  SINGLE_STUDENT_ORDER_API_ERROR,
  SINGLE_ORDER_BY_ORDER_ID_API,
  SINGLE_ORDER_BY_ORDER_ID_API_ERROR,
  CREATE_NEW_ORDER_API,
  CREATE_NEW_ORDER_API_ERROR,
  STUDENT_PURCHASED_API,
  STUDENT_PURCHASED_API_ERROR,
  STUDENT_SUBJECT_API,
  STUDENT_SUBJECT_API_ERROR,
  STUDENT_CHAPTER_API,
  STUDENT_CHAPTER_API_ERROR,
  STUDENT_CHAPTER_DETAILS_API,
  STUDENT_CHAPTER_DETAILS_API_ERROR,
  PAYMENT_DETAIL_API,
  PAYMENT_DETAIL_API_ERROR
} from '../actionTypes/authType'

const initialState = {
  //login
    loginLoader: false,
    loginData:[],
    loginErr:'',
    loginState:false,
  //forgotpassword
    errMsg:'',    
    email:'',
    forgotLoader:false,
  //register
    phone:'',
    standard:'',
    parent:'',
    PAN_number:'',
    dateUnique:'Date of Birth *',
    student:'',
    address:'',
    username:'',
    err:'',
    registerLoader:false,
  //reset password
    change_err_msg:'',
    otpErr:'',
    otpLoader:false,
    resetLoader:false,
    codeObjResData:'',
  //student profile
    errorMsg:'',
    profileData:[],
    profileLoader:false,
  //change password
  password_Err_Msg:'',
  changePasswordData:{},
  passwordLoader:false,
  //all classes
    classesData:[],
    error:'',
  //single class
    singleClassData:{},
    singleErr:'',
    cardLoader:false,
  // get student cart
    cartData:[],
    cartErr:'',
  //add cart
    loader:false,
    addErr:'',
    addCartData:{},
    quantity:'1',
  //remove cart
    removeErr:'',
    removeLoader:false,
    cart_key:'',
    removeAllCartData:'',
  //BillingDetails
    // firstname:'',
    // lastname:'',
    // mobile:'',
    // country:'',
    // city:'',
    // state:'',
    // streetaddress:'',
    // postcode:'',
    detailsErr:'',
    billingdetailsData:{},
    billingLoader:false,
  //get billing Details 
    getBillData:[],
    getBillErr:'',
  //student purchased classes
    purchasedData:[],
    purchasedLoader:false,
    purchasedErr:'',
  //student subject
  studentsubjectData:[],
  studentsubjectLoader:false,
  studentsubjectErr:'',
  //student chapter
  studentchapterData:[],
  studentchapterLoader:false,
  studentchapterErr:'',
  //student chapter details
  studentchapterDetailsData:[],
  studentchapterDetailsLoader:false,
  studentchapterDetailsErr:'',
  //CREATE NEW ORDER
  createOrderErr:'',
  createOrderData:{},
  orderCreated:false,
  orderLoader:false,
  orderCheck:'',
  //Order by student ID
  singleLoader:false,
  singleOrderData:[],
  singleOrderErr:'',
  //Orders of single student
  studentLoader:false,
  studentOrderData:{},
  //Payment response
  paymentDetail:{},
  paymentStatus:false,
  paymentErr:'',
  
  docSuccessMessage:'Updated SuccessFully',
  registeredData:'Account Created',
  updatePassword:'Password Updated'
}
const authReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case AUTH_FORM: {
        return {
          ...state,
          ...payload,
        };
      }
      // LOGIN  API
      case LOGIN_API: {
        return {
          ...state,
          loginLoader:false,
          loginData:payload,
          // loginState:true,
        };
      }
      case LOGIN_API_ERROR: {
        return {
          ...state,
          loginLoader: false,
          loginErr: payload,
          // loginState:false,
        };
      }
      // RESET PASSWORD  API
      case RESET_PWD_API: {
        return {
          ...state,
          forgotLoader: false,
        };
      }
      case RESET_PWD_API_ERROR: {
        return {
          ...state,
          forgotLoader: false,
          errMsg: payload
        };
      }
      //REGISTER API
      case REGISTER_API: {
        return {
          ...state,
          registerLoader: false,
          registerData:payload,
        }
      }
      case REGISTER_API_ERROR: {
        return {
          ...state,
          registerLoader: false,
          err: payload,
        }
      }
      // CODE API
      case CODE_API: {
        return {
          ...state,
          otpLoader: false,
          codeObjResData:payload
        };
      }
      case CODE_API_ERROR: {
        return {
          ...state,
          otpLoader: false,
          otpErr: payload
        };
      }
      // SET PASSWORD API
      case SET_PWD_API: {
        return {
          ...state,
          resetLoader: false,
        };
      }
      case SET_PWD_API_ERROR: {
        return {
          ...state,
          resetLoader: false,
          change_err_msg: payload
        };
      }
      //STUDENT PROFILE API
      case STUDENT_PROFILE_API: {
        return {
          ...state,
          profileData: payload,
          profileLoader:false
        };
      }
      case STUDENT_PROFILE_API_ERROR: {
        return {
          ...state,
          errorMsg: payload,
          profileLoader:false
        };
      }
      case CHANGE_PASSWORD_API: {
        return {
          ...state,
          changePasswordData: payload,
          passwordLoader:false
        };
      }
      case CHANGE_PASSWORD_API_ERROR: {
        return {
          ...state,
          password_Err_Msg: payload,
          passwordLoader:false
        };
      }
      //ALL CLASSES API
      case ALL_CLASSES_API: {
        return {
          ...state,
          classesData: payload,
          cardLoader:false
        };
      }
      case ALL_CLASSES_API_ERROR: {
        return {
          ...state,
          cardLoader:false,
          error: payload
        };
      }
      //SINGLE CLASS API
      case SINGLE_CLASS_API: {
        return {
          ...state,
          cardLoader:false,
          singleClassData: payload
        };
      }
      case SINGLE_CLASS_API_ERROR: {
        return {
          ...state,
          cardLoader:false,
          singleErr: payload
        };
      }
      // STUDENT CART API
      case GET_STUDENT_CART_API: {
        return {
          ...state,
          loader:false,
          cartData: payload
        };
      }
      case GET_STUDENT_CART_API_ERROR: {
        return {
          ...state,
          loader:false,
          cartErr: payload
        };
      }
      //ADD CLASS API
      case ADD_CLASS_API: {
        return {
          ...state,
          // loader:false,
          addCartData: payload
        };
      }
      case ADD_CLASS_API_ERROR: {
        return {
          ...state,
          // loader:false,
          addErr: payload
        };
      }
      //REMOVE CLASS API
      case REMOVE_CLASS_API: {
        return {
          ...state,
          removeLoader:false,
          removeCartData: payload
        };
      }
      case REMOVE_CLASS_API_ERROR: {
        return {
          ...state,
          removeLoader:false,
          removeErr: payload
        };
      }
      //REMOVE ALL CLASS API
      case REMOVE_ALL_CLASS_API: {
        return {
          ...state,
          removeLoader:false,
          removeAllCartData: payload
        };
      }
      case REMOVE_ALL_CLASS_API_ERROR: {
        return {
          ...state,
          removeLoader:false,
          removeAllErr: payload
        };
      }
      //ADD BILLING DETAILS API
      case ADD_BILLING_DETAILS_API: {
        return {
          ...state,
          billingLoader: false,
          billingdetailsData:payload,
        }
      }
      case ADD_BILLING_DETAILS_API_ERROR: {
        return {
          ...state,
          billingLoader: false,
          detailsErr: payload,
        }
      }
      //GET BILLING DETAILS API
      case GET_BILLING_DETAILS_API: {
        return {
          ...state,
          getBillData: payload
        };
      }
      case GET_BILLING_DETAILS_API_ERROR: {
        return {
          ...state,
          getBillErr: payload
        };
      }
      //SINGLE STUDENT ORDER API
      case SINGLE_STUDENT_ORDER_API: {
        return {
          ...state,
          studentLoader:false,
          studentOrderData: payload
        };
      }
      case SINGLE_STUDENT_ORDER_API_ERROR: {
        return {
          ...state,
          studentLoader:false,
          studentOrderErr: payload
        };
      }
      //SINGLE ORDER BY ORDER ID API
      case SINGLE_ORDER_BY_ORDER_ID_API: {
        return {
          ...state,
          singleLoader:false,
          singleOrderData: payload
        };
      }
      case SINGLE_ORDER_BY_ORDER_ID_API_ERROR: {
        return {
          ...state,
          singleLoader:false,
          singleOrderErr: payload
        };
      }
      //CREATE ORDER API
      case CREATE_NEW_ORDER_API: {
        return {
          ...state,
          orderLoader:false,
          createOrderData: payload,
          orderCreated:true,
        };
      }
      case CREATE_NEW_ORDER_API_ERROR: {
        return {
          ...state,
          orderLoader:false,
          createOrderErr: payload,
          orderCreated:false
        };
      }
      //STUDENT PURCHASED API
      case STUDENT_PURCHASED_API: {
        return {
          ...state,
          purchasedLoader:false,
          purchasedData: payload
        };
      }
      case STUDENT_PURCHASED_API_ERROR: {
        return {
          ...state,
          purchasedLoader:false,
          purchasedErr: payload
        };
      }
      //STUDENT SUBJECT API
      case STUDENT_SUBJECT_API: {
        return {
          ...state,
          studentsubjectLoader:false,
          studentsubjectData: payload
        };
      }
      case STUDENT_SUBJECT_API_ERROR: {
        return {
          ...state,
          studentsubjectLoader:false,
          studentsubjectErr: payload
        };
      }
      //STUDENT CHAPTER API
      case STUDENT_CHAPTER_API: {
        return {
          ...state,
          studentchapterLoader:false,
          studentchapterData: payload
        };
      }
      case STUDENT_CHAPTER_API_ERROR: {
        return {
          ...state,
          studentchapterLoader:false,
          studentchapterErr: payload
        };
      }
      //STUDENT CHAPTER DETAILS API
      case STUDENT_CHAPTER_DETAILS_API: {
        return {
          ...state,
          studentchapterDetailsLoader:false,
          studentchapterDetailsData: payload
        };
      }
      case STUDENT_CHAPTER_DETAILS_API_ERROR: {
        return {
          ...state,
          studentchapterDetailsLoader:false,
          studentchapterDetailsErr: payload
        };
      }
      //PAYMENT RESPONSE API
      case PAYMENT_DETAIL_API: {
        return {
          ...state,
          paymentDetail:payload,
          paymentStatus:true
        };
      }
      case PAYMENT_DETAIL_API_ERROR: {
        return {
          ...state,
          paymentErr:payload,
          paymentStatus:true
        };
      }
      default: {
        return state
      }
    }
}    
export default authReducer;