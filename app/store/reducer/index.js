import authReducer from './authReducer';
import sampleReducer from './sampleReducer';

const rootReducer = {
  // sample: sampleReducer,
  authState: authReducer,
};

export default rootReducer