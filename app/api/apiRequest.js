import OAuth from 'oauth-1.0a';
import axios, { AxiosResponse } from 'axios';
// import crypto from 'crypto'
import hmacSHA1 from 'crypto-js/hmac-sha1';
import Base64 from 'crypto-js/enc-base64';
import config from '../config/config'

const _getOAuth = (): OAuth => new OAuth({
  consumer: {
    key: config.WC_CONSUMER_KEY,
    secret: config.WC_CONSUMER_SECRET
  },
  signature_method: 'HMAC-SHA1',
  nonce_length:11,
  hash_function: (baseString: string, key: string) => Base64.stringify(
    hmacSHA1(baseString, key)
  )
});

const get = async (path: string): Promise<AxiosResponse<object>> => {
  const request = {
   url: `${config.WC_BASE_URL}${path}`,
    method: 'GET'
  };
  const oauth = _getOAuth().authorize(request);
  return axios.get(request.url, { params: oauth });
};

const post = async (path: string, body: object): Promise<AxiosResponse<object>> => {
  const request = {
    url: `${config.WC_BASE_URL}${path}`,
    method: 'POST'
  };
  const oauth = _getOAuth().authorize(request);
  return axios.post(request.url, body, { params: oauth });
};

// declare a request interceptor
axios.interceptors.request.use(config => {
  return config;
}, error => {
  // handle the error
  return Promise.reject(error);
});


axios.interceptors.response.use((response) => {
    return response;
    
}, function (error) {
  // console.log("inter",error)
  // console.log(error.response);
    return  Promise.reject(error.response);
});

export default {
  get,
  post
};