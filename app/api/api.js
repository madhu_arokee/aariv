import config from '../config/config'

// DASHBOARD APIS
  
const resetPassword = `${config.WC_API_URL}/reset-password`;   
const setPassword = `${config.WC_API_URL}/set-new-password`;
const verifyCode = `${config.WC_API_URL}/verify-code-email`;    
const studentProfile = `${config.WC_API_URL}/get-student-profile`;     
const loginApi = `${config.WC_API_URL}/login`;          
const register = `${config.WC_API_URL}/register`;
const changePassword = `${config.WC_API_URL}/update-password`;

// STUDENT CLASS APIS

const allClasses = `${config.WC_API_URL}/get-all-class`;              
const singleClass = `${config.WC_API_URL}/get-class`;                                      

// STUDENT CART APIS

const studentCart = `${config.WC_API_URL}/get-cart`;                    
const addCart = `${config.WC_API_URL}/add-cart`;
const removeCart = `${config.WC_API_URL}/remove-cart`;
const removeAllCart = `${config.WC_API_URL}/bulk-delete`;                       

// STUDENT CHECKOUT APIS

const addBillingDetails = `${config.WC_API_URL}/add-billing-details`;
const getBillingDetail = `${config.WC_API_URL}/get-billing-detail`;            

// STUDENT ORDER APIS
    
const getOrders = `${config.WC_API_URL}/get-orders`;    
const singleOrder = `${config.WC_API_URL}/get-single-order`; 
const createOrders = `${config.WC_API_URL_ORDERS}`;   

//STUDENT CLASSES APIS

const purchasedClass = `${config.WC_API_URL}/view-classes`;    
const mySubject = `${config.WC_API_URL}/get-mysubject`; 
const myChapter = `${config.WC_API_URL}/get-mychapter`; 
const myChapterDetails = `${config.WC_API_URL}/chapter-details`;

//PAYMENT RESPONSE API

const paymentResponse = `${config.WC_API_URL}/payment-response`;

module.exports = {
    resetPassword,
    setPassword,
    loginApi,
    verifyCode,
    studentProfile,
    register,
    changePassword,
    allClasses,
    singleClass,
    studentCart,
    addCart,
    removeCart,
    removeAllCart,
    addBillingDetails,
    getBillingDetail,
    createOrders,
    getOrders,
    singleOrder,
    purchasedClass,
    mySubject,
    myChapter,
    myChapterDetails,
    paymentResponse
};