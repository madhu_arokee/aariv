import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './app/store/index';

import SwiperComponent from './app/pages/onBoarding/onBoarding';
import CustomButton from './app/components/customButton/customButton';
import LoginPage from './app/pages/login/login'
import Textfield from './app/components/textfield/textfield'
import BackButton from './app/components/backbutton/backbutton';
import Router from './app/navigation/router'
import RegisterPage from './app/pages/registrationPage/register'
import Home from './app/pages/Home/Home'
import Card from './app/components/card/card'
import CourseDetails from './app/pages/coursedetails/coursedetails';
import PageComponent from './app/components/pageComponent/pageComponent'
import MyCourse from './app/pages/mycourse/mycourse'
import Slider from './app/components/slider/slider'
import SubjectBox from './app/components/subjectbox/subjectbox';
import MyCourseDetails from './app/pages/mycoursedetails/mycoursedetails'
import ChapterPage from './app/pages/chapterpage/chapterpage';
import ChapterDetailsPage from './app/pages/chapterDetailsPage/chapterDetailsPage';
import PageImages from './app/components/detailsPageImages/detailsPageImages';
import ChapterVideo from './app/pages/chaptervideo/chapterVideo';
import ChapterPdf from './app/pages/chapterPdf/ChapterPdf';
import Checkout from './app/pages/checkout/checkout';
import Cart from './app/pages/cart/cart';
import ProfilePage from './app/pages/profile/profile'
import OrderSummary from './app/pages/orderSummary/orderSummary';
import MyOrders from './app/pages/orders/orders'
import BookImage from './app/components/bookImage/bookImage'
import PdfView from './app/pages/pdf/pdf';
import PlayVideo from './app/pages/playVideo/playVideo'
import Quiz from './app/pages/quiz/quiz';
import ResetPassword from './app/pages/resetPassword/resetPassword'
import QuizView from './app/pages/quizView/quizView';

const store = configureStore();
 


export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router/>
      </Provider>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#F5FCFF',
    marginBottom: 5,
  },
});